const gulp = require('gulp');
// const pkg = require('./package.json');
const mocha = require('gulp-mocha');
const cover = require('gulp-coverage');

gulp.task('test', function() {
    return gulp.src('./mocha/*.js', {read: true})
    .pipe(cover.instrument({
        pattern: ['./mocha/**/*', '!./node_modules/**/*'],
        debugDirectory: './mocha/debug'
    }))
    .pipe(mocha({reporter: 'xunit-file'}))
    .pipe(cover.gather())
    .pipe(cover.format())
    .pipe(gulp.dest('./mocha/reports'));
});
