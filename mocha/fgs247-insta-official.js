'use strict';
const fs = require('fs');
const {assert, expect, should} = require('chai');
const shell = require('shelljs');
const moment = require('moment');

const work_path = 'D:/work/tool/tools/mocha/bteq/'; 

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

const target = 'FGS247_insta_official';

const isLoadData = () => {
    let data_file = `D:\/work\/tool\/tools\/output\/${target}_fastload_data.txt`;

    if (fs.existsSync(data_file)){
        return true;
    }
    return false;
}

const unlink = () => {
    try {
        fs.unlinkSync(`${work_path}${target}_sqlresult.txt`);
    } catch (e) {}
}

const getResult = () => {
    let today = moment().format('YYYY/MM/DD');    
    
    let bteq_script = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;
.EXPORT DATA FILE=${work_path}${target}_sqlresult.txt
.RECORDMODE OFF
    select trim(count(*)) from SBX_ICB_ICBB.fashion_${target}
     where getdate = '${today}';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
    fs.writeFileSync(`${work_path}${target}_checksql.txt`, bteq_script);
    shell.exec(`bteq < ${work_path}${target}_checksql.txt > ${work_path}${target}_bteqlog.txt` );
}

const isData = () => {
    let data = fs.readFileSync(`${work_path}${target}_sqlresult.txt`, 'utf8');
    //console.log(data);
    if (data && data >= 1) {
        return '異常なし';
    }
    return 'データがSPDBに反映されていません。';
}


describe('FGS247_insta_official', function(){
    it('【FGS247】_insta_officialのデータが格納されているか？', function(){
        if (isLoadData()) {
            unlink();
            getResult();
            expect('異常なし').to.equal(isData());
        }else{
            expect('異常なし').to.equal('異常なし');
        };

    }).timeout(600000);;

});
