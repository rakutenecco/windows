'use strict';
const fs = require('fs');
const {assert, expect, should} = require('chai');
const shell = require('shelljs');
const moment = require('moment');

const filepath = 'D:/work/tool/tools/mocha/bteq/fgs172_point_coupon.txt';
const filepath_out = 'D:/work/tool/tools/mocha/bteq/fgs172_point_coupon_out.txt';


const setBteq = () => {
    let yyyymm = moment().add(-1, 'months').format('YYYYMM');
    let bteq_script = `
    .SESSION CHARSET 'utf8';

    .logmech LDAP;
    .LOGON dm-red101/ts-rie.naruo,narunaru@201907!!;
    .set width 20000;
    .SET separator "	";

        DATABASE SBX_ICB_ICBB;
            .EXPORT REPORT FILE=${filepath_out}
            select count(*) as row_cnt from SBX_ICB_ICBB.fashion_FGS172_point_coupon a
            where a.yymm = '${yyyymm}';
        .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

    .LOGOFF
    .EXIT
    `;
    fs.writeFileSync(filepath, bteq_script);
}


const unlink = () => {
    try {
        fs.unlinkSync(filepath_out);
    } catch (e) {}
}


const isData = () => {
    let data = fs.readFileSync(filepath_out, 'utf8');
    let chk = data.match(/\d{2,}/i);
    if (chk && chk[0] >= 10) {
        return '異常なし';
    }
    return 'データがSPDBに反映されていません。';
}


describe('math', function(){
  it('amazonのデータが格納されているか？', function(){

    unlink();
    setBteq();
    shell.exec('bteq < mocha/bteq/fgs172_point_coupon.txt > mocha/bteq/fgs172_point_coupon_log.txt');

    expect('異常なし').to.equal(isData());

  }).timeout(600000);;

});
