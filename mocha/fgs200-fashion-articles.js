'use strict';
const fs = require('fs');
const {assert, expect, should} = require('chai');
const shell = require('shelljs');
const moment = require('moment');

const work_path = 'D:/work/tool/tools/mocha/bteq/'; 

let FGS = '';
let data_file = '';

const setBteq = () => {
    let today = moment().format('YYYYMMDD');    
    
    let bteq_script = `
    .SESSION CHARSET 'utf8';
    .logmech LDAP;
    .LOGON dm-red101/ts-masako.ikeda,1234qwerT;
    .set width 20000;
    .SET separator "	";

        DATABASE SBX_ICB_ICBB;
            .EXPORT REPORT FILE=${work_path}${FGS}_sqlresult.txt
            select count(*) as row_cnt from SBX_ICB_ICBB.fashion_${FGS}_articles
            where c_getdate = '${today}';
        .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

    .LOGOFF
    .EXIT
    `;
    fs.writeFileSync(`${work_path}${FGS}_checksql.txt`, bteq_script);
}


const unlink = () => {
    try {
        fs.unlinkSync(`${work_path}${FGS}_sqlresult.txt`);
    } catch (e) {}
}


const isData = () => {
    let data = fs.readFileSync(`${work_path}${FGS}_sqlresult.txt`, 'utf8');
    let chk = data.match(/\d{1,}/);
    if (chk && chk[0] >= 1) {
        return '異常なし';
    }
    return 'データがSPDBに反映されていません。';
}


describe('fashion-articles', function(){
    it('【FGS200】fashion-pressのデータが格納されているか？', function(){

        FGS = 'FGS200';
        data_file = `D:\/work\/tool\/tools\/output\/${FGS}_fastload_data.txt`;
        if (fs.existsSync(data_file)) {
            unlink();
            setBteq();
            shell.exec(`bteq < ${work_path}${FGS}_checksql.txt > ${work_path}${FGS}_bteqlog.txt` );
            expect('異常なし').to.equal(isData());
        }else{
            expect('異常なし').to.equal('異常なし');
        };

    }).timeout(600000);;

    it('【FGS201】fashion-headlineのデータが格納されているか？', function(){

        FGS = 'FGS201'; 
        data_file = `D:\/work\/tool\/tools\/output\/${FGS}_fastload_data.txt`;
        if (fs.existsSync(data_file)) {
            unlink();
            setBteq();
            shell.exec(`bteq < ${work_path}${FGS}_checksql.txt > ${work_path}${FGS}_bteqlog.txt` );
            expect('異常なし').to.equal(isData());
        }else{
            expect('異常なし').to.equal('異常なし');
        };

    }).timeout(600000);;

    it('【FGS203】GQjapanのデータが格納されているか？', function(){

        FGS = 'FGS203';  
        data_file = `D:\/work\/tool\/tools\/output\/${FGS}_fastload_data.txt`;
        if (fs.existsSync(data_file)) {
            unlink();
            setBteq();
            shell.exec(`bteq < ${work_path}${FGS}_checksql.txt > ${work_path}${FGS}_bteqlog.txt` );
            expect('異常なし').to.equal(isData());
        }else{
            expect('異常なし').to.equal('異常なし');
        };

    }).timeout(600000);;


});
