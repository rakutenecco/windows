'use strict';
const co = require('co');
const fs = require('fs');
const {assert, expect, should} = require('chai');
const shell = require('shelljs');
const moment = require('moment');
const excel = require('exceljs-rtl');

const today = moment().format('YYYYMMDD');
// const filepath = `C:/Users/ts-rie.naruo/Box Sync/fashion_kuriyama3/${today}提出【ツール】Item別YoY.xlsx`;
const filepath = `D:\/Box Sync\/EC Consulting Department\/ecc_fashion\/genre_moving_average_7_yoy\/${today}提出【ツール】Item別YoY.xlsx`;

const chkData = () => {

    return new Promise((resolve) => {

        let mes = '';
        let data1 = '';
        let rows1 = '';

        try {

            const workbook = new excel.Workbook();
            workbook.xlsx.readFile(filepath)
            .then(function() {
                const worksheet = workbook.getWorksheet('ジャンル3rd');
                const row = worksheet.getRow(4).values;

                if (row.length > 10 && row[2] !== '') {
                    resolve('異常なし');
                } else {
                    resolve('データが欠落しています。');
                }
            });

        } catch (e) {
            resolve('ファイルが作成されていません。');
        }
    });
}


describe('math', function(){
    it('yahooのデータが格納されているか？', function(done){
        co(function* () {

            let res = yield chkData();
            expect('異常なし').to.equal(res);
            done();
        });
    }).timeout(600000);
});
