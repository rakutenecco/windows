'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const fs = require('fs');
const moment = require('moment');
const date = moment().format('YYYYMMDD');
let yesterday;

// (1) FGS239GMS, fastload daily 9:25
new cronJob(`00 25 09 * * 0-6`, function(){
    console.log(`cron102(1) ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} start //`);
    
    //console.log(`fgs239-targets-load start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
    //shell.exec('node src/fgs239-targets-load.js');
    
    console.log(`fgs239-report-GMS start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
    shell.exec('node src/fgs239-report-GMS.js');
    
    console.log(`cron102(1) ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} end //`);

}, function(){
    // error
    console.log(`Error ${moment().format('YYYY-MM-DD HH:mm:ss')} : FGS239 FGS258 fastload, report cron101`);
},
true, 'Asia/Tokyo');

// (2) FGS239 FGS258 FGS283 fastload,report daily 09-23h 55m
new cronJob(`00 55 09-23 * * 0-6`, function(){
//new cronJob(`00 25,55 07-23 * * 1-6`, function(){
    console.log(`cron102(2) ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} start //`);
    
    // // Twitter
    // console.log(`fgs258-twitter-posts start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);

    // // 2days ago
    // let target = moment().add(-2, "days").format('YYYYMMDD');
    // console.log(`   target date: ${target}`);
    // shell.exec(`node src/fgs258-twitter-posts.js -t ${target}`);
    
    // // yaeserday
    // target = moment().add(-1, "days").format('YYYYMMDD');
    // console.log(`   target date: ${target}`);
    // shell.exec('node src/fgs258-twitter-posts.js');

    // Instageam
    if( moment().day() <= 2 ){	// 日,月,火,水(数値)
        console.log(`fgs239-insta-posts start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
        //shell.exec('node src/fgs239-insta-posts.js');
    }

    console.log(`cron102(2) ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} end //`);
}, function(){
    // error
    console.log(`Error ${moment().format('YYYY-MM-DD HH:mm:ss')} : FGS239 FGS258 FGS283 fastload, report cron102(2)`);
}, true, 'Asia/Tokyo');

// (3) FGS239 GMS weekly sunday 13:25
new cronJob(`00 25 13 * * 0`, function(){
    console.log(`cron102(5) ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} start //`);
    
    console.log(`fgs239-insta-GMS start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
    shell.exec(`node src/fgs239-insta-GMS.js`);

    console.log(`cron102(3) ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} end //`);
}, function(){
    // error
    console.log(`Error ${moment().format('YYYY-MM-DD HH:mm:ss')} : FGS239 GMS Weekly cron102(3)`);
}, true, 'Asia/Tokyo');


// (4) FGS239 weekly friday 15:15
new cronJob(`00 15 15 * * 5`, function(){
    console.log(`cron102(4) ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} start //`);
    
    console.log(`fgs239-insta-posts all maxfile start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
    shell.exec(`node src/fgs239-insta-posts.js -p 0`);

    console.log(`cron102(4) ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} end //`);
}, function(){
    // error
    console.log(`Error ${moment().format('YYYY-MM-DD HH:mm:ss')} : fgs239-insta-posts all maxfile cron102(4)`);
}, true, 'Asia/Tokyo');


console.log('cron102 setting');







