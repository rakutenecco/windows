'use strict';

const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const shell = require('shelljs');
const co = require('co');

// parse args
const cli = commandArgs([
    { name: 'runtype', alias: 'r', type: Number }, // 1:load 2:report
    { name: 'filename', alias: 'f', type: String },
]);

let runtype = cli['runtype'] || 0;
if(runtype == 2 && cli['filename']) {
    console.log(` -r 2 -f cli['filename'] : ${cli['filename']} -r 1 Only`);
    return;
}
let targetdate = moment().day(0).add(-7, "days").format('YYYYMMDD'); // lastSunday -7days
let targetdate_hyphen = moment(targetdate).format('YYYY-MM-DD');
let targetdate_slash = targetdate_hyphen.replace(/-/g,'/');
let today = moment().format('YYYYMMDD');

//ex) FGS258_twitter_posts_all_week_20190602_03_01.txt
//ex) FGS258_twitter_posts_all_week_20190602_03_02.txt
//ex) FGS258_twitter_posts_all_week_20190602_03_03.txt
//ex) FGS258_twitter_posts_all_week_20190526_03_01.txt
//ex) FGS258_twitter_posts_all_week_20190526_03_02.txt
//ex) FGS258_twitter_posts_all_week_20190526_03_03.txt
//ex) FGS258_twitter_posts_new_week_20190606_20190602.txt
//ex) FGS258_twitter_posts_new_week_20190606_20190526.txt
//ex) FGS258_twitter_posts_new_week_20190606_20190519.txt

const alldiv = '03';
const ndiv = Number(alldiv);

let load_fnm = 'FGS258_twitter_posts_all_week';
let target_fnm = cli['filename'];
let icnt = 0;
//console.log('tmpfnm', tmpfnm, tmpfnm.split(/[_\.]/g));
if(target_fnm){
    let tmpfnm = target_fnm.split(/[_\.]/g);
    //console.log(tmpfnm, isFinite(tmpfnm[3]));
    if(tmpfnm[0] == 'FGS258' && tmpfnm[1] == 'twitter' && tmpfnm[2] == 'posts' && tmpfnm[4] == 'week' && tmpfnm[tmpfnm.length-1] == 'txt'){
        if(tmpfnm[3] == 'all'){
            console.log(moment(tmpfnm[5]).day());
            if(moment(tmpfnm[5]).day() > 0){
                console.log(` -f cli['filename'] : ${cli['filename']} not Sunday`);
                return;
            }
            if(tmpfnm[6] != alldiv){
                console.log(` -f cli['filename'] : ${cli['filename']} alldiv diffeerent`);
                return;
            }

        }else if(tmpfnm[3] == 'new'){
            console.log(moment(tmpfnm[6]).day());
            if(moment(tmpfnm[6]).day() > 0){
                console.log(` -f cli['filename'] : ${cli['filename']} not Sunday`);
                return;
            }
            load_fnm = `FGS258_twitter_posts_new_week`;
        }
    }else{
        console.log(` -f cli['filename'] : ${cli['filename']} File Format Error`);
        return;
    }
}

// const wago2 = moment(targetdate_hyphen).add(-7, "days").format('YYYY/MM/DD');
// const wago3 = moment(targetdate_hyphen).add(-14, "days").format('YYYY/MM/DD');
// const wago4 = moment(targetdate_hyphen).add(-21, "days").format('YYYY/MM/DD');
// const wago5 = moment(targetdate_hyphen).add(-28, "days").format('YYYY/MM/DD');
// const wago6 = moment(targetdate_hyphen).add(-35, "days").format('YYYY/MM/DD');

const wago = [
    { date: targetdate, slash: targetdate_slash },
    { date: moment(targetdate_hyphen).add(-7, "days").format('YYYYMMDD'), slash: moment(targetdate_hyphen).add(-7, "days").format('YYYY/MM/DD') },
    { date: moment(targetdate_hyphen).add(-14, "days").format('YYYYMMDD'), slash: moment(targetdate_hyphen).add(-14, "days").format('YYYY/MM/DD') },
    { date: moment(targetdate_hyphen).add(-21, "days").format('YYYYMMDD'), slash: moment(targetdate_hyphen).add(-21, "days").format('YYYY/MM/DD') },
    { date: moment(targetdate_hyphen).add(-28, "days").format('YYYYMMDD'), slash: moment(targetdate_hyphen).add(-28, "days").format('YYYY/MM/DD') },
    //{ date: moment(targetdate_hyphen).add(-35, "days").format('YYYYMMDD'), slash: moment(targetdate_hyphen).add(-35, "days").format('YYYY/MM/DD') },
];

const prgrm = 'FGS258_twitter';
const share_path = 'D:/share/'; 

const bteq_path = `D:/work/tool/tools/bteq/${prgrm}/`;
const box_path = `D:/Box Sync/EC Consulting Department/ecc_fashion/${prgrm}/`;
const fl_fd = 'fastload/';
const rp_fd = 'report/';
const av_fd = 'average/';

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

const tbls = [
    { ctgry: 'food', datatbl: 'food_FGS258_twitter_posts_week', kwdtbl: 'food_FGS239_targets', mnpst: '70' },
    { ctgry: 'fashion', datatbl: 'fashion_FGS258_twitter_posts_fashion_week', kwdtbl: 'fashion_FGS239_targets_fashion', mnpst: '70' },
    { ctgry: 'watch', datatbl: 'fashion_FGS258_twitter_posts_watch_week', kwdtbl: 'fashion_FGS239_targets_watch', mnpst: '70' },
    { ctgry: 'flower', datatbl: 'flower_FGS258_twitter_posts_week', kwdtbl: 'flower_FGS239_targets', mnpst: '70' },
    { ctgry: 'interior', datatbl: 'interior_FGS258_twitter_posts_week', kwdtbl: 'interior_FGS239_targets', mnpst: '70' },
    { ctgry: 'kitchen', datatbl: 'kitchen_FGS258_twitter_posts_week', kwdtbl: 'kitchen_FGS239_targets', mnpst: '70' },
    { ctgry: 'life', datatbl: 'life_FGS258_twitter_posts_week', kwdtbl: 'life_FGS239_targets', mnpst: '70' },
    { ctgry: 'cosme', datatbl: 'cosme_FGS258_twitter_posts_week', kwdtbl: 'cosme_FGS239_targets', mnpst: '70' },
    { ctgry: 'wine', datatbl: 'wine_FGS258_twitter_posts_week', kwdtbl: 'wine_FGS239_targets', mnpst: '70' },
];

let rprt_fnm;

console.log(`/// FGS258-twitter-posts-week.js start /// ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
console.log(`  runtype   : ${runtype}`);
console.log(`  targetdate: ${targetdate_hyphen}`);
console.log(`  arg_fname : ${cli['filename']}`);

let fcnt;

// fastload
function fastload() {
    return new Promise((resolve) => {
        co(function*() {

            if(cli['filename']){
                let rt = yield load_check();
                if(rt) {
                    let rt2 = yield load();
                    if (rt2) yield insert();
                }
            }else{
                // FGS258_twitter_posts_all_week_20190602_03_01.txt ~ FGS258_twitter_posts_all_week_20190602_03_03.txt
                //  ...
                // FGS258_twitter_posts_all_week_20190428_03_01.txt ~ FGS258_twitter_posts_all_week_20190428_03_03.txt

                for (let i = 0; i < wago.length; i++) {
                    let icnt = 0;
                    do{
                        icnt++;
                        target_fnm = `${load_fnm}_${wago[i].date}_${alldiv}_0${icnt}.txt`;
                        let rt = yield load_check();
                        if(rt) {
                            let rt2 = yield load();
                            if (rt2) yield insert();
                        }
                    }while(icnt < ndiv)

                }
            }

            resolve();
        });
    });
};


// report
function report() {
    return new Promise((resolve) => {
        co(function*() {

        //    let rt = yield report_check();
        //    if(rt) {
        //        yield report_out();
                yield average_out();
        //    }

            resolve();
        });
    });
};


// load_check
function load_check() {

    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;
            let fname0 = `${share_path}${target_fnm}`;
            let fname1 = `${box_path}${fl_fd}${target_fnm}`;
            let fname2 = `${bteq_path}${fl_fd}${target_fnm}`;
            //console.log(`fname1 : ${fname1}`);
            // check files            
            if (fs.existsSync(fname0)) {
                if (fs.existsSync(fname1)) {
                    console.log(` fastload : ${fname1} done`);
                }else{
                    if (fs.existsSync(fname2)) {
                        console.log(` fastload : ${fname2} done`);
                    } else {
                        console.log(` share : ${fname0} exists`);
                        shell.cp(fname0, `${bteq_path}${fl_fd}`);
                        result = true;
                    }
                }
            } else {
                console.log(` share : ${fname0} not exists`);
            }
            
            if ( result ) {
                let lines = fs.readFileSync(`${bteq_path}${fl_fd}${target_fnm}`).toString().split("\n");
                if(lines[lines.length -1].length == 0) lines.pop();
                console.log(`  lines.length : ${lines.length}`);
                fcnt = lines.length;
            }

            resolve(result);
        });
    });
};

// load
function load() {

    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;
                
            let flstr = `
SET SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
    DATABASE SBX_ICB_ICBB;

    DROP TABLE fashion_${load_fnm}_pre;
    DROP TABLE fashion_${load_fnm}_pre_et;
    DROP TABLE fashion_${load_fnm}_pre_uv;

    CREATE TABLE fashion_${load_fnm}_pre
    (
        keyword varchar(500),
        category varchar(500),
        getdate VARCHAR(10),
        startdate VARCHAR(10),
        posts bigint
    )PRIMARY INDEX ( keyword );

    BEGIN LOADING fashion_${load_fnm}_pre ERRORFILES fashion_${load_fnm}_pre_et, fashion_${load_fnm}_pre_uv;
    set record vartext "	";
    DEFINE 
        keyword (varchar(500)),
        category (varchar(500)),
        getdate (VARCHAR(10)),
        startdate (VARCHAR(10)),
        posts (VARCHAR(20))
    FILE = ${bteq_path}${fl_fd}${target_fnm};

    INSERT INTO fashion_${load_fnm}_pre VALUES(
        :keyword,
        :category,
        :getdate,
        :startdate,
        :posts
    );
    END LOADING;

.EXIT
`;  
            //console.log(`fastload filename:${bteq_path}${fl_fd}${load_fnm}_load.txt`);
            fs.writeFileSync(`${bteq_path}${fl_fd}${load_fnm}_load.txt`, flstr);
            // Fastload
            console.log('  fastload 実行中');
            shell.exec(`fastload < ${bteq_path}${fl_fd}${load_fnm}_load.txt > ${bteq_path}${fl_fd}${target_fnm}_load.log`);

            let rt = yield load_count();
            if( rt ) {
                result = true;
            } else {
                console.log('  fastload failed');
            }

            resolve(result);
        });
    });
};
    
    
// load_count
function load_count() {

    return new Promise((resolve) => {
        co(function*() {
            
            let result = false;

            let btqfile = `${bteq_path}${fl_fd}${load_fnm}_loadcount.txt`;
            let btqlog = `${bteq_path}${fl_fd}${load_fnm}_loadcount.log`;
            let cntfile = `${bteq_path}${fl_fd}${load_fnm}_loadcount_result.txt`;
            shell.rm(cntfile);

            // select count
            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${cntfile}
.RECORDMODE OFF
DATABASE SBX_ICB_ICBB;
SELECT
    Trim(x.cnt)
FROM 
    ( SELECT count(*) cnt 
        FROM fashion_${load_fnm}_pre
    ) x;
    
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(btqfile, bteq);
            console.log(`  load count 実行中   :  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);

            let data = fs.readFileSync(cntfile, 'utf8');
            let cnt = data.match(/\d{1,}/);

            if( cnt == fcnt){
                console.log(`   fastload : ${cnt}件`);
                result = true;
            }
            
            resolve(result);
        });
    });
};


// insert into each table
function insert() {

    return new Promise((resolve) => {
        co(function*() {
            
            let result = false;
            // insert
            for (let i = 0; i < tbls.length; i++) {

                let iststr = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;
    DATABASE SBX_ICB_ICBB;

    INSERT INTO ${tbls[i].datatbl}

    SELECT keyword, max(category), max(getdate), startdate, max(posts)
    FROM fashion_${load_fnm}_pre a
    WHERE NOT EXISTS 
        (SELECT * FROM ${tbls[i].datatbl} b 
            WHERE a.keyword = b.keyword AND a.startdate = b.startdate )
    AND a.keyword in (select keyword from ${tbls[i].kwdtbl})
    GROUP BY keyword, postdate;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;    
                fs.writeFileSync(`${bteq_path}${fl_fd}insert_${load_fnm}_${tbls[i].ctgry}.txt`, iststr);
                console.log(`  insert 実行中 : ${tbls[i].ctgry}  :  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                shell.exec(`bteq < ${bteq_path}${fl_fd}insert_${load_fnm}_${tbls[i].ctgry}.txt > ${bteq_path}${fl_fd}insert_${load_fnm}_${tbls[i].ctgry}.log`);
            }

            shell.cp(`${bteq_path}${fl_fd}${target_fnm}`, `${box_path}${fl_fd}`);            
            result = true;
            resolve(result);
        });
    });
};


// report_check
function report_check() {

    return new Promise((resolve) => {
        co(function*() {

            let result = false;
            let rt = true;
            for (let i = 0; i < tbls.length; i++) {
                rprt_fnm = `report_twitter_week_${tbls[i].ctgry}_${targetdate}.csv`;
                let f1 = `${box_path}${rp_fd}${rprt_fnm}`;

                if (fs.existsSync(f1)) {
                    console.log(`report : ${f1} done`);
                }else{
                    console.log(`report : ${f1} not exists`);
                    rt = false;
                }
            }

            if(!rt) {
                // load check
                rt = true;
                for (let i = 1; i <= ndiv; i++) {
                    target_fnm = `${load_fnm}_${targetdate}_${alldiv}_0${icnt}.txt`;
                    let f1 = `${box_path}${fl_fd}${target_fnm}`;

                    if (fs.existsSync(f1)) {
                        console.log(`loadfile : ${f1} done`);
                    }else{
                        console.log(`loadfile : ${f1} not exists`);
                        rt = false;
                    }
                }
                if(rt) result = true;
            }

            resolve(result);
        });
    });
};

// report 急上昇keywordのレポートを生成
function report_out() {

    return new Promise((resolve) => {
        co(function*() {

            console.log(`report output [${targetdate_slash}]  ${moment().format('YYYY/MM/DD HH:mm:ss')}  start`);

            for (let i = 0; i < tbls.length; i++) {
                rprt_fnm = `report_twitter_week_${tbls[i].ctgry}_${targetdate}.csv`;
                let f1 = `${box_path}${rp_fd}${rprt_fnm}`;

                if (!fs.existsSync(f1)) {

                    let reportfile = `${bteq_path}${rp_fd}${rprt_fnm}`;
                    let btqfile = `${bteq_path}${rp_fd}${rprt_fnm}.txt`;
                    let btqlog = `${bteq_path}${rp_fd}${rprt_fnm}_log.txt`;

                    let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${reportfile}
.RECORDMODE OFF
DATABASE SBX_ICB_ICBB;`;
            if(tbls[i].ctgry == 'food'){
                bteq = `${bteq}
SELECT 'keyword, 属性１, 属性２,投稿数　　　　　(先週),伸び率%　　　　　(先週),投稿数　　　　　(2週間前),伸び率%　　　　　(2週間前),投稿数　　　　　(3週間前),伸び率%　　　　　(3週間前),投稿数　　　　　(4週間前),伸び率%　　　　　(4週間前)';
SELECT
    '"' || Trim(x.kwd) || '",' ||
    Trim(x.attr1) || ',' ||
    Trim(x.attr2) || ',' ||`;

            }else {
                bteq = `${bteq}
SELECT 'keyword, 投稿数　　　　　(先週),伸び率%　　　　　(先週),投稿数　　　　　(2週間前),伸び率%　　　　　(2週間前),投稿数　　　　　(3週間前),伸び率%　　　　　(3週間前),投稿数　　　　　(4週間前),伸び率%　　　　　(4週間前)';
SELECT
    '"' || Trim(x.kwd) || '",' ||`;
                
            }
            bteq = `${bteq}
    Trim(x.w1) || ',' ||
    Trim(x.w1_rate) || ',' ||
    Trim(x.w2) || ',' ||
    Trim(x.w2_rate) || ',' ||
    Trim(x.w3) || ',' ||
    Trim(x.w3_rate) || ',' ||
    Trim(x.w4) || ',' ||
    Trim(x.w4_rate)
FROM (

    SELECT
    w1.keyword "kwd"
  , Coalesce( k.attribute1, '' ) "attr1"
  , Coalesce( k.attribute2, '' ) "attr2"
  ,w1.posts "w1"
  , Coalesce( (w1.posts - w2.posts) * 10000 / CASE w2.posts WHEN 0 THEN 1 ELSE w2.posts end , '-') "w1_rate"
  , Coalesce( w2.posts , '-') "w2"
  , Coalesce( (w2.posts - w3.posts) * 10000 / CASE w3.posts WHEN 0 THEN 1 ELSE w3.posts end , '-') "w2_rate"
  , Coalesce( w3.posts , '-') "w3"
  , Coalesce( (w3.posts - w4.posts) * 10000 / CASE w4.posts WHEN 0 THEN 1 ELSE w4.posts end , '-') "w3_rate"
  , Coalesce( w4.posts , '-') "w4"
  , Coalesce( (w4.posts - w5.posts) * 10000 / CASE w5.posts WHEN 0 THEN 1 ELSE w5.posts end , '-') "w4_rate"
    
  FROM
    ( SELECT keyword, posts
        FROM ${tbls[i].datatbl}
       WHERE startdate = '${targetdate_slash}' --'2019/06/02'
         AND posts >= ${tbls[i].mnpst}
    ) w1
 INNER JOIN
    ( SELECT keyword, posts
        FROM ${tbls[i].datatbl}
       WHERE startdate = '${wago[1].slash}' --'2019/05/26'
         --AND posts > 0
    ) w2
    ON w1.keyword = w2.keyword AND w1.posts-w2.posts > 0
  LEFT JOIN
    ( SELECT keyword, posts
        FROM ${tbls[i].datatbl}
       WHERE startdate = '${wago[2].slash}' --'2019/05/19'
    ) w3
    ON w3.keyword = w1.keyword
  LEFT JOIN
    ( SELECT keyword, posts
        FROM ${tbls[i].datatbl}
        WHERE startdate = '${wago[3].slash}' --'2019/05/12'
    ) w4
    ON w4.keyword = w1.keyword
  LEFT JOIN
    ( SELECT keyword, posts
        FROM ${tbls[i].datatbl}
        WHERE startdate = '${wago[2].slash}' --'2019/05/05'
    ) w5
    ON w5.keyword = w1.keyword
      
  LEFT JOIN
    ( SELECT Lower(Translate( keyword USING unicode_to_unicode_nfkc)) keyword
           , attribute1, attribute2
        FROM SBX_ICB_ICBB.food_FGS239_targets_test2
    ) k
    ON k.keyword = w1.keyword
  
  ) x
  ORDER BY "w1" DESC, "w1_rate" DESC, kwd;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;

//console.log(btqfile);
//console.log(bteq);
                    fs.writeFileSync(btqfile, bteq);

                    //  bteq 
                    console.log(`  report:${rprt_fnm} 出力中`);
                    shell.exec(`bteq < ${btqfile} > ${btqlog}`);
                
                    // bteq -> box
                    shell.cp(`${reportfile}`, `${box_path}${rp_fd}${rprt_fnm}`);
                }
            }

            console.log(`report output : [${targetdate_slash}]  ${moment().format('YYYY/MM/DD HH:mm:ss')}  end //`);            

            resolve();
        });
    });
};

// averageファイルの出力
function average_out() {

    return new Promise((resolve) => {
        co(function*() {

            let avrfnm = 'FGS258_twitter_targets_all_average.txt';
            console.log(`average output : ${avrfnm}    ${moment().format('YYYY/MM/DD HH:mm:ss')}  start`);

            let f1 = `${share_path}targets/${avrfnm}`;
            if (fs.existsSync(f1)) fs.renameSync(f1, `${f1}_${today}bak`);

            let maxfile = `${bteq_path}${av_fd}${avrfnm}`;
            let btqfile = `${bteq_path}${av_fd}btq_${avrfnm}`;
            let btqlog = `${bteq_path}${av_fd}btq_${avrfnm}.log`;

            if (fs.existsSync(maxfile)) fs.renameSync(maxfile, `${maxfile}_${today}bak`);

            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${maxfile}
.RECORDMODE OFF;
DATABASE SBX_ICB_ICBB;

SELECT Trim(k.keyword) || '///' || Trim(Coalesce( d.aveposts,'0'))
  FROM (
      SELECT DISTINCT a.keyword
        FROM (
            SELECT keyword FROM ${tbls[0].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[1].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[2].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[3].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[4].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[5].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[6].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[7].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[8].kwdtbl}
             ) a
        ) k
  LEFT JOIN (
      SELECT b.keyword, OReplace(Cast(Cast(Round(Ave(posts)) AS DECIMAL(20,0)) AS VARCHAR(20)), '.','') aveposts
        FROM (
            SELECT keyword,posts FROM ${tbls[0].datatbl} WHERE startdate >= '${wago[4].slash}' UNION ALL
            SELECT keyword,posts FROM ${tbls[1].datatbl} WHERE startdate >= '${wago[4].slash}' UNION ALL
            SELECT keyword,posts FROM ${tbls[2].datatbl} WHERE startdate >= '${wago[4].slash}' UNION ALL
            SELECT keyword,posts FROM ${tbls[3].datatbl} WHERE startdate >= '${wago[4].slash}' UNION ALL
            SELECT keyword,posts FROM ${tbls[4].datatbl} WHERE startdate >= '${wago[4].slash}' UNION ALL
            SELECT keyword,posts FROM ${tbls[5].datatbl} WHERE startdate >= '${wago[4].slash}' UNION ALL
            SELECT keyword,posts FROM ${tbls[6].datatbl} WHERE startdate >= '${wago[4].slash}' UNION ALL
            SELECT keyword,posts FROM ${tbls[7].datatbl} WHERE startdate >= '${wago[4].slash}' UNION ALL
            SELECT keyword,posts FROM ${tbls[8].datatbl} WHERE startdate >= '${wago[4].slash}' 
             ) b
       GROUP BY 1
        ) d
    ON k.keyword = d.keyword
 ORDER BY 1;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(btqfile, bteq);

            //  bteq 
            console.log('  average file 出力中');
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);
        
            // bteq -> share
            shell.cp(`${maxfile}`, `${share_path}targets/${avrfnm}`);

            console.log(`average file output : [${targetdate_slash}]  ${moment().format('YYYY/MM/DD HH:mm:ss')}  end //`);            

            resolve();
        });
    });
};

// run
co(function*() {
    
    let st = moment().format('YYYY/MM/DD HH:mm:ss');

    if (runtype == 0 || runtype == 1) {
        yield fastload();    
    }
    
    if (runtype == 0 || runtype == 2) {
        yield report();    
    }

    console.log(`/// FGS258-twitter-posts-week.js end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

});
