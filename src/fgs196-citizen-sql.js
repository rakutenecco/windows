'use strict';

const shell = require('shelljs');
const fs = require('fs-extra');
const moment = require('moment');
const iconv = require('iconv-lite');
const co = require('co');
const month0Start = moment().format('YYYY-MM-01');
const month1Start = moment().add(-1, "months").format('YYYY-MM-01');

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';


// 本体テーブルにinsert
// modelsテーブルにmerge
exports.setBteq_ctzn_ins = function(filepath) {
    
    let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

insert into SBX_ICB_ICBB.fashion_FGS196_citizen
select Cast( Cast( a.insert_date AS Format 'YYYYMMDD') AS CHAR(8) )
     , a.*
  from SBX_ICB_ICBB.fashion_FGS196_citizen_pre a
 where NOT EXISTS
       (SELECT * FROM SBX_ICB_ICBB.fashion_FGS196_citizen b 
         WHERE Cast( Cast( a.insert_date AS Format 'YYYYMMDD') AS CHAR(8) ) = b.getdate
           AND a.mpn_1 = b.mpn_1 );

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;


merge into SBX_ICB_ICBB.fashion_FGS196_citizen_models m
using (
    select mpn_1
         , Cast( Cast( insert_date AS Format 'YYYYMMDD') AS CHAR(8) ) as getdate
      from SBX_ICB_ICBB.fashion_FGS196_citizen_pre
      ) as p
   on m.mpn_1 = p.mpn_1

 when matched then
    update set getdate = p.getdate
      
 when not matched then
    insert ( mpn_1, getdate ) 
    values ( p.mpn_1, p.getdate );

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;

    fs.writeFileSync(filepath, bteq);
    
}

// output1（売上情報）を生成
exports.setBteq_ctzn_dtl = function(filepath, out1_path) {

    let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

DELETE SBX_ICB_ICBB.fashion_FGS196_citizen_detail ALL;
INSERT INTO SBX_ICB_ICBB.fashion_FGS196_citizen_detail

    select s.order_no
         , s.units
         , s.item_name

      from UA_VIEW_MK_ICHIBA.red_basket_detail_tbl s
     inner join UA_VIEW_MK_ICHIBA.item_genre_dimension201804 i
        on s.genre_id = i.genre_id
     where s.reg_datetime >= '${month1Start} 00:00:00'
       and s.reg_datetime < '${month0Start} 00:00:00'
       and s.sub_total_amt > 0
       and s.units > 0
       and s.cancel_datetime is null
       and i.g1 = 558929
       and (index(s.item_name , 'citizen') > 0 or index(s.item_name , 'シチズン') > 0)
       and index(s.item_name , 'まごころ長期修理保証') = 0;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.set width 20000;
.EXPORT DATA FILE=${out1_path}
.RECORDMODE OFF
SELECT 'MPN_1,times,order_num,all_units';
SELECT
    Trim(a.MPN_1)     || ',' ||
    Trim(a.times)     || ',' ||
    Trim(a.order_num) || ',' ||
    Trim(a.all_units)

  FROM (  
    SELECT
        m.mpn_1 AS MPN_1
      , m.getdate AS times
      , Count(DISTINCT c.order_no) order_num
      , Sum(Coalesce(c.units,0)) all_units

      FROM SBX_ICB_ICBB.fashion_FGS196_citizen_models m
      LEFT JOIN SBX_ICB_ICBB.fashion_FGS196_citizen_detail c
        ON INDEX(c.item_name , m.mpn_1) > 0
     GROUP BY 1,2
        ) a
 ORDER BY 1;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;

    fs.writeFileSync(filepath, bteq);
    
}

// output2（登録商品情報）を生成
exports.setBteq_ctzn_itm = function(filepath, out2_path) {
    
    return new Promise((resolve, reject) => {
        co(function* () {

            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

DELETE SBX_ICB_ICBB.fashion_FGS196_citizen_item ALL;
INSERT INTO SBX_ICB_ICBB.fashion_FGS196_citizen_item

    SELECT s.shop_id
         , s.item_id
         , s.genre_id
         , s.price
         , s.inventory_type
         , s.shop_url
         , s.item_name

      from UA_VIEW_MK_ICHIBA.ssd_normal_item s
     inner join UA_VIEW_MK_ICHIBA.item_genre_dimension201804 i
        on s.genre_id = i.genre_id
     where s.delete_flg = 0
       and s.limited_flg = 0
       and s.depot_flg = 0
       and s.purchase_flg = 1
       and s.shop_url not like 'Z_%' ESCAPE 'Z'
       and exists (select * from UA_VIEW_MK_ICHIBA.red_rms_shop_mst_base b 
           where s.shop_id = b.shop_id and b.open_flg = 1 )
       and i.g1 = 558929
       and (index(s.item_name , 'citizen') > 0 or index(s.item_name , 'シチズン') > 0)
       and index(s.item_name , 'まごころ長期修理保証') = 0;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.set width 20000;
.EXPORT DATA FILE=${out2_path}
.RECORDMODE OFF
SELECT 'mpn_1,times,item_num,price_max,price_min,item_num2,price_max2,price_min2';
SELECT
       Trim(a.mpn_1) || ',' ||
       Trim(a.times) || ',' ||
       Trim(a.ccnt)  || ',' ||
       Trim(a.cmax)  || ',' ||
       Trim(a.cmin)  || ',' ||
       Trim(a.c2cnt) || ',' ||
       Trim(a.c2max) || ',' ||
       Trim(a.c2min)
  FROM (  
      SELECT
             m.mpn_1 AS mpn_1
           , m.getdate AS times
           , Count(c.item_id) ccnt
           , Coalesce(Max(c.price),'') cmax
           , Coalesce(Min(c.price),'') cmin
           , Count(c2.item_id) c2cnt
           , Coalesce(Max(c2.price),'') c2max
           , Coalesce(Min(c2.price),'') c2min
        FROM SBX_ICB_ICBB.fashion_FGS196_citizen_models m
        LEFT JOIN SBX_ICB_ICBB.fashion_FGS196_citizen_item c
          ON INDEX(c.item_name , m.mpn_1) > 0
        LEFT JOIN (
            SELECT * FROM SBX_ICB_ICBB.fashion_FGS196_citizen_item
             WHERE inventory_type IN (0, 2, 4)
            ) c2
          ON INDEX(c2.item_name , m.mpn_1) > 0
       GROUP BY 1,2
       ) a
 ORDER BY 1;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;

            fs.writeFileSync(filepath, bteq);
    
            resolve();
        });
    });
}

// output3（sqlファイル）を生成
exports.setBteq_ctzn_sql = function(outpath) {
    
    return new Promise((resolve, reject) => {
        co(function* () {

            let text = `
SELECT m.mpn_1
     , c.shop_id
     , c.item_id
     , c.genre_id
     , c.price
     , c.inventory_type
     , c.shop_url
     , c.item_name

  FROM SBX_ICB_ICBB.fashion_FGS196_citizen_models m
  LEFT JOIN SBX_ICB_ICBB.fashion_FGS196_citizen_item c
    ON INDEX(c.item_name , m.mpn_1) > 0
--------------------------------------------------------------------------------
--    ここに、型番を記載して検索する
--    where m.mpn_1 in ('AT6060-00A','BV3-111 863244BELT ブルー')
--------------------------------------------------------------------------------
 ORDER BY m.mpn_1, c.price;
`;

            let buf = iconv.encode(text, "Shift_JIS" );
            fs.writeFileSync(outpath,buf);
    
            resolve();
        });
    });
}