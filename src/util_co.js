'use strict';

const moment = require('moment');
const fs = require('fs');
const shell = require('shelljs');
const isFile = require('is-file');
const isdir = require('isdir');
const _ = require('underscore');
const nodemailer = require('nodemailer');
const co = require('co');
const http = require('http');
const util = new (require('./util.js'));


// Create the transporter with the required configuration for Gmail
// change the user and pass !
let transporter = nodemailer.createTransport({
    host: 'smtp.office365.com',
    port: 587,
    // secure: true, // use SSL
    auth: {
        user: 'ts-masako.ikeda@rakuten.com',
        pass: '1234qwerT'
    }
});

// Util_co 関数生成
let Util_co = function() {
    this.title = 'util_co';
};

Util_co.prototype = util;




// mail to each person
Util_co.prototype.mailto = function(title, from, to, cc, text, filepath) {

    return new Promise((resolve) => {

    	// setup e-mail data
    	let mailOptions = {
    	    from: from, // sender address (who sends)
    	    to: to, // list of receivers (who receives)
            cc: cc,
    	    subject: title, // Subject line
    	    text: text
    	};

        // 
        if (filepath) {
            mailOptions.attachments = [
                {
                    path: filepath // stream this file
                }
            ];
        }

    	// send mail with defined transport object
    	transporter.sendMail(mailOptions, function(error, info){
    	    if(error){
    	        return console.log(error);
    	    }

    	    console.log('Message sent: ' + info.response);
            resolve();
    	});
    });
}


// get response data
Util_co.prototype.fastload = function(mes, project, filepath, isTime) {

    return new Promise((resolve) => {

        let column = [];
        let data = mes.data;
        let splits = filepath.split('/');
        let filename = splits[splits.length - 1].replace('.txt','');
        let filepath1 = filepath.replace('.txt','') + '_fastload_script.txt';
        let filepath2 = filepath.replace('.txt','') + '_fastload_data.txt';
        let filename2 = filename + '_fastload_data.txt';
        if (isFile(filepath1)) {
            fs.unlinkSync(filepath1);
        }
        if (isFile(filepath2)) {
            fs.unlinkSync(filepath2);
        }

        for (let i = 0; i < data.length; i++) {

            let vals = [];
            for (var key in data[i]) {
                if (data[i].hasOwnProperty(key)) {

                    if (i === 0) {
                        column.push({
                            name: key,
                            type: data[i][key]
                        });
                    }

                    if (i > 0) {
                        vals.push(data[i][key]);
                    }
                }
            }

            if (i === 0) { // set fastload script

                let script = this.TEMP_FASLOAD;
                let table = `fashion_${project}`;
                if (isTime) {
                    table = table + `_` + moment().format('YYYYMMDD');
                }

                let define = '';
                for (let j = 0; j < column.length; j++) {
                    define += column[j].name + ' ' + column[j].type;
                    if (j !== column.length - 1) {
                        define += ',';
                    }
                }

                let vol = '';
                for (let j = 0; j < column.length; j++) {
                    let type_chk = column[j].type.match(/VARCHAR/g);
                    let type_vol = type_chk ? ` (${column[j].type}),` : ' (VARCHAR(1000)),';
                    vol += column[j].name + type_vol;
                }

                let index = column[0].name;

                let path = this.DB_DIR + filename2; //config.DB_DIR + filepath2;

                let val = '';
                for (let j = 0; j < column.length; j++) {
                    val += ':' + column[j].name;
                    if (j !== column.length - 1) {
                        val += ',';
                    }
                }

                script = script.replace('{0}', this.DB_USER)
                    .replace(/\{1\}/g, table)
                    .replace('{2}', define)
                    .replace('{3}', index)
                    .replace('{4}', vol)
                    .replace('{5}', path)
                    .replace('{6}', val)
                    .replace('{7}', this.DB_PW)

                Util_co.prototype.insertTxt(filepath1, script);

            } else { // set fastload txt file

                let row = vals.join('	') + '\n';
                Util_co.prototype.insertTxt(filepath2, row);

                // 最終行でfastload実行
                if (i === data.length - 1) {
                    console.log('fastload 設定');
                    let dbdir = this.DB_DIR;
                    setTimeout(function() {
                        shell.exec(`fastload < ${dbdir}${filename}_fastload_script.txt > ${dbdir}${project}_log.txt`);
                        console.log('fastload 実行');
                        resolve();
                    }, 3000);
                }
            }
        }
    });
}



Util_co.prototype.getBrandList = function(filepath, {
    ids,
    nums,
    categories,
    names,
    from,
    to
}, cb) {

    return new Promise((resolve) => {

        try {

            fs.readFile(filepath, 'utf8', function(err, data) {

                if (err) throw err;
                let cnt = 0;
                let heads = [];
                let brands = [];
                let rows = data.replace(/\r/g, '').split('\n');

                rows.forEach(function(row, i) {

                    // 日本語除外
                    if (i < 1) return;

                    let brand = {};
                    let cells = row.split('	');

                    // i=1の場合はキーにする。
                    if (i === 1) {
                        heads = cells;

                    } else {
                        for (let j = 0; j < cells.length; j++) {
                            brand[heads[j]] = cells[j];
                        }

                        // 除外処理
                        if (brand.delflag === 1) return;

                        // ids, nums, categories, names
                        if (
                            ( // delflagの除外
                                (brand.delflag === 1)
                            ) ||
                            (!( // ids, nums, categories, namesの指定内容を合わせる
                                    (ids.indexOf(brand.id) >= 0) ||
                                    (nums.indexOf(brand.num) >= 0) ||
                                    (categories.indexOf(brand.category) >= 0) ||
                                    (names.indexOf(brand.name) >= 0)
                                ) &&
                                !( // すべてに指定がなかった場合は全対象の例外
                                    (ids.length === 0) &&
                                    (nums.length === 0) &&
                                    (categories.length === 0) &&
                                    (names.length === 0)
                                )
                            )
                        ) {
                            return;
                        }

                        cnt++;

                        if ( // cntが範囲内であること(to==-1は全範囲)
                            !(cnt >= from && cnt < to) &&
                            to > 0
                        ) {
                            return;
                        }


                        brands.push(brand);
                    }
                });
                // call back
                resolve(brands);
            });

        } catch (e) {
            console.log('ERROR: PASS');
        }
    });
}



Util_co.prototype.mkDir = function(dirPath) {

    return new Promise((resolve) => {

        isdir(dirPath, function callback(err, dir) { // named callback function 
            if(err) {
                fs.mkdirSync(dirPath);
            } else if(dir) {

            } else {
                fs.mkdirSync(dirPath);
            }
            resolve();
        });
    });
}



Util_co.prototype.getHTTP = function(url) {

    return new Promise((resolve, reject) => {

        http.get(url, (res) => {
            let body = '';
            res.setEncoding('utf8');

            res.on('data', (chunk) => {
                body += chunk;
            });

            res.on('end', (res) => {
                try {
                    res = JSON.parse(body);
                    resolve(res);
                } catch (e) {
                    resolve({
                        err: `【Error】 >>> ${body}`
                    });
                }
            });
        }).on('error', (e) => {
            resolve(e);
        });
    });
}












module.exports = Util_co;