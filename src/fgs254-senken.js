'use strict';

const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const shell = require('shelljs');
const co = require('co');
const util = new (require('./util_co.js'));
let today = moment().format('YYYYMMDD');

// parse args
const cli = commandArgs([
   { name: 'time', alias: 't', type: String }
]);


// reset time
if (cli['time']) {
    today = cli['time'];
}

const tools_path = 'D:/work/tool/tools/'; 
const share_path = 'D:/share/'; 

const FGS = 'FGS254';

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

// init
function init() {

    return new Promise((resolve) => {
        co(function*() {

            // rm 
            shell.rm(`${tools_path}output/*${FGS}*`);
            
            // share -> input_fastload            
            shell.cp(`${share_path}${FGS}-${today}.txt`, `${tools_path}input_fastload/${FGS}.txt`);

            resolve();
        });
    });
};


// do Fastload
function doFastload() {

    return new Promise((resolve) => {
        co(function*() {

            // Fastload
            shell.exec(`node src/makeFastload.js -p ${FGS}_pre -d`);

            resolve();
        });
    });
};


// do Insert
function doInsert() {

    return new Promise((resolve) => {
        co(function*() {

            let datapath = `${tools_path}output/${FGS}_fastload_data.txt`;
            if (fs.existsSync(datapath)) {
                // insert
                let filepath1 = `${tools_path}bteq/${FGS}.txt`;
                let bteq1 = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;

INSERT INTO SBX_ICB_ICBB.fashion_${FGS}_articles

SELECT *
  FROM SBX_ICB_ICBB.fashion_${FGS}_pre a
 WHERE NOT EXISTS 
       (SELECT * FROM SBX_ICB_ICBB.fashion_${FGS}_articles b 
         WHERE a.c_URL = b.c_URL AND a.c_publish = b.c_publish );

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
    `;

                fs.writeFileSync(filepath1, bteq1);
                shell.exec(`bteq < bteq/${FGS}.txt`);
            
            }else{
                let deletepath = `${tools_path}input_fastload/${FGS}.txt`;
                fs.unlink(deletepath);
            }
            resolve();
        });
    });
};


// run
co(function*() {

    yield init();
    yield doFastload();
    yield doInsert();

});











