'use strict';

const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const shell = require('shelljs');
const co = require('co');

// parse args
const cli = commandArgs([
    { name: 'type', alias: 'p', type: Number },
    { name: 'time', alias: 't', type: String }  // startdate YYYY-MM-DD
]);

const lastSunday = moment().subtract(6,'d').day(0).format('YYYY-MM-DD');
const startdate = cli['time'] ? cli['time'] : lastSunday;  //YYYY-MM-DD

const tlpath = 'D:/work/tool/tools/bteq/FGS239_insta_GMS/'; 
if(!fs.existsSync(tlpath)){
    fs.mkdirSync(tlpath);
} 
const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qweR';

// do Insert
function doInsert(tgttbl, totbl, clm, gr1, gr2, gr3) {
    return new Promise((resolve) => {
        co(function*() {

            //let dt = startdate;
            let dt = moment(startdate).day(0).format('YYYY-MM-DD');
            let sql = yield getSql(dt, lastSunday, tgttbl, totbl, clm, gr1, gr2, gr3);

            let filepath = `${tlpath}${totbl}_insert.txt`;
            fs.writeFileSync(filepath, sql);
            shell.exec(`bteq < ${filepath} > ${tlpath}${totbl}_insert_log_${moment().format('YYYYMMDD')}.txt`);

            resolve();
        });
    });
};

// do Insert food
function doInsert_food(frmtbl, totbl, genre1, genre2) {
    return new Promise((resolve) => {
        co(function*() {

            let dt;
            if (startdate) {
                dt = moment(startdate).day(0).format('YYYY-MM-DD');
            }else{
                dt = lastSunday;
            }

            let sql1 = yield getSql_food_insert(dt, lastSunday, frmtbl, totbl, genre1, genre2);

            let filepath1 = `${tools_path}bteq/${totbl}_food_insert.txt`;
            fs.writeFileSync(filepath1, sql1);
            shell.exec(`bteq < ${filepath1}`);

            // let sql2 = yield getSql_food_update(dt, lastSunday, totbl, genre2);

            // let filepath2 = `${tools_path}bteq/${totbl}_food_update.txt`;
            // fs.writeFileSync(filepath2, sql2);
            // shell.exec(`bteq < ${filepath2}`);

            resolve();
        });
    });
};

// get sql
const getSql = function (dt, lastSunday, tgttbl, totbl, clm, gr1, gr2, gr3) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
`;

            while(dt <= lastSunday){
                let dt8 = dt.replace(/[^0-9]/g, "");
                let nxtdt = moment(dt).add(7,'d').format('YYYY-MM-DD');
                sql = sql + `
INSERT INTO SBX_ICB_ICBB.${totbl}
    ( keyword, startdate, ${clm} )

SELECT a.kwd AS keyword
     , '${dt8}' AS startdate`;
                if(tgttbl == 'food_FGS239_targets'){
                    sql = sql + `
     , Coalesce(s.GMS,0) GMS_sweets
     , Coalesce(s.units,0) units_sweets
     , Coalesce(f.GMS,0) GMS_food
     , Coalesce(f.units,0) units_food`;
                } else if(tgttbl == 'fashion_FGS239_targets_fashion'){
                    sql = sql + `
     , Coalesce(s.GMS,0) GMS_ladies
     , Coalesce(s.units,0) units_ladies
     , Coalesce(f.GMS,0) GMS_mens
     , Coalesce(f.units,0) units_mens
     , Coalesce(g.GMS,0) GMS_bag
     , Coalesce(g.units,0) units_bag`;
                } else if(tgttbl == 'fashion_FGS239_targets_watch'){
                    sql = sql + `
     , Coalesce(s.GMS,0) GMS
     , Coalesce(s.units,0) units`;
                } else if(tgttbl == 'flower_FGS239_targets'){
                    sql = sql + `
     , Coalesce(s.GMS,0) GMS
     , Coalesce(s.units,0) units`;
                } else if(tgttbl == 'kitchen_FGS239_targets'){
                    sql = sql + `
     , Coalesce(s.GMS,0) GMS
     , Coalesce(s.units,0) units`;
                } else if(tgttbl == 'interior_FGS239_targets'){
                    sql = sql + `
     , Coalesce(s.GMS,0) GMS
     , Coalesce(s.units,0) units`;
                }
                sql = sql + `
  FROM ( SELECT DISTINCT Lower(keyword) as kwd FROM SBX_ICB_ICBB.${tgttbl} ) a

  LEFT JOIN
    ( SELECT k.kwd kwd, Sum(sub_total_amt) GMS, Sum(units) units
        FROM UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
       INNER JOIN  ( SELECT DISTINCT Lower(keyword) AS kwd FROM SBX_ICB_ICBB.${tgttbl} ) k
          ON INDEX(Lower(r.item_name) , k.kwd) > 0
       INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension i
          ON r.genre_id = i.genre_id ${gr1}
       WHERE r.reg_datetime >= '${dt} 00:00:00'
         AND r.reg_datetime < '${nxtdt} 00:00:00'
         AND r.price > 0
         AND r.units > 0
         AND r.cancel_datetime IS NULL
       GROUP BY k.kwd
    ) s
    ON a.kwd = s.kwd`;
                if(tgttbl == 'food_FGS239_targets' || tgttbl == 'fashion_FGS239_targets_fashion'){
                    sql = sql + `
  LEFT JOIN
    ( SELECT k.kwd kwd, Sum(sub_total_amt) GMS, Sum(units) units
        FROM UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
       INNER JOIN  ( SELECT DISTINCT Lower(keyword) AS kwd FROM SBX_ICB_ICBB.${tgttbl} ) k
          ON INDEX(Lower(r.item_name) , k.kwd) > 0
       INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension i
          ON r.genre_id = i.genre_id ${gr2}
       WHERE r.reg_datetime >= '${dt} 00:00:00'
         AND r.reg_datetime < '${nxtdt} 00:00:00'
         AND r.price > 0
         AND r.units > 0
         AND r.cancel_datetime IS NULL
       GROUP BY k.kwd
    ) f
    ON a.kwd = f.kwd`;
                }
                if(tgttbl == 'fashion_FGS239_targets_fashion'){
                    sql = sql + `
  LEFT JOIN
    ( SELECT k.kwd kwd, Sum(sub_total_amt) GMS, Sum(units) units
        FROM UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
       INNER JOIN  ( SELECT DISTINCT Lower(keyword) AS kwd FROM SBX_ICB_ICBB.${tgttbl} ) k
          ON INDEX(Lower(r.item_name) , k.kwd) > 0
       INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension i
          ON r.genre_id = i.genre_id ${gr3}
       WHERE r.reg_datetime >= '${dt} 00:00:00'
         AND r.reg_datetime < '${nxtdt} 00:00:00'
         AND r.price > 0
         AND r.units > 0
         AND r.cancel_datetime IS NULL
       GROUP BY k.kwd
    ) g
    ON a.kwd = g.kwd`;
                }
                sql = sql + `
 WHERE NOT EXISTS 
    (  SELECT * FROM SBX_ICB_ICBB.${totbl} b
        WHERE b.keyword = a.kwd AND b.startdate = '${dt8}');
`;   
        
                dt = moment(dt).add(7,'d').format('YYYY-MM-DD');  //7日後
            }

            sql = sql + `
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            resolve(sql);
        });
    });
};

// get sql food
const getSql_food_update = function (dt, lastSunday, totbl, genre1) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
`;

            while(dt <= lastSunday){
                let dt8 = dt.replace(/[^0-9]/g, "");
                let nxtdt = moment(dt).add(7,'d').format('YYYY-MM-DD');
                sql = sql + `
UPDATE a
FROM SBX_ICB_ICBB.food_${totbl} a,
(
   SELECT k.keyword AS keyword
        , Sum(Coalesce(f.sub_total_amt,0)) GMS_food
        , Sum(Coalesce(f.units,0)) units_food
    FROM ( SELECT keyword FROM SBX_ICB_ICBB.food_${totbl}
            WHERE startdate = '${dt8}' AND GMS_food IS NULL  ) k
    
    LEFT JOIN
    ( SELECT sub_total_amt
           , units
           , item_name
       FROM UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
      INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension201804 i
         ON r.genre_id = i.genre_id ${genre1}
      WHERE reg_datetime >= '${dt} 00:00:00'
        AND reg_datetime < '${nxtdt} 00:00:00'
        AND price > 0
        AND units > 0
        AND cancel_datetime IS NULL
    ) f
      ON INDEX(f.item_name , k.keyword) > 0
        
   GROUP BY k.keyword
) b

 SET GMS_food = b.GMS_food
   , units_food = b.units_food
    
WHERE a.keyword = b.keyword
  AND a.startdate = '${dt8}';
`;   
        
                dt = moment(dt).add(7,'d').format('YYYY-MM-DD');  //7日後
            }

            sql = sql + `
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            resolve(sql);
        });
    });
};

// do Insert fashion
function doInsert_fashion(frmtbl, totbl, genre) {
    return new Promise((resolve) => {
        co(function*() {

            let dt;
            if (startdate) {
                dt = moment(startdate).day(0).format('YYYY-MM-DD');
            }else{
                dt = lastSunday;
            }

            let sql = yield getSql_fashion(dt, lastSunday, frmtbl, totbl, genre);

            let filepath = `${tools_path}bteq/${totbl}_insert.txt`;
            fs.writeFileSync(filepath, sql);
            shell.exec(`bteq < ${filepath}`);

            resolve();
        });
    });
};

// get sql fashion
const getSql_fashion = function (dt, lastSunday, frmtbl, totbl, genre) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
`;

            while(dt <= lastSunday){
                let dt8 = dt.replace(/[^0-9]/g, "");
                let nxtdt = moment(dt).add(7,'d').format('YYYY-MM-DD');
                sql = sql + `
INSERT INTO SBX_ICB_ICBB.fashion_${totbl}

SELECT k.keyword AS keyword
     , Min(k.category) AS category
     , '${dt8}' AS startdate
     , Sum(Coalesce(d.sub_total_amt,0)) GMS
     , Sum(Coalesce(d.units,0)) units
  FROM ( SELECT DISTINCT keyword,category FROM SBX_ICB_ICBB.fashion_${frmtbl} ) k
  LEFT JOIN
   ( SELECT sub_total_amt
          , units
          , item_name
       FROM UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
      INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension201804 i
         ON r.genre_id = i.genre_id ${genre}
      WHERE reg_datetime >= '${dt} 00:00:00'
        AND reg_datetime < '${nxtdt} 00:00:00'
        AND price > 0
        AND units > 0
        AND cancel_datetime IS NULL
   ) d
    ON INDEX(d.item_name , k.keyword) > 0
 WHERE NOT EXISTS 
        (SELECT * FROM SBX_ICB_ICBB.fashion_${totbl} a
          WHERE a.keyword = k.keyword AND a.startdate = '${dt8}')
 GROUP BY k.keyword;
`;   
        
                dt = moment(dt).add(7,'d').format('YYYY-MM-DD');  //7日後
            }

            sql = sql + `
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            resolve(sql);
        });
    });
};

// run
co(function*() {

    if (!cli['type'] || cli['type'] == 1) {
        // instagram posts (food)
        let tgttbl = 'food_FGS239_targets';
        let totbl = 'food_FGS239_insta_GMS';
        let clm = 'GMS_sweets, units_sweets, GMS_food, units_food';
        let gr1 = 'AND i.g1 = 551167 --sweets';
        let gr2 = 'AND i.g1 = 100227 --food';
        yield doInsert(tgttbl, totbl, clm, gr1, gr2);
    }
    if (!cli['type'] || cli['type'] == 2) {
        // instagram posts (fashion)
        let tgttbl = 'fashion_FGS239_targets_fashion';
        let totbl = 'fashion_FGS239_insta_GMS_fashion';
        let clm = 'GMS_ladies, units_ladies, GMS_mens, units_mens, GMS_bag, units_bag';
        let gr1 = 'AND i.g1 = 100371 --ladies fashion';
        let gr2 = 'AND i.g1 = 551177 --mens fashion';
        let gr3 = 'AND i.g1 = 216131 --bag';
        yield doInsert(tgttbl, totbl, clm, gr1, gr2, gr3);
    }
    if (!cli['type'] || cli['type'] == 3) {
        // instagram posts (watch)
        let tgttbl = 'fashion_FGS239_targets_watch';
        let totbl = 'fashion_FGS239_insta_GMS_watch';
        let clm = 'GMS, units';
        let gr1 = 'AND i.g1 = 558929 --watch';        
        yield doInsert(tgttbl, totbl, clm, gr1);
    }
    if (!cli['type'] || cli['type'] == 4) {
        // instagram posts (flower)
        let tgttbl = 'flower_FGS239_targets';
        let totbl = 'flower_FGS239_insta_GMS';
        let clm = 'GMS, units';
        let gr1 = 'AND i.g1 = 100005 --flower';        
        yield doInsert(tgttbl, totbl, clm, gr1);
    }
    if (!cli['type'] || cli['type'] == 5) {
        // instagram posts (kitchen)
        let tgttbl = 'kitchen_FGS239_targets';
        let totbl = 'kitchen_FGS239_insta_GMS';
        let clm = 'GMS, units';
        let gr1 = 'AND i.g1 = 558944 --kitchen';        
        yield doInsert(tgttbl, totbl, clm, gr1);
    }
    if (!cli['type'] || cli['type'] == 6) {
        // instagram posts (interior)
        let tgttbl = 'interior_FGS239_targets';
        let totbl = 'interior_FGS239_insta_GMS';
        let clm = 'GMS, units';
        let gr1 = 'AND i.g1 = 100804 --interior';        
        yield doInsert(tgttbl, totbl, clm, gr1);
    }


    console.log('FGS239-insta-GMS end //');

});