'use strict';

const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const shell = require('shelljs');
const co = require('co');

// parse args
const cli = commandArgs([
    { name: 'targetdate', alias: 't', type: String },// yyyymmdd
    { name: 'type', alias: 'p', type: String },// 1:cookpad/2:rakuten
]);
if(cli['targetdate']){
    let day = moment(cli['targetdate']).day();
    if(day > 0){
        console.log(`-t cli['targetdate'] is not sunday : ${cli['targetdate']} : ${day}`);
        return;
    }
}
const targetdate = cli['targetdate'] ? cli['targetdate'] : moment().day(0).format('YYYYMMDD');//lastsunday
const targetdate_slash = `${targetdate.slice(0,4)}/${targetdate.slice(4,6)}/${targetdate.slice(6)}`;
const type = cli['type'] ? cli['type'] : 0;

const share_path = 'D:/share/'; 
//const fl_path = `D:\/work\/tool\/tools\/bteq\/FGS283_recipe\/`;
//const fls_path = 'D:/work/tool/tools/bteq/FGS283_recipe/';

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

const box_path = `D:\/Box Sync\/EC Consulting Department\/ecc_fashion\/FGS239_insta\/`;
const bteq_path = `D:\/work\/tool\/tools\/bteq\/FGS239_insta\/`;
const bteq_paths = `D:/work/tool/tools/bteq/FGS239_insta/`;
const fnm = `FGS283_recipe_posts`;
const tnm = `food_FGS283_recipe_posts`;
const rfnm = `report_recipe`;
const mx_fd = 'max/';

let tdate = targetdate_slash.replace(/\//g,'-');//before sunday
const tdate7 = moment(tdate).subtract(7, 'days').format('YYYYMMDD');
const tdate14 = moment(tdate).subtract(14, 'days').format('YYYYMMDD');
const tdate21 = moment(tdate).subtract(21, 'days').format('YYYYMMDD');
const tdate28 = moment(tdate).subtract(28, 'days').format('YYYYMMDD');
const tdate35 = moment(tdate).subtract(35, 'days').format('YYYYMMDD');
tdate = moment(tdate).format('YYYYMMDD');
console.log(' targetdate:',targetdate,tdate,tdate7,tdate14,tdate21,tdate28)

let rptfname;
const today = moment().format('YYYYMMDD');
const mdate = moment().add(-34, "days").format('YYYYMMDD');


// load_report
function load_report() {

    return new Promise((resolve) => {
        co(function*() {

            if(type == 0 || type == 1){
                //cookpad
                let rt = yield load_check('cookpad');
                let cnt = 0;
                if(!rt) {
                    let rt2 = yield load('cookpad');
                    if(rt2) cnt = yield load_count('cookpad');
                }
                if(rt || cnt > 0){
                    let rt3 = yield report_check('cookpad');
                    if(!rt3) {
                        yield report_out('cookpad');
                        yield max_output('cookpad');
                    }
                }
            }
            
            if(type == 0 || type == 2){
                //rakuten
                let rt = yield load_check('rakuten');
                let cnt = 0;
                if(!rt) {
                    let rt2 = yield load('rakuten');
                    if(rt2) cnt = yield load_count('rakuten');
                }
                if(rt || cnt > 0){
                    let rt3 = yield report_check('rakuten');
                    if(!rt3) {
                        yield report_out('rakuten');     
                        yield max_output('rakuten');           
                    }
                }
            }
            
            resolve();
        });
    });
};

// load_check
function load_check(tp) {

    return new Promise((resolve) => {
        co(function*() {
                         
            let result = false;
            let filename = `${fnm}_${tp}_total_${targetdate}.txt`;
            let f1 = `${box_path}fastload\/${filename}`;
            let f2 = `${bteq_path}fastload\/${filename}`;
            // check files
            if (fs.existsSync(f1)) {
                result = true;
                console.log(` ${f1} done`);
            }else{
                if (fs.existsSync(f2)) {
                    result = true;
                    console.log(` ${f2} done`);
                }
            }

            resolve(result);
        });
    });
};

// load
function load(tp) {

    return new Promise((resolve) => {
        co(function*() {

            let result = false;
            let filename = `${fnm}_${tp}_total`;
            let tablename = `${tnm}_${tp}_total`;

            // check target files
            if (fs.existsSync(`${share_path}${filename}_${targetdate}.txt`)) {
                console.log(` fasetload file : ${filename}_${targetdate}.txt`);
                console.log(` fasetload table : ${tablename}`);
                // share -> bteq
                shell.cp(`${share_path}${filename}_${targetdate}.txt`, `${bteq_path}fastload\/${filename}_${targetdate}.txt`);
                
                let flstr = `
SET SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
    DATABASE SBX_ICB_ICBB;

    DROP TABLE ${tablename}_pre;
    DROP TABLE ${tablename}_pre_et;
    DROP TABLE ${tablename}_pre_uv;

    CREATE TABLE ${tablename}_pre
    (
        keyword varchar(500) CaseSpecific,
        posts BIGINT,
        getdate varchar(8)
    )PRIMARY INDEX ( keyword );

    BEGIN LOADING ${tablename}_pre ERRORFILES ${tablename}_pre_et, ${tablename}_pre_uv;
    set record vartext "	";
    DEFINE 
        keyword (VARCHAR(500)),
        posts (VARCHAR(20)),
        getdate (varchar(8))
    FILE = ${bteq_paths}fastload/${filename}_${targetdate}.txt;

    INSERT INTO ${tablename}_pre VALUES(
        :keyword,
        :posts,
        :getdate
    );
    END LOADING;

.EXIT
`;
                    fs.writeFileSync(`${bteq_path}fastload\/${filename}_load.txt`, flstr);
                    // Fastload
                    console.log(`  fastload 実行中 ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                    shell.exec(`fastload < ${bteq_path}fastload\/${filename}_load.txt > ${bteq_path}fastload\/${filename}_load_log.txt`);
                
                    // insert
                    let iststr = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;
    DATABASE SBX_ICB_ICBB;

    INSERT INTO ${tablename}

    SELECT a.kwd, a.posts, a.getdate
      FROM 
        ( SELECT lower(keyword) kwd
               , max(posts) posts
               , max(getdate) getdate
            FROM ${tablename}_pre
           GROUP BY lower(keyword)
        ) a
     WHERE NOT EXISTS 
        ( SELECT * FROM ${tablename} b 
           WHERE a.kwd = b.keyword and a.getdate = b.getdate );
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
                fs.writeFileSync(`${bteq_path}fastload\/${filename}_insert.txt`, iststr);
                shell.exec(`bteq < ${bteq_path}fastload\/${filename}_insert.txt > ${bteq_path}fastload\/${filename}_insert_log.txt`);
                result = true;
            }else{
                console.log(` ${share_path}${filename}_${targetdate}.txt not exists`);
            }

            resolve(result);
        });
    });
};


// load_count
function load_count(tp) {

    return new Promise((resolve) => {
        co(function*() {
           
            let cnt;

            let filename = `${fnm}_${tp}_total`;
            let tablename = `${tnm}_${tp}_total`;

            let btqfile = `${bteq_path}fastload\/${filename}_count.txt`;
            let btqlog = `${bteq_path}fastload\/${filename}_count_log.txt`;
            let cntfile = `${bteq_path}fastload\/${filename}_count_result.txt`;
            shell.rm(cntfile);

            // select count
            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${cntfile}
.RECORDMODE OFF
SELECT
    Trim(x.cnt)
FROM 
  ( select count(*) cnt 
      from SBX_ICB_ICBB.${tablename}
     where getdate = '${targetdate}'
  ) x;
  
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(btqfile, bteq);
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);

            let data = fs.readFileSync(cntfile, 'utf8');
            cnt = data.match(/\d{1,}/);

            if(cnt > 0){                
                // share -> box,bteq 
                shell.cp(`${share_path}${filename}_${targetdate}.txt`, `${box_path}fastload\/${filename}_${targetdate}.txt`);
                shell.cp(`${share_path}${filename}_${targetdate}.txt`, `${bteq_path}fastload\/${filename}_${targetdate}.txt`);
            }

            resolve(cnt);
        });
    });
};


// report_check
function report_check(tp) {

    return new Promise((resolve) => {
        co(function*() {
                         
            let result = false;
            //let yesterday = moment(titledate.replace(/\//g,'-')).subtract(1, 'days').format('YYYYMMDD');//yesterday
            rptfname = `${rfnm}_${tp}_total_${targetdate}.csv`;
            let f1 = `${box_path}report\/${rptfname}`;
            let f2 = `${bteq_path}report\/${rptfname}`;
            //console.log(f1);
            //console.log(f2);
            // check files
            if (fs.existsSync(f1)) {
                result = true;
                console.log(` ${f1} done`);
            }else{
                if (fs.existsSync(f2)) {
                    result = true;
                    console.log(` ${f2} done`);
                }
            }

            resolve(result);
        });
    });
};

// report 急上昇keywordのレポートを生成
function report_out(tp) {

    return new Promise((resolve) => {
        co(function*() {

            let tname;
            let minposts;
            switch (tp) {

                case 'cookpad':
                    tname = `SBX_ICB_ICBB.${tnm}_cookpad_total`;
                    minposts = 30;
                    break;

                case 'rakuten':
                    tname = `SBX_ICB_ICBB.${tnm}_rakuten_total`;
                    minposts = 10;
                    break;
            }

            let btqfile = `${bteq_path}report\/${rfnm}_${tp}_total.txt`;
            let btqlog = `${bteq_path}report\/${rfnm}_${tp}_total_log.txt`;

            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${bteq_path}report\/${rptfname}
.RECORDMODE OFF
SELECT 'keyword, 属性１, 属性２, ${targetdate_slash}　　　　　総投稿数,割合%　　　　　(先週/総投稿数),投稿数　　　　　(先週),伸び率%　　　　　(先週),投稿数　　　　　(2週間前),伸び率%　　　　　(2週間前),投稿数　　　　　(3週間前),伸び率%　　　　　(3週間前),投稿数　　　　　(4週間前),伸び率%　　　　　(4週間前)';
SELECT
    Trim(x.kwd) || ',' ||
    Trim(x.attr1) || ',' ||
    Trim(x.attr2) || ',' ||
    Trim(x.a0) || ',' ||
    Trim(x.ttl_rate) || ',' ||
    Trim(x.w1) || ',' ||
    Trim(x.w1_rate) || ',' ||
    Trim(x.w2) || ',' ||
    Trim(x.w2_rate) || ',' ||
    Trim(x.w3) || ',' ||
    Trim(x.w3_rate) || ',' ||
    Trim(x.w4) || ',' ||
    Trim(x.w4_rate)
FROM (

    SELECT
        a0.keyword "kwd"
      , Coalesce( k.attribute1, '' ) "attr1"
      , Coalesce( k.attribute2, '' ) "attr2"
      , a0.posts "a0"
      , (a0.posts - a1.posts) * 10000 / a0.posts  "ttl_rate"

      , a0.posts - a1.posts "w1"
      , Coalesce( ((a0.posts - a1.posts) - (a1.posts - a2.posts)) * 10000 / CASE a1.posts WHEN a2.posts THEN 1 ELSE (a1.posts - a2.posts) end , '-') "w1_rate"

      , Coalesce( a1.posts - a2.posts  , '-') "w2"
      , Coalesce( ((a1.posts - a2.posts) - (a2.posts - a3.posts)) * 10000 / CASE a2.posts WHEN a3.posts THEN 1 ELSE  (a2.posts - a3.posts) END   , '-') "w2_rate"

      , Coalesce( a2.posts - a3.posts   , '-') "w3"
      , Coalesce( ((a2.posts - a3.posts) - (a3.posts - a4.posts)) * 10000 / CASE a3.posts WHEN a4.posts THEN 1 ELSE (a3.posts - a4.posts) END   , '-') "w3_rate"

      , Coalesce( a3.posts - a4.posts   , '-') "w4"
      , Coalesce( ((a3.posts - a4.posts) - (a4.posts - a5.posts)) * 10000 / CASE a4.posts WHEN a5.posts THEN 1 ELSE (a4.posts - a5.posts) END   , '-') "w4_rate"
      
    FROM
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate}'
           AND posts > 0
      ) a0
   INNER JOIN
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate7}'
           AND posts > 0
      ) a1
     ON a0.keyword = a1.keyword
   LEFT JOIN
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate14}'
           AND posts > 0
      ) a2
     ON a0.keyword = a2.keyword
   LEFT JOIN
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate21}'
           AND posts > 0
      ) a3
     ON a0.keyword = a3.keyword
   LEFT JOIN
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate28}'
           AND posts > 0
      ) a4
     ON a0.keyword = a4.keyword
   LEFT JOIN
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate35}'
           AND posts > 0
      ) a5
     ON a0.keyword = a5.keyword

   LEFT JOIN
      ( SELECT Lower(Translate( keyword USING unicode_to_unicode_nfkc)) keyword
             , attribute1, attribute2
          FROM SBX_ICB_ICBB.food_FGS239_targets_test2
      ) k
     ON k.keyword = a0.keyword
      
   WHERE a0.posts-a1.posts >= ${minposts}
     AND "w1"-"w2" > 0
     AND "w2" <> '-'
  
  ) x
  ORDER BY "w1" DESC,  "w1_rate" DESC, kwd;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;

//console.log(btqfile);
//console.log(bteq);
            fs.writeFileSync(btqfile, bteq);

            //yield setBteq(btqfile,fname,);
            // daily output
            //  bteq 
            
            console.log(`  report 出力中 ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);
        
            // bteq -> box
            shell.cp(`${bteq_path}report\/${rptfname}`, `${box_path}report\/${rptfname}`);                

            console.log(` ${rptfname} output end`);            

            resolve();
        });
    });
};


// max output
function max_output(tp) {

    return new Promise((resolve) => {
        co(function*() {

            let result = false;

            // FGS286_recipe_rakuten_max.txt
            // FGS286_recipe_cookpad_max.txt
            let maxfnm;
            if(tp == 'rakuten') {
                maxfnm = 'FGS283_recipe_rakuten_max.txt';

            }else if(tp == 'cookpad'){
                maxfnm = 'FGS283_recipe_cookpad_max.txt';

            }else{
                return;
            }

            console.log(` max file : ${maxfnm}`);
            
            let f1 = `${share_path}targets/${maxfnm}`;
            if (fs.existsSync(f1)) fs.renameSync(f1, `${f1}_${today}bak`);

            let maxfile = `${bteq_path}${mx_fd}${maxfnm}`;
            let btqfile = `${bteq_path}${mx_fd}btq_${maxfnm}`;
            let btqlog = `${bteq_path}${mx_fd}btq_${maxfnm}.log`;

            if (fs.existsSync(maxfile)) fs.renameSync(maxfile, `${maxfile}_${today}bak`);

            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${maxfile}
.RECORDMODE OFF;
DATABASE SBX_ICB_ICBB;

SELECT
    Trim(x.kwd) || '///' || Trim(x.mxpst)
  FROM (
    SELECT a.keyword kwd, Nvl(b.maxposts,0) mxpst
      FROM food_FGS239_targets a
      LEFT JOIN (
          SELECT keyword, Max(posts) maxposts
            FROM food_FGS283_recipe_posts_${tp}_total 
           WHERE getdate >= '${mdate}'
           GROUP BY 1) b
        ON a.keyword = b.keyword
     ) x
 ORDER BY 1;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT`;

            fs.writeFileSync(btqfile, bteq);

            //  bteq 
            console.log(`  maxfile 出力中 ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);
        
            // bteq -> share
            shell.cp(`${maxfile}`, `${share_path}targets/${maxfnm}`);                
            
            resolve();
        });
    });
};


// run
co(function*() {
    
    yield load_report();
    
    console.log(`FGS283-recipe  ${moment().format('YYYY/MM/DD HH:mm:ss')}  end //`);

});
