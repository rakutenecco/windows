'use strict';

const shell = require('shelljs');
const fs = require('fs-extra');
const moment = require('moment');
const co = require('co');
const commandArgs = require('command-line-args');
const today = moment().format('YYYYMMDD');
//const yesterday = moment().add(-1, 'days').format('YYYYMMDD');
// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: String }, // 0 /1:GMS/2:GMSYoY
    { name: 'targetdate', alias: 't', type: String }, // yyyymmdd today
]);

const runtype = cli['run']? cli['run'] : 0;
const targetdate = cli['targetdate'] ? cli['targetdate'] : today;  // YYYYMMDD
const targetdate_h = targetdate.substr(0, 4) + '-' + targetdate.substr(4, 2) + '-' + targetdate.substr(6, 2);  // yyyy-mm-dd GMS,GMSYoY,sql
const lastdate_h = moment(targetdate_h).add(-1, 'days').format('YYYY-MM-DD');  // yyyy-mm-dd  GMS sql case
const lastdate_s = lastdate_h.replace(/-/g,'/');  // yyyy/mm/dd  GMS,GMSYoY header
const _2daysago_h = moment(targetdate_h).add(-2, 'days').format('YYYY-MM-DD');  // yyyy-mm-dd  GMS sql where
const _2daysago_s = _2daysago_h.replace(/-/g,'/');  // yyyy/mm/dd  GMS header

const _30daysago_h = moment(targetdate_h).add(-30, 'days').format('YYYY-MM-DD');  // yyyy-mm-dd GMSYoY sql case
const _60daysago_h = moment(targetdate_h).add(-60, 'days').format('YYYY-MM-DD');  // yyyy-mm-dd GMSYoY sql case
const _90daysago_h = moment(targetdate_h).add(-90, 'days').format('YYYY-MM-DD');  // yyyy-mm-dd GMSYoY sql case
const _120daysago_h = moment(targetdate_h).add(-120, 'days').format('YYYY-MM-DD');  // yyyy-mm-dd GMSYoY sql where case

const targetdate_lastyear_h = moment(targetdate_h).add(-1, 'years').format('YYYY-MM-DD');  // yyyy-mm-dd GMSYoY sql where
const lastdate_lastyear_h = moment(targetdate_lastyear_h).add(-1, 'days').format('YYYY-MM-DD');  // yyyy-mm-dd GMSYoY sql where
const lastdate_lastyear_s = lastdate_lastyear_h.replace(/-/g,'/');  // yyyy/mm/dd GMSYoY header
const _30daysago_lastyear_h = moment(targetdate_lastyear_h).add(-30, 'days').format('YYYY-MM-DD');  // yyyy-mm-dd GMSYoY sql case
const _60daysago_lastyear_h = moment(targetdate_lastyear_h).add(-60, 'days').format('YYYY-MM-DD');  // yyyy-mm-dd GMSYoY sql case
const _90daysago_lastyear_h = moment(targetdate_lastyear_h).add(-90, 'days').format('YYYY-MM-DD');  // yyyy-mm-dd GMSYoY sql case
const _120daysago_lastyear_h = moment(targetdate_lastyear_h).add(-120, 'days').format('YYYY-MM-DD');  // yyyy-mm-dd GMSYoY sql where case


const GMSfrd = 'GMS';
let bteq_path = `D:\/work\/tool\/tools\/bteq\/FGS239_insta\/${GMSfrd}\/`;
let box_path = `D:\/Box Sync\/EC Consulting Department\/ecc_fashion\/FGS239_insta\/${GMSfrd}\/`;

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

let type;
let wlimit;

let fname;
let btqfile;
let btqlog;

// let title1;
// let title2;
// let from1;
// let until1;
// let from2;
// let until2;

// const t1 = targetdate;
// const t2 = lastdate;

// const t1y = targetdate + '　　　（過去30日間）';
// const t2y = lastyear + '　　　（過去30日間）';

// const where_today = `Cast( Cast( Current_Date AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`;
// const where_yesterday = `Cast( Cast( Current_Date -1 AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`;
// const where_2d_ago = `Cast( Cast( Current_Date -2 AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`;

// const where_30d_ago = `Cast( Cast( Current_Date -30 AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`;
// const where_60d_ago = `Cast( Cast( Current_Date -60 AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`;
// const where_90d_ago = `Cast( Cast( Current_Date -90 AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`;
// const where_120d_ago = `Cast( Cast( Current_Date -120 AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`;

// const where_today_lyr = `Cast( Cast( Add_Months(Current_Date, -12) AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`
// const where_30d_ago_lyr = `Cast( Cast( Add_Months(Current_Date, -12) -30 AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`;
// const where_60d_ago_lyr = `Cast( Cast( Add_Months(Current_Date, -12) -60 AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`;
// const where_90d_ago_lyr = `Cast( Cast( Add_Months(Current_Date, -12) -90 AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`;
// const where_120d_ago_lyr = `Cast( Cast( Add_Months(Current_Date, -12) -120 AS Format 'YYYY-MM-DD') AS CHAR(10) ) || ' 00:00:00'`;

const gr_sweets = 'g1 = 551167 --sweets';
const gr_food = 'g1 = 100227 --food';
const gr_ladies_fashion = 'g1 = 100371 --ladies fashion';
const gr_mens_fashion = 'g1 = 551177 --mens fashion';
const gr_bag = 'g1 = 216131 --bag';
const gr_flower = 'g1 = 100005 --flower';
const gr_kitchen = 'g1 = 558944 --kitchen';
const gr_interior = 'g1 = 100804 --interior';
const gr_watch = 'g1 = 558929 --watch';
const gr_cosme = 'g1 = 100939 --cosme';
const gr_wine = 'g1 = 510915 and g2 = 100317 --wine';

const keytbl_food = 'food_FGS239_targets';
const keytbl_fashion = 'fashion_FGS239_targets_fashion';
const keytbl_watch = 'fashion_FGS239_targets_watch';
const keytbl_flower = 'flower_FGS239_targets';
const keytbl_kitchen = 'kitchen_FGS239_targets';
const keytbl_interior = 'interior_FGS239_targets';
const keytbl_cosme = 'cosme_FGS239_targets';
const keytbl_wine = 'wine_FGS239_targets';


// output 急上昇keywordのレポートを生成
function output() {

    return new Promise((resolve) => {
        co(function*() {

            
            if (runtype == '0' || runtype == '1') {
                // GMS
                // title1 = t1; // yesterday
                // title2 = t2; //lastdate
                // from1 = where_yesterday;
                // until1 = where_today;
                // from2 = where_2d_ago;
                // until2 = where_yesterday;
                type = 'GMS'
                //wlimit = 'GMS_a >= 100000';
                //wlimit = 'rate1 >= 10000 AND GMS_a >= 100000';

                yield output_report('sweets');
                yield output_report('food');
                yield output_report('ladies_fashion');
                yield output_report('mens_fashion');
                yield output_report('bag');
                yield output_report('flower');
                yield output_report('kitchen');
                yield output_report('interior');
                yield output_report('watch');
                yield output_report('cosme');
                yield output_report('wine');
            }
            
            if (runtype == '0' || runtype == '2') {
                // GMSYoY                
                // title1 = t1y; // yesterday
                // title2 = t2y; // lastyear
                // from1 = where_30d_ago;
                // until1 = where_today;
                // from2 = where_30d_ago_lyr;
                // until2 = where_today_lyr;
                type = 'GMSYoY'                
                //wlimit = '3000000';

                yield output_report('sweets');
                yield output_report('ladies_fashion');
                yield output_report('mens_fashion');
                yield output_report('bag');
                yield output_report('flower');
                yield output_report('kitchen');
                yield output_report('interior');
                yield output_report('watch');
                yield output_report('food');
                yield output_report('cosme');
                yield output_report('wine');
            }

            resolve();
        });
    });
};


// output 急上昇keywordのレポートを生成
function output_report(gr) {

    return new Promise((resolve) => {
        co(function*() {

            if(!fs.existsSync(bteq_path)){
                fs.mkdirSync(bteq_path);
            } 
            if(!fs.existsSync(box_path)){
                fs.mkdirSync(box_path);
            } 

            fname = `report_${type}_${gr}_${lastdate_h.replace(/-/g,'')}.csv`;
            btqfile = `report_${type}_${gr}.txt`;
            btqlog = `report_${type}_${gr}_log_${lastdate_h.replace(/-/g,'')}.txt`;

            // out_csv check
            let rt = yield out_check();
            if(!rt) {
                
                yield setBteq_daily(gr);
                // daily output
                //  bteq 
                let st = moment().format('YYYY/MM/DD HH:mm:ss');
                shell.exec(`bteq < ${bteq_path}${btqfile} > ${bteq_path}${btqlog}`);
            
                // bteq -> box
                shell.cp(`${bteq_path}${fname}`, box_path);
                    

                console.log(`${fname} output end ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}//`);
            }
            resolve();
        });
    });
};


// load
function out_check() {

    return new Promise((resolve) => {
        co(function*() {
           
              
            let result = false;
            let f1 = `${box_path}${fname}`;
            let f2 = `${bteq_path}${fname}`;
            // check files
            if (fs.existsSync(f1)) {
                result = true;
                console.log(f1 + ' done');
            }else{
                if (fs.existsSync(f2)) {
                    result = true;
                    console.log(f2 + ' done');
                }
            }

            resolve(result);
        });
    });
};


// output dailyを生成
function setBteq_daily(gr) {
    return new Promise((resolve) => {
        co(function*() {
            console.log(type,gr);
            let bteq;
            let wgnr;
            let keytbl;

            if(gr == 'sweets'){
                wgnr = gr_sweets;
                keytbl = keytbl_food;
            }else if(gr == 'food'){
                wgnr = gr_food;
                keytbl = keytbl_food;
            }else if(gr == 'ladies_fashion'){
                wgnr = gr_ladies_fashion;
                keytbl = keytbl_fashion;
            }else if(gr == 'mens_fashion'){
                wgnr = gr_mens_fashion;
                keytbl = keytbl_fashion;
            }else if(gr == 'bag'){
                wgnr = gr_bag;
                keytbl = keytbl_fashion;
            }else if(gr == 'flower'){
                wgnr = gr_flower;
                keytbl = keytbl_flower;
            }else if(gr == 'kitchen'){
                wgnr = gr_kitchen;
                keytbl = keytbl_kitchen;
            }else if(gr == 'interior'){
                wgnr = gr_interior;
                keytbl = keytbl_interior;
            }else if(gr == 'watch'){
                wgnr = gr_watch;
                keytbl = keytbl_watch;
            }else if(gr == 'cosme'){
                wgnr = gr_cosme;
                keytbl = keytbl_cosme;
            }else if(gr == 'wine'){
                wgnr = gr_wine;
                keytbl = keytbl_wine;
            }
            bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${bteq_path}${fname}
.RECORDMODE OFF
`;
            if(type == 'GMS'){

                if(gr == 'sweets' || gr == 'food'){
                    bteq = `${bteq}
SELECT 'keyword, 属性１, 属性２, ${_2daysago_s}, ${lastdate_s}, 伸び率%, ${_2daysago_s}, ${lastdate_s}, 伸び率%';`;

                }else {
                    bteq = `${bteq}
SELECT 'keyword, ${_2daysago_s}, ${lastdate_s}, 伸び率%, ${_2daysago_s}, ${lastdate_s}, 伸び率%';`;

                }

            }else if(type == 'GMSYoY'){
                
                if(gr == 'sweets' || gr == 'food'){
                    bteq = `${bteq}
SELECT 'keyword, 属性１, 属性２, ${lastdate_lastyear_s}　　　（過去30日間）, ${lastdate_s}　　　（過去30日間）, 伸び率%　（過去30日間）, 伸び率%（31日前から過去30日間）, 伸び率%（61日前から過去30日間）, 伸び率%（91日前から過去30日間）, ${lastdate_lastyear_s}　　　（過去30日間）, ${lastdate_s}　　　（過去30日間）, 伸び率%';`;

                }else {
                    bteq = `${bteq}
SELECT 'keyword, ${lastdate_lastyear_s}　　　（過去30日間）, ${lastdate_s}　　　（過去30日間）, 伸び率%　（過去30日間）, 伸び率%（31日前から過去30日間）, 伸び率%（61日前から過去30日間）, 伸び率%（91日前から過去30日間）, ${lastdate_lastyear_s}　　　（過去30日間）, ${lastdate_s}　　　（過去30日間）, 伸び率%';`;

                }
            }

            bteq = `${bteq}

--create keywords
CREATE VOLATILE MULTISET TABLE FGS239_keywords AS 
(
--SELECT DISTINCT
--       RegExp_Replace(Lower(keyword) ,'[ー‐―−]','-',1,0,'c') (VARCHAR (50)) AS keyword
--  FROM SBX_ICB_ICBB.${keytbl}

SELECT DISTINCT
       RegExp_Replace(Lower(a.keyword) ,'[ー‐―−]','-',1,0,'c') (VARCHAR (50)) AS keyword
     , b.attribute1 (VARCHAR (20)) AS attribute1
     , b.attribute2 (VARCHAR (20)) AS attribute2
  FROM SBX_ICB_ICBB.${keytbl} a
  LEFT JOIN ( SELECT Lower(Translate( keyword USING unicode_to_unicode_nfkc)) keyword
                   , attribute1, attribute2
                FROM SBX_ICB_ICBB.${keytbl}_test2 ) b
    ON a.keyword = b.keyword

) WITH DATA
NO PRIMARY INDEX
ON COMMIT PRESERVE ROWS
;

--create orders
CREATE VOLATILE MULTISET TABLE FGS239_orders AS
(
SELECT `;

            if(type == 'GMS'){
                bteq = `${bteq}
    CASE WHEN reg_datetime >= '${lastdate_h} 00:00:00' THEN 'a01'
        ELSE 'b01' END (CHAR(3)) AS term `;

            }else if(type == 'GMSYoY'){
                bteq = `${bteq}
    CASE WHEN reg_datetime >= '${_30daysago_h} 00:00:00' THEN 'a01'
        WHEN reg_datetime >= '${_60daysago_h} 00:00:00' THEN 'a31'
        WHEN reg_datetime >= '${_90daysago_h} 00:00:00' THEN 'a61'
        WHEN reg_datetime >= '${_120daysago_h} 00:00:00' THEN 'a91'
        WHEN reg_datetime >= '${_30daysago_lastyear_h} 00:00:00' THEN 'b01'
        WHEN reg_datetime >= '${_60daysago_lastyear_h} 00:00:00' THEN 'b31'
        WHEN reg_datetime >= '${_90daysago_lastyear_h} 00:00:00' THEN 'b61'
        ELSE 'b91' End (CHAR(3)) AS term `;
            }
            bteq = `${bteq}
    , RegExp_Replace(Lower(Translate( item_name USING unicode_to_unicode_nfkc)) ,'[ー‐―−]','-',1,0,'c') (VARCHAR (250)) AS item_name
    , Sum(sub_total_amt (BIGINT)) AS gms
    , Sum(units (BIGINT)) AS units
FROM ua_view_mk_ichiba.red_basket_detail_tbl a
INNER JOIN ua_view_mk_ichiba.item_genre_dimension b
ON a.genre_id = b.genre_id
AND ${wgnr} `;

            if(type == 'GMS'){
                bteq = `${bteq}
AND reg_datetime >= '${_2daysago_h} 00:00:00'
AND reg_datetime <  '${targetdate_h} 00:00:00'`;

            }else if(type == 'GMSYoY'){
                bteq = `${bteq}               
AND ( ( reg_datetime >= '${_120daysago_h} 00:00:00' AND reg_datetime < '${targetdate_h} 00:00:00' )
    OR ( reg_datetime >= '${_120daysago_lastyear_h} 00:00:00' AND reg_datetime <  '${targetdate_lastyear_h} 00:00:00' )
    ) `;
            }            
                   
            bteq = `${bteq}
AND price > 0
AND units > 0
AND cancel_datetime IS NULL

GROUP BY 1,2
) WITH DATA
NO PRIMARY INDEX
ON COMMIT PRESERVE ROWS
;`;

            if(type == 'GMSYoY'){
                bteq = `${bteq}            
--del keywords
DELETE FROM FGS239_keywords WHERE keyword NOT IN
(
SELECT a01.keyword
    FROM  (SELECT a.keyword AS keyword, Sum(gms) AS gms
            FROM FGS239_keywords a
            INNER JOIN FGS239_orders b
                ON Position(a.keyword IN b.item_name) > 0
            AND b.term = 'a01'
            GROUP BY 1
        ) a01
    LEFT JOIN
        (SELECT a.keyword AS keyword, Sum(gms) AS gms
            FROM FGS239_keywords a
            INNER JOIN FGS239_orders b
                ON Position(a.keyword IN b.item_name) > 0
            AND b.term = 'b01'
            GROUP BY 1
        ) b01
    ON a01.keyword = b01.keyword
    WHERE a01.gms >= 3000000
    AND a01.gms - Nvl(b01.gms,0) > 0
);`;
            }

            bteq = `${bteq}

--select output report
SELECT
    Trim(x.kwd) || ',' ||`;

            if(gr == 'sweets' || gr == 'food'){
                bteq = `${bteq}
    Trim(x.attr1) || ',' ||
    Trim(x.attr2) || ',' ||`;
            }
            
            bteq = `${bteq}
    Trim(x.GMS_b) || ',' ||
    Trim(x.GMS_a) || ',' ||
    Trim(x.rate1 ) || ',' ||`;
            if(type == 'GMSYoY'){
                bteq = `${bteq}
    Trim(x.rate31 ) || ',' ||
    Trim(x.rate61 ) || ',' ||
    Trim(x.rate91 ) || ',' ||`;
            }
            bteq = `${bteq}
    Trim(x.units_b) || ',' ||
    Trim(x.units_a) || ',' ||
    Trim(x.rate2 )
FROM (

    SELECT oreplace(k.keyword,'-','ー') AS kwd
         , Coalesce(k.attribute1, '') attr1
         , Coalesce(k.attribute2, '') attr2
         , Nvl(b01.GMS,0) GMS_b
         , a01.GMS GMS_a
         , (a01.GMS- Nvl(b01.GMS,0)) * 10000 / CASE Nvl(b01.GMS,0) WHEN 0 THEN 1 ELSE b01.GMS END "rate1" `;

            if(type == 'GMSYoY'){
                bteq = `${bteq}
         , (Nvl(a31.GMS,0)- Nvl(b31.GMS,0)) * 10000 / CASE Nvl(b31.GMS,0) WHEN 0 THEN 1 ELSE b31.GMS END "rate31"
         , (Nvl(a61.GMS,0)- Nvl(b61.GMS,0)) * 10000 / CASE Nvl(b61.GMS,0) WHEN 0 THEN 1 ELSE b61.GMS END "rate61"
         , (Nvl(a91.GMS,0)- Nvl(b91.GMS,0)) * 10000 / CASE Nvl(b91.GMS,0) WHEN 0 THEN 1 ELSE b91.GMS END "rate91" `;
            }
    
            bteq = `${bteq}                 
         , Nvl(b01.units,0) units_b
         , a01.units units_a
         , (a01.units-Nvl(b01.units,0)) * 10000 / CASE Nvl(b01.units,0) WHEN 0 THEN 1 ELSE b01.units END "rate2"
              
      FROM FGS239_keywords k
     INNER JOIN
        ( SELECT a.keyword  AS keyword, Sum(gms) AS gms, Sum(units) AS units
            FROM FGS239_keywords a
            INNER JOIN FGS239_orders b
                ON Position(a.keyword IN b.item_name) > 0
            AND b.term = 'a01'
            GROUP BY 1) AS a01 ON k.keyword = a01.keyword
      LEFT JOIN
        ( SELECT a.keyword  AS keyword, Sum(gms) AS gms, Sum(units) AS units
            FROM FGS239_keywords a
            INNER JOIN FGS239_orders b
                ON Position(a.keyword IN b.item_name) > 0
            AND b.term = 'b01'
            GROUP BY 1) AS b01 ON k.keyword = b01.keyword `;

            if(type == 'GMSYoY'){
                bteq = `${bteq}
      LEFT JOIN
        ( SELECT a.keyword  AS keyword, Sum(gms) AS gms
            FROM FGS239_keywords a
            INNER JOIN FGS239_orders b
                ON Position(a.keyword IN b.item_name) > 0
            AND b.term = 'a31'
            GROUP BY 1) AS a31 ON k.keyword = a31.keyword
      LEFT JOIN
        ( SELECT a.keyword  AS keyword, Sum(gms) AS gms
            FROM FGS239_keywords a
            INNER JOIN FGS239_orders b
                ON Position(a.keyword IN b.item_name) > 0
            AND b.term = 'b31'
            GROUP BY 1) AS b31 ON k.keyword = b31.keyword
      LEFT JOIN
        ( SELECT a.keyword  AS keyword, Sum(gms) AS gms
            FROM FGS239_keywords a
            INNER JOIN FGS239_orders b
                ON Position(a.keyword IN b.item_name) > 0
            AND b.term = 'a61'
            GROUP BY 1) AS a61 ON k.keyword = a61.keyword
      LEFT JOIN
        ( SELECT a.keyword  AS keyword, Sum(gms) AS gms
            FROM FGS239_keywords a
            INNER JOIN FGS239_orders b
                ON Position(a.keyword IN b.item_name) > 0
            AND b.term = 'b61'
            GROUP BY 1) AS b61 ON k.keyword = b61.keyword
      LEFT JOIN
        ( SELECT a.keyword  AS keyword, Sum(gms) AS gms
            FROM FGS239_keywords a
            INNER JOIN FGS239_orders b
                ON Position(a.keyword IN b.item_name) > 0
            AND b.term = 'a91'
            GROUP BY 1) AS a91 ON k.keyword = a91.keyword
      LEFT JOIN
        ( SELECT a.keyword  AS keyword, Sum(gms) AS gms
            FROM FGS239_keywords a
            INNER JOIN FGS239_orders b
                ON Position(a.keyword IN b.item_name) > 0
            AND b.term = 'b91'
            GROUP BY 1) AS b91 ON k.keyword = b91.keyword `;
            }

            if(type == 'GMS'){
                bteq = `${bteq}            
     WHERE rate1 >= 10000
       AND GMS_a >= 100000 
       AND GMS_a - GMS_b > 0 `;
            }
    
            bteq = `${bteq}
    ) x
ORDER BY x.GMS_a DESC, x.rate1 DESC, x.kwd;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            
//// 
//             bteq = `${bteq} 


// WITH k AS
//  ( SELECT DISTINCT Lower(keyword) keyword FROM SBX_ICB_ICBB.${keytbl} ) 
// SELECT
//     Trim(x.kwd) || ',' ||
//     Trim(x.GMS_b) || ',' ||
//     Trim(x.GMS_a) || ',' ||
//     Trim(x.rate1 ) || ',' ||`;
//             if(type == 'GMSYoY'){
//                 bteq = `${bteq}
//     Trim(x.rate31 ) || ',' ||
//     Trim(x.rate61 ) || ',' ||
//     Trim(x.rate91 ) || ',' ||`;
//             }
//             bteq = `${bteq}
//     Trim(x.units_b) || ',' ||
//     Trim(x.units_a) || ',' ||
//     Trim(x.rate2 )
// FROM (

//     SELECT k.keyword AS kwd
//     , Coalesce(b.GMS,0) GMS_b
//     , Coalesce(a.GMS,0) GMS_a
//     , (a.GMS- Coalesce(b.GMS,0))*10000/CASE  Coalesce(b.GMS,0) WHEN 0 THEN 1 ELSE b.GMS end "rate1"`;
//             if(type == 'GMSYoY'){
//                 bteq = `${bteq}
//     , (Coalesce(a31.GMS,0)- Coalesce(b31.GMS,0))*10000/CASE  Coalesce(b31.GMS,0) WHEN 0 THEN 1 ELSE b31.GMS end "rate31"
//     , (Coalesce(a61.GMS,0)- Coalesce(b61.GMS,0))*10000/CASE  Coalesce(b61.GMS,0) WHEN 0 THEN 1 ELSE b61.GMS end "rate61"
//     , (Coalesce(a91.GMS,0)- Coalesce(b91.GMS,0))*10000/CASE  Coalesce(b91.GMS,0) WHEN 0 THEN 1 ELSE b91.GMS end "rate91"`;
//             }
//             bteq = `${bteq}
//     , Coalesce(b.units,0) units_b
//     , Coalesce(a.units,0) units_a
//   , (a.units-Coalesce(b.units,0))*10000/CASE Coalesce(b.units,0) WHEN 0 THEN 1 ELSE b.units end "rate2"
//  FROM k
 
//  LEFT JOIN
//     ( SELECT k.keyword keyword
//            , Sum(r.sub_total_amt) GMS
//            , Sum(r.units) units
//         FROM k
//        INNER JOIN UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
//           ON INDEX(Lower(r.item_name) , k.keyword) > 0
//        INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension i
//           ON r.genre_id = i.genre_id
//          AND i.${wgnr}
//        WHERE reg_datetime >= ${from1}
//          AND reg_datetime < ${until1}
//          AND price > 0
//          AND units > 0
//          AND cancel_datetime IS NULL
//        GROUP BY k.keyword
//     ) a   ON k.keyword = a.keyword

//  LEFT JOIN
//     ( SELECT k.keyword keyword
//            , Sum(r.sub_total_amt) GMS
//            , Sum(r.units) units
//         FROM k
//        INNER JOIN UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
//           ON INDEX(Lower(r.item_name) , k.keyword) > 0
//        INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension i
//           ON r.genre_id = i.genre_id
//          AND i.${wgnr}
//        WHERE reg_datetime >= ${from2}
//          AND reg_datetime < ${until2}
//          AND price > 0
//          AND units > 0
//          AND cancel_datetime IS NULL
//        GROUP BY k.keyword
//     ) b   ON k.keyword = b.keyword`;

//             if(type == 'GMSYoY'){
//                 bteq = `${bteq}
//  LEFT JOIN
//     ( SELECT k.keyword keyword
//            , Sum(r.sub_total_amt) GMS
//         FROM k
//        INNER JOIN UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
//           ON INDEX(Lower(r.item_name) , k.keyword) > 0
//        INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension i
//           ON r.genre_id = i.genre_id
//          AND i.${wgnr}
//        WHERE reg_datetime >= ${where_60d_ago}
//          AND reg_datetime < ${where_30d_ago}
//          AND price > 0
//          AND units > 0
//          AND cancel_datetime IS NULL
//        GROUP BY k.keyword
//     ) a31 ON k.keyword = a31.keyword

//  LEFT JOIN
//     ( SELECT k.keyword keyword
//            , Sum(r.sub_total_amt) GMS
//         FROM k
//        INNER JOIN UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
//           ON INDEX(Lower(r.item_name) , k.keyword) > 0
//        INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension i
//           ON r.genre_id = i.genre_id
//          AND i.${wgnr}
//        WHERE reg_datetime >= ${where_60d_ago_lyr}
//          AND reg_datetime < ${where_30d_ago_lyr}
//          AND price > 0
//          AND units > 0
//          AND cancel_datetime IS NULL
//        GROUP BY k.keyword
//     ) b31 ON k.keyword = b31.keyword

//  LEFT JOIN
//     ( SELECT k.keyword keyword
//            , Sum(r.sub_total_amt) GMS
//         FROM k
//        INNER JOIN UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
//           ON INDEX(Lower(r.item_name) , k.keyword) > 0
//        INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension i
//           ON r.genre_id = i.genre_id
//          AND i.${wgnr}
//        WHERE reg_datetime >= ${where_90d_ago}
//          AND reg_datetime < ${where_60d_ago}
//          AND price > 0
//          AND units > 0
//          AND cancel_datetime IS NULL
//        GROUP BY k.keyword
//     ) a61 ON k.keyword = a61.keyword

//  LEFT JOIN
//     ( SELECT k.keyword keyword
//            , Sum(r.sub_total_amt) GMS
//         FROM k
//        INNER JOIN UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
//           ON INDEX(Lower(r.item_name) , k.keyword) > 0
//        INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension i
//           ON r.genre_id = i.genre_id
//          AND i.${wgnr}
//        WHERE reg_datetime >= ${where_90d_ago_lyr}
//          AND reg_datetime < ${where_60d_ago_lyr}
//          AND price > 0
//          AND units > 0
//          AND cancel_datetime IS NULL
//        GROUP BY k.keyword
//     ) b61 ON k.keyword = b61.keyword

//  LEFT JOIN
//     ( SELECT k.keyword keyword
//            , Sum(r.sub_total_amt) GMS
//         FROM k
//        INNER JOIN UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
//           ON INDEX(Lower(r.item_name) , k.keyword) > 0
//        INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension i
//           ON r.genre_id = i.genre_id
//          AND i.${wgnr}
//        WHERE reg_datetime >= ${where_120d_ago}
//          AND reg_datetime < ${where_90d_ago}
//          AND price > 0
//          AND units > 0
//          AND cancel_datetime IS NULL
//        GROUP BY k.keyword
//     ) a91 ON k.keyword = a91.keyword

//  LEFT JOIN
//     ( SELECT k.keyword keyword
//            , Sum(r.sub_total_amt) GMS
//         FROM k
//        INNER JOIN UA_VIEW_MK_ICHIBA.red_basket_detail_tbl r
//           ON INDEX(Lower(r.item_name) , k.keyword) > 0
//        INNER JOIN UA_VIEW_MK_ICHIBA.item_genre_dimension i
//           ON r.genre_id = i.genre_id
//          AND i.${wgnr}
//        WHERE reg_datetime >= ${where_120d_ago_lyr}
//          AND reg_datetime < ${where_90d_ago_lyr}
//          AND price > 0
//          AND units > 0
//          AND cancel_datetime IS NULL
//        GROUP BY k.keyword
//     ) b91 ON k.keyword = b91.keyword`;
//             }
//             bteq = `${bteq}
// WHERE ${wlimit}
//     AND GMS_a-GMS_b > 0

// ) x
// ORDER BY x.GMS_a DESC, x.rate1 DESC, x.kwd;

// .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

// .EXIT
// `;
           

//console.log(bteq_path + btqfile);
//console.log(bteq);
            fs.writeFileSync(bteq_path + btqfile, bteq);
            resolve();
        });
    });
}


// run
co(function*() {
    yield output();
    
    console.log('fgs239-report-GMS end //');

});