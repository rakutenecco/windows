'use strict';

const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const shell = require('shelljs');
const co = require('co');

// parse args
const cli = commandArgs([
    { name: 'runtype', alias: 'r', type: String },
    { name: 'startdate', alias: 's', type: String }, // yyyymmdd default yesterday
    { name: 'filename', alias: 'f', type: String }, 
]);
let runtype = 1;
if(cli['startdate'] && cli['filename']) runtype = 9; //'err';
let startdate = cli['startdate'] ? cli['startdate'] : moment().add(-1, "days").format('YYYYMMDD');
let startdate_hyphen = moment(startdate).format('YYYY-MM-DD');
let today_hyphen = moment().format('YYYY-MM-DD');

//ex) FGS286_yahoo_all_20190307.txt
//ex) FGS286_yahoo_all_20190307_20190325.txt
//ex) FGS286_yahoo_new_20190322_20190325.txt
//ex) FGS286_yahoo_all_20190507_06_01.txt

let load_fnm;
let target_fnm = cli['filename'];
let icnt = 0;
//console.log('tmpfnm', tmpfnm, tmpfnm.split(/[_\.]/g));
if(target_fnm){
    let tmpfnm = target_fnm.split(/[_\.]/g);
    //console.log(tmpfnm, isFinite(tmpfnm[3]));
    if(tmpfnm[0] == 'FGS286' && tmpfnm[1] == 'yahoo' && tmpfnm[tmpfnm.length-1] == 'txt'){
        load_fnm = `FGS286_yahoo_${tmpfnm[2]}`;
        startdate = '';
    }else{
        runtype = 9; //'err';
    }
}else{
    load_fnm = 'FGS286_yahoo_all';
    target_fnm = `FGS286_yahoo_all_${startdate}_06_0${icnt}.txt`;
}

// const tdate1 = moment(postdate_hyphen).add(-1, "days").format('YYYY/MM/DD');
// const tdate7 = moment(postdate_hyphen).add(-7, "days").format('YYYY/MM/DD');
// const tdate14 = moment(postdate_hyphen).add(-14, "days").format('YYYY/MM/DD');
// const tdate21 = moment(postdate_hyphen).add(-21, "days").format('YYYY/MM/DD');
// const tdate28 = moment(postdate_hyphen).add(-28, "days").format('YYYY/MM/DD');
// const tdate35 = moment(postdate_hyphen).add(-35, "days").format('YYYY/MM/DD');

// const title1 = postdate_slash;
// const title2 = tdate1;

const prgrm = 'FGS286_yahoo';
const share_path = 'D:/share/'; 

const bteq_path = `D:/work/tool/tools/bteq/${prgrm}/`;
const box_path = `D:/Box Sync/EC Consulting Department/ecc_fashion/${prgrm}/`;
const fl_fd = 'fastload/';
//const rp_fd = 'report/';

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

const tbls = [
    { ctgry: 'food', datatbl: 'food_FGS286_yahoo', kwdtbl: 'food_FGS239_targets' },
    { ctgry: 'fashion', datatbl: 'fashion_FGS286_yahoo_fashion', kwdtbl: 'fashion_FGS239_targets_fashion' },
    { ctgry: 'watch', datatbl: 'fashion_FGS286_yahoo_watch', kwdtbl: 'fashion_FGS239_targets_watch' },
    { ctgry: 'flower', datatbl: 'flower_FGS286_yahoo', kwdtbl: 'flower_FGS239_targets' },
    { ctgry: 'interior', datatbl: 'interior_FGS286_yahoo', kwdtbl: 'interior_FGS239_targets' },
    { ctgry: 'kitchen', datatbl: 'kitchen_FGS286_yahoo', kwdtbl: 'kitchen_FGS239_targets' },
    { ctgry: 'life', datatbl: 'life_FGS286_yahoo', kwdtbl: 'life_FGS239_targets' },
    { ctgry: 'cosme', datatbl: 'cosme_FGS286_yahoo', kwdtbl: 'cosme_FGS239_targets' },
    { ctgry: 'wine', datatbl: 'wine_FGS286_yahoo', kwdtbl: 'wine_FGS239_targets' },
];

//let category;
//let tname;
//let minposts;
//let rprt_fnm;

console.log(`/// FGS286-yahoo.js start /// ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
console.log(`  runtype   : ${runtype}`);
console.log(`  startdate: ${startdate_hyphen}`);
console.log(`  load_fnm  : ${load_fnm}`);
console.log(`  target_fnm: ${target_fnm}`);
console.log(`  arg_fname : ${cli['filename']}`);

let fcnt;

// fastload
function fastload() {
    return new Promise((resolve) => {
        co(function*() {
           
            if(cli['filename']){
                let rt = yield load_filecheck();
                if(rt) {
                    let rt2 = yield load();
                    if (rt2) yield insert();
                }
            }else{
                // FGS286_yahoo_all_20190506_06_01.txt ~ FGS286_yahoo_all_20190506_06_06.txt
                // FGS286_yahoo_all_20190507_06_01.txt ~ FGS286_yahoo_all_20190507_06_06.txt
                do{
                    do{
                        icnt++;
                        target_fnm = `FGS286_yahoo_all_${startdate}_06_0${icnt}.txt`;
                        let rt = yield load_filecheck();
                        if(rt) {
                            let rt2 = yield load();
                            if (rt2) yield insert();
                        }
                    }while(icnt < 6)

                    startdate_hyphen = moment(startdate_hyphen).add(1, "days").format('YYYY-MM-DD');
                    startdate = startdate_hyphen.replace(/-/g,'');
                    icnt = 0;

                }while(startdate_hyphen < today_hyphen)
            }
            resolve();
        });
    });
};


// load_check
function load_filecheck() {

    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;
            let fname0 = `${share_path}${target_fnm}`;
            let fname1 = `${box_path}${fl_fd}${target_fnm}`;
            let fname2 = `${bteq_path}${fl_fd}${target_fnm}`;

            // check files
            if (fs.existsSync(fname0)) {
                if (fs.existsSync(fname1)) {
                    console.log(`fastload : ${fname1} done`);
                }else{
                    if (fs.existsSync(fname2)) {
                        console.log(`fastload : ${fname2} done`);
                    } else {
                        console.log(`share : ${fname0} exists`);
                        shell.cp(fname0, `${bteq_path}${fl_fd}`);
                        result = true;
                    }
                }
            } else {
                console.log(`share : ${fname0} not exists`);
            }

            if ( result ) {
                let lines = fs.readFileSync(`${bteq_path}${fl_fd}${target_fnm}`).toString().split("\n");
                if(lines[lines.length -1].length == 0) lines.pop();
                console.log(`  lines.length : ${lines.length}`);
                fcnt = lines.length;
            }

            resolve(result);
        });
    });
};

// load
function load() {

    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;
            
            let flstr = `
SET SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
    DATABASE SBX_ICB_ICBB;

    DROP TABLE fashion_${load_fnm}_pre;
    DROP TABLE fashion_${load_fnm}_pre_et;
    DROP TABLE fashion_${load_fnm}_pre_uv;

    CREATE TABLE fashion_${load_fnm}_pre
    (
        keyword varchar(200),
        category varchar(100),
        getdate VARCHAR(10),
        postdate VARCHAR(10),
        tweets bigint,
        badfeel integer,
        goodfeel integer
    )PRIMARY INDEX ( keyword );

    BEGIN LOADING fashion_${load_fnm}_pre ERRORFILES fashion_${load_fnm}_pre_et, fashion_${load_fnm}_pre_uv;
    set record vartext "	";
    DEFINE 
        keyword (varchar(200)),
        category (varchar(100)),
        getdate (VARCHAR(10)),
        postdate (VARCHAR(10)),
        tweets (VARCHAR(20)),
        badfeel (VARCHAR(5)),
        goodfeel (VARCHAR(5))
    FILE = ${bteq_path}${fl_fd}${target_fnm};

    INSERT INTO fashion_${load_fnm}_pre VALUES(
        :keyword,
        :category,
        :getdate,
        :postdate,
        :tweets,
        :badfeel,
        :goodfeel
    );
    END LOADING;

.EXIT
`;

            fs.writeFileSync(`${bteq_path}${fl_fd}load_${target_fnm}`, flstr);
            // Fastload
            console.log(`  fastload 実行中   :  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
            shell.exec(`fastload < ${bteq_path}${fl_fd}load_${target_fnm} > ${bteq_path}${fl_fd}load_${target_fnm}.log`);

            let rt = yield load_count();
            if( rt ) {
                result = true;
            } else {
                console.log('  fastload failed');
            }

            resolve(result);
        });
    });
};


// load_count
function load_count() {

    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;

            let btqfile = `${bteq_path}${fl_fd}loadcount_${target_fnm}`;
            let btqlog = `${bteq_path}${fl_fd}loadcount_${target_fnm}.log`;
            let cntfile = `${bteq_path}${fl_fd}loadcount_${target_fnm}_result.txt`;
            shell.rm(cntfile);

            // select count
            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${cntfile}
.RECORDMODE OFF
DATABASE SBX_ICB_ICBB;
SELECT
    Trim(x.cnt)
FROM 
  ( SELECT count(*) cnt 
      FROM fashion_${load_fnm}_pre
  ) x;
  
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(btqfile, bteq);
            console.log(`  load count 実行中   :  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);

            let data = fs.readFileSync(cntfile, 'utf8');
            let cnt = data.match(/\d{1,}/);

            if( cnt == fcnt){
                console.log(`   fastload : ${cnt}件`);
                result = true;
            }
            
            resolve(result);
        });
    });
};


// insert into each table
function insert() {

    return new Promise((resolve) => {
        co(function*() {
            
            let result = false;
            // insert
            for (let i = 0; i < tbls.length; i++) {
                let iststr = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;
    DATABASE SBX_ICB_ICBB;

    INSERT INTO ${tbls[i].datatbl}

    SELECT keyword, max(category), max(getdate), postdate, max(tweets), max(badfeel), max(goodfeel)
    FROM fashion_${load_fnm}_pre a
    WHERE NOT EXISTS 
        (SELECT * FROM ${tbls[i].datatbl} b 
          WHERE a.keyword = b.keyword AND a.postdate = b.postdate )
    AND a.keyword in (select keyword from ${tbls[i].kwdtbl})
    GROUP BY keyword, postdate;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;    
                fs.writeFileSync(`${bteq_path}${fl_fd}insert_${target_fnm}_${tbls[i].ctgry}.txt`, iststr);
                console.log(`  insert 実行中 : ${tbls[i].ctgry}  :  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                shell.exec(`bteq < ${bteq_path}${fl_fd}insert_${target_fnm}_${tbls[i].ctgry}.txt > ${bteq_path}${fl_fd}insert_${target_fnm}_${tbls[i].ctgry}.log`);
            }

            shell.cp(`${bteq_path}${fl_fd}${target_fnm}`, `${box_path}${fl_fd}`);            
            result = true;
            resolve(result);
        });
    });
};


// run
co(function*() {
    
    let st = moment().format('YYYY/MM/DD HH:mm:ss');

    if (runtype == 1) {
        yield fastload();
        //if (rt) yield report();
    
    }
    
    console.log(`/// FGS286-yahoo.js end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

});
