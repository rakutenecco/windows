'use strict';

const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const shell = require('shelljs');
const co = require('co');

// parse args
const cli = commandArgs([
    { name: 'type', alias: 'p', type: Number }, // 1:food 2:fashion 3:watch 4:flower 5:interior 6:kitchen 7:life
    { name: 'targetdate', alias: 't', type: String }, // yyyymmdd default yesterday
    { name: 'term', alias: 'r', type: String }, // all
]);

const targetdate = cli['targetdate'] ? cli['targetdate'] : moment().add(-1, "days").format('YYYYMMDD');
const postdate_hyphen = targetdate.substr(0, 4) + '-' + targetdate.substr(4, 2) + '-' + targetdate.substr(6, 2);
const postdate_slash = postdate_hyphen.replace(/-/g,'/');

const tdate1 = moment(postdate_hyphen).add(-1, "days").format('YYYY/MM/DD');
const tdate7 = moment(postdate_hyphen).add(-7, "days").format('YYYY/MM/DD');
const tdate14 = moment(postdate_hyphen).add(-14, "days").format('YYYY/MM/DD');
const tdate21 = moment(postdate_hyphen).add(-21, "days").format('YYYY/MM/DD');
const tdate28 = moment(postdate_hyphen).add(-28, "days").format('YYYY/MM/DD');
const tdate35 = moment(postdate_hyphen).add(-35, "days").format('YYYY/MM/DD');

const title1 = postdate_slash;
const title2 = tdate1;

const term = cli['term'] ? cli['term'] : 'all'; //all, daily, weekly, monthly

const prgrm = 'FGS258_twitter';
const share_path = 'D:/share/'; 

const bteq_path = `D:/work/tool/tools/bteq/${prgrm}/`;
const box_path = `D:/Box Sync/EC Consulting Department/ecc_fashion/${prgrm}/`;
const fl_fd = 'fastload/';
const rp_fd = 'report/';

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

const tname_twitter_food = 'SBX_ICB_ICBB.food_FGS258_twitter_posts';
const tname_twitter_fashion = 'SBX_ICB_ICBB.fashion_FGS258_twitter_posts_fashion';
const tname_twitter_watch = 'SBX_ICB_ICBB.fashion_FGS258_twitter_posts_watch';
const tname_twitter_flower = 'SBX_ICB_ICBB.flower_FGS258_twitter_posts';
const tname_twitter_interior = 'SBX_ICB_ICBB.interior_FGS258_twitter_posts';
const tname_twitter_kitchen = 'SBX_ICB_ICBB.kitchen_FGS258_twitter_posts';
const tname_twitter_life = 'SBX_ICB_ICBB.life_FGS258_twitter_posts';

let category;
let tname;
let minposts;
let load_fnm;
let rprt_fnm;


// fastload
function fastload() {
    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;
            let rt = yield load_check();
            let ct = 0;
            if(!rt) {
                let rt2 = yield load();
                if (rt2) ct = yield load_count();
            }
            if(rt || ct > 0) result = true;
            resolve(result);
        });
    });
};


// report
function report() {
    return new Promise((resolve) => {
        co(function*() {

            // out_csv check
            let rt = yield report_check();
            if(!rt) {
                yield report_out();
            }
            resolve();
        });
    });
};


// load_check
function load_check() {

    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;
            load_fnm = `FGS258_twitter_posts_${category}`;
            let fname1 = `${box_path}${fl_fd}${load_fnm}_${targetdate}.txt`;
            let fname2 = `${bteq_path}${fl_fd}${load_fnm}_${targetdate}.txt`;
            //console.log(`fname1 : ${fname1}`);
            // check files
            if (fs.existsSync(fname1)) {
                result = true;
                console.log(`fastload : ${fname1} done`);
            }else{
                if (fs.existsSync(fname2)) {
                    result = true;
                    console.log(`fastload : ${fname2} done`);
                }
            }

            resolve(result);
        });
    });
};

// load
function load() {

    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;
            // check share files
            if (fs.existsSync(`${share_path}${load_fnm}_${targetdate}.txt`)) {
                console.log(`fastload : ${load_fnm}_${targetdate}.txt   ${moment().format('YYYY/MM/DD HH:mm:ss')}  start`);
                // share -> bteq
                shell.cp(`${share_path}${load_fnm}_${targetdate}.txt`, `${bteq_path}${fl_fd}${load_fnm}_${targetdate}.txt`);
                
                let flstr = `
SET SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
    DATABASE SBX_ICB_ICBB;

    DROP TABLE ${tname}_pre;
    DROP TABLE ${tname}_pre_et;
    DROP TABLE ${tname}_pre_uv;

    CREATE TABLE ${tname}_pre
    (
        keyword varchar(500),
        category varchar(500),
        getdate VARCHAR(10),
        postdate VARCHAR(10),
        posts bigint
    )PRIMARY INDEX ( keyword );

    BEGIN LOADING ${tname}_pre ERRORFILES ${tname}_pre_et, ${tname}_pre_uv;
    set record vartext "	";
    DEFINE 
        keyword (varchar(500)),
        category (varchar(500)),
        getdate (VARCHAR(10)),
        postdate (VARCHAR(10)),
        posts (VARCHAR(20))
    FILE = ${bteq_path}${fl_fd}${load_fnm}_${targetdate}.txt;

    INSERT INTO ${tname}_pre VALUES(
        :keyword,
        :category,
        :getdate,
        :postdate,
        :posts
    );
    END LOADING;

.EXIT
`;
                
                //console.log(`fastload filename:${bteq_path}${fl_fd}${load_fnm}_load.txt`);
                fs.writeFileSync(`${bteq_path}${fl_fd}${load_fnm}_load.txt`, flstr);
                // Fastload
                console.log('  fastload 実行中');
                shell.exec(`fastload < ${bteq_path}${fl_fd}${load_fnm}_load.txt > ${bteq_path}${fl_fd}${load_fnm}_load_log.txt`);
                
                // insert
                let iststr = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;

    INSERT INTO ${tname}

    SELECT *
    FROM ${tname}_pre a
    WHERE NOT EXISTS 
       (SELECT * FROM ${tname} b 
         WHERE a.keyword = b.keyword AND a.postdate = b.postdate );

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;

                fs.writeFileSync(`${bteq_path}${fl_fd}${load_fnm}_insert.txt`, iststr);
                console.log('  insert 実行中');
                shell.exec(`bteq < ${bteq_path}${fl_fd}${load_fnm}_insert.txt > ${bteq_path}${fl_fd}${load_fnm}_insert_log.txt`);
                result = true;
            }else{
                console.log(`${share_path}${load_fnm}_${targetdate}.txt not exists`);
            }

            resolve(result);
        });
    });
};

// load_count
function load_count() {

    return new Promise((resolve) => {
        co(function*() {
           
            let cnt;
            let btqfile = `${bteq_path}${fl_fd}${load_fnm}_count.txt`;
            let btqlog = `${bteq_path}${fl_fd}${load_fnm}_count_log.txt`;
            let cntfile = `${bteq_path}${fl_fd}${load_fnm}_count_result.txt`;
            shell.rm(cntfile);

            // select count
            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${cntfile}
.RECORDMODE OFF
SELECT
    Trim(x.cnt)
FROM 
  ( select count(*) cnt 
      from ${tname}
     where postdate = '${postdate_slash}'
  ) x;
  
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(btqfile, bteq);
            console.log('  count 実行中');
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);

            let data = fs.readFileSync(cntfile, 'utf8');
            cnt = data.match(/\d{1,}/);

            if(cnt > 0){                
                // share -> box,bteq 
                shell.cp(`${share_path}${load_fnm}_${targetdate}.txt`, `${box_path}${fl_fd}${load_fnm}_${targetdate}.txt`);
                shell.cp(`${share_path}${load_fnm}_${targetdate}.txt`, `${bteq_path}${fl_fd}${load_fnm}_${targetdate}.txt`);
            }

            console.log(`fastload : ${load_fnm}_${targetdate}.txt    ${moment().format('YYYY/MM/DD HH:mm:ss')}  end // `);
            resolve(cnt);
        });
    });
};

// report_check
function report_check() {

    return new Promise((resolve) => {
        co(function*() {
                         
            let result = false;
            rprt_fnm = `report_twitter_${category}`;
            let f1 = `${box_path}${rp_fd}${rprt_fnm}_${targetdate}.csv`;
            let f2 = `${bteq_path}${rp_fd}${rprt_fnm}_${targetdate}.csv`;
            //console.log(f1);
            //console.log(f2);
            // check files
            if (fs.existsSync(f1)) {
                result = true;
                console.log(`report : ${f1} done`);
            }else{
                if (fs.existsSync(f2)) {
                    result = true;
                    console.log(`report : ${f2} done`);
                }
            }

            resolve(result);
        });
    });
};

// report 急上昇keywordのレポートを生成
function report_out() {

    return new Promise((resolve) => {
        co(function*() {

            console.log(`report output : ${rprt_fnm}_${targetdate}.csv    ${moment().format('YYYY/MM/DD HH:mm:ss')}  start`);
            let reportfile = `${bteq_path}${rp_fd}${rprt_fnm}_${targetdate}.csv`;
            let btqfile = `${bteq_path}${rp_fd}${rprt_fnm}.txt`;
            let btqlog = `${bteq_path}${rp_fd}${rprt_fnm}_log.txt`;

            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${reportfile}
.RECORDMODE OFF`;
            if(category == 'food'){
                bteq = `${bteq}
SELECT 'keyword, 属性１, 属性２, ${title2}, ${title1}, 伸び率%,投稿数　　　　　(過去1週間),伸び率%　　　　　(過去1週間),投稿数　　　　　(2週間前),伸び率%　　　　　(2週間前),投稿数　　　　　(3週間前),伸び率%　　　　　(3週間前),投稿数　　　　　(4週間前),伸び率%　　　　　(4週間前)';
SELECT
    '"' || Trim(x.kwd) || '",' ||
    Trim(x.attr1) || ',' ||
    Trim(x.attr2) || ',' ||`;

            }else {
                bteq = `${bteq}
SELECT 'keyword, ${title2}, ${title1}, 伸び率%,投稿数　　　　　(過去1週間),伸び率%　　　　　(過去1週間),投稿数　　　　　(2週間前),伸び率%　　　　　(2週間前),投稿数　　　　　(3週間前),伸び率%　　　　　(3週間前),投稿数　　　　　(4週間前),伸び率%　　　　　(4週間前)';
SELECT
    '"' || Trim(x.kwd) || '",' ||`;
                
            }
            bteq = `${bteq}

    Trim(x.pst2) || ',' ||
    Trim(x.pst1) || ',' ||
    Trim(x.rate1) || ',' ||
    Trim(x.w1) || ',' ||
    Trim(x.w1_rate) || ',' ||
    Trim(x.w2) || ',' ||
    Trim(x.w2_rate) || ',' ||
    Trim(x.w3) || ',' ||
    Trim(x.w3_rate) || ',' ||
    Trim(x.w4) || ',' ||
    Trim(x.w4_rate)
FROM (

    SELECT
    a1.keyword "kwd"
  , Coalesce( k.attribute1, '' ) "attr1"
  , Coalesce( k.attribute2, '' ) "attr2"
  , a2.posts "pst2"
  , a1.posts "pst1"
  , (a1.posts-a2.posts)*10000/CASE a2.posts WHEN 0 THEN 1 ELSE a2.posts end "rate1"
  ,w1.posts "w1"
  , Coalesce( (w1.posts - w2.posts) * 10000 / CASE w2.posts WHEN 0 THEN 1 ELSE w2.posts end , '-') "w1_rate"
  , Coalesce( w2.posts , '-') "w2"
  , Coalesce( (w2.posts - w3.posts) * 10000 / CASE w3.posts WHEN 0 THEN 1 ELSE w3.posts end , '-') "w2_rate"
  , Coalesce( w3.posts , '-') "w3"
  , Coalesce( (w3.posts - w4.posts) * 10000 / CASE w4.posts WHEN 0 THEN 1 ELSE w4.posts end , '-') "w3_rate"
  , Coalesce( w4.posts , '-') "w4"
  , Coalesce( (w4.posts - w5.posts) * 10000 / CASE w5.posts WHEN 0 THEN 1 ELSE w5.posts end , '-') "w4_rate"
    
  FROM
    ( SELECT keyword, posts
        FROM ${tname}
       WHERE postdate = '${title1}' --'2019/02/11'
         AND posts >= ${minposts}
    ) a1
 INNER JOIN
    ( SELECT keyword, posts
        FROM ${tname}
       WHERE postdate = '${title2}' --'2019/02/10'
         --AND posts > 0
    ) a2
    ON a1.keyword = a2.keyword AND a1.posts-a2.posts > 0

 INNER JOIN
    ( SELECT keyword, Sum(posts) posts
        FROM ${tname}
      WHERE postdate <= '${title1}' --'2019/02/11'
	      AND postdate > '${tdate7}' --'2019/02/04'
	 GROUP BY keyword
    ) w1
      ON w1.keyword = a1.keyword
  LEFT JOIN
    ( SELECT keyword, Sum(posts) posts
        FROM ${tname}
      WHERE postdate <= '${tdate7}' --'2019/02/04'
	      AND postdate > '${tdate14}' --'2019/01/28'
	 GROUP BY keyword
    ) w2
      ON w2.keyword = a1.keyword
  LEFT JOIN
    ( SELECT keyword, Sum(posts) posts
        FROM ${tname}
      WHERE postdate <= '${tdate14}' --'2019/01/28'
	      AND postdate > '${tdate21}' --'2019/01/21'
	 GROUP BY keyword
    ) w3
      ON w3.keyword = a1.keyword
  LEFT JOIN
    ( SELECT keyword, Sum(posts) posts
        FROM ${tname}
      WHERE postdate <= '${tdate21}' --'2019/01/21'
	      AND postdate > '${tdate28}' --'2019/01/14'
	 GROUP BY keyword
    ) w4
      ON w4.keyword = a1.keyword
  LEFT JOIN
    ( SELECT keyword, Sum(posts) posts
        FROM ${tname}
      WHERE postdate <= '${tdate28}' --'2019/01/14'
	      AND postdate > '${tdate35}' --'2019/01/07'
	 GROUP BY keyword
    ) w5
      ON w5.keyword = a1.keyword
      
  LEFT JOIN
    ( SELECT Lower(Translate( keyword USING unicode_to_unicode_nfkc)) keyword
           , attribute1, attribute2
        FROM SBX_ICB_ICBB.food_FGS239_targets_test2
    ) k
    ON k.keyword = a1.keyword
  
  ) x
  ORDER BY "pst1" DESC, "rate1" DESC, kwd;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;

//console.log(btqfile);
//console.log(bteq);
            fs.writeFileSync(btqfile, bteq);

            //  bteq 
            console.log('  report 出力中');
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);
        
            // bteq -> box
            shell.cp(`${reportfile}`, `${box_path}${rp_fd}${rprt_fnm}_${targetdate}.csv`);                

            console.log(`report output : ${rprt_fnm}_${targetdate}.csv    ${moment().format('YYYY/MM/DD HH:mm:ss')}  end //`);            

            resolve();
        });
    });
};


// run
co(function*() {
    
    if (!cli['type'] || cli['type'] == 1) {
        // twitter posts (food)
        category = 'food'
        tname = tname_twitter_food;
        minposts = 10;
        let rt = yield fastload();
        if (rt) yield report();
    }
    if (!cli['type'] || cli['type'] == 2) {
        // twitter posts (fashion)
        category = 'fashion'
        tname = tname_twitter_fashion;
        minposts = 10;
        let rt = yield fastload();
        if (rt) yield report();
    }
    if (!cli['type'] || cli['type'] == 3) {
        // FGS258_twitter posts (watch)
        category = 'watch'
        tname = tname_twitter_watch;
        minposts = 10;
        let rt = yield fastload();
        if (rt) yield report();
    }
    if (!cli['type'] || cli['type'] == 4) {
        // twitter posts (flower)
        category = 'flower'
        tname = tname_twitter_flower;
        minposts = 10;
        let rt = yield fastload();
        if (rt) yield report();
    }
    if (!cli['type'] || cli['type'] == 5) {
        // twitter posts (interior)
        category = 'interior'
        tname = tname_twitter_interior;
        minposts = 10;
        let rt = yield fastload();
        if (rt) yield report();
    }
    if (!cli['type'] || cli['type'] == 6) {
        // twitter posts (kitchen)
        category = 'kitchen'
        tname = tname_twitter_kitchen;
        minposts = 10;
        let rt = yield fastload();
        if (rt) yield report();
    }
    if (!cli['type'] || cli['type'] == 7) {
        // twitter posts (life)
        category = 'life'
        tname = tname_twitter_life;
        minposts = 10;
        let rt = yield fastload();
        if (rt) yield report();
    }
    
    console.log(`FGS258-twitter-posts  ${moment().format('YYYY/MM/DD HH:mm:ss')}  end //`);

});
