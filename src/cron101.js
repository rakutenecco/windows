'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const fs = require('fs');
const moment = require('moment');
const date = moment().format('YYYYMMDD');
const path_log = `./log/${date}.txt`;
let now = '';


// // (1) FGS239GMS, fastload daily 8:05
// new cronJob(`00 05 08 * * 0-6`, function(){
//     console.log(`cron101(1) ${moment().format('YYYY-MM-DD HH:mm:ss')} start //`);
    
//     //console.log(`fgs239-targets-load start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
//     //shell.exec('node src/fgs239-targets-load.js');

//     console.log(`fgs239-report-GMS start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
//     shell.exec('node src/fgs239-report-GMS.js');
    
//     console.log(`fgs251-vogue start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
//     shell.exec('node src/fgs251-vogue.js');
//     console.log(`fgs252-FashionNetwork start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
//     shell.exec('node src/fgs252-FashionNetwork.js');
//     console.log(`fgs254-senken start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
//     shell.exec('node src/fgs254-senken.js');
//     console.log(`fgs247-insta-official start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
//     shell.exec('node src/fgs247-insta-official.js');

//     console.log(`cron101(1) ${moment().format('YYYY-MM-DD HH:mm:ss')} end //`);

// }, function(){
//     // error
//     console.log(`Error ${moment().format('YYYY-MM-DD HH:mm:ss')} : FGS239 FGS258 fastload, report cron101`);
// },
// true, 'Asia/Tokyo');

// // (2) FGS239 FGS258 FGS283 fastload,report daily 08-23h 15,45m
// new cronJob(`00 10,40 08-23 * * 0-6`, function(){
//     console.log(`cron101(2) ${moment().format('YYYY-MM-DD HH:mm:ss')} start //`);
    
//     //console.log(`fgs239-insta-posts start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
//     //shell.exec('node src/fgs239-insta-posts.js');
//     console.log(`fgs258-twitter-posts start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
//     shell.exec('node src/fgs258-twitter-posts.js');
//     //console.log(`fgs283-recipe-posts-total start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
//     //shell.exec('node src/fgs283-recipe-posts-total.js');


//     //    console.log(`fgs239-get-hashtag start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
// //    shell.exec('node src/fgs239-get-hashtag.js');

//     console.log(`cron101(2) ${moment().format('YYYY-MM-DD HH:mm:ss')} end //`);
// }, function(){
//     // error
//     console.log(`Error ${moment().format('YYYY-MM-DD HH:mm:ss')} : FGS239 FGS258 FGS283 fastload, report cron101(2)`);
// }, true, 'Asia/Tokyo');

// (3) FGS239 FGS283 fastload,report weekly 08-23h 15,45m
new cronJob(`00 10,40 08-23 * * 0`, function(){
//new cronJob(`00 10,40 13-23 * * 1`, function(){
    console.log(`cron101(3) ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} start //`);
    
    console.log(`fgs239-insta-posts start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
    shell.exec('node src/fgs239-insta-posts.js');
    //console.log(`fgs283-recipe-posts-total start ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
    //shell.exec('node src/fgs283-recipe-posts-total.js');

    console.log(`cron101(3) ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} end //`);
}, function(){
    // error
    console.log(`Error ${moment().format('YYYY-MM-DD HH:mm:ss')} : FGS239 FGS283 fastload, report cron101(3)`);
}, true, 'Asia/Tokyo');


// FGS286 yahoo fastload tuu-sun 7:55
new cronJob(`00 55 07 * * 4-7`, function(){
    console.log(`yahoo fastload ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} start //`);

    let sdate = moment().add(-2, 'days').format('YYYYMMDD');
    //shell.exec(`node src/fgs286-yahoo.js -s ${sdate}`);

    console.log(`yahoo fastload ${moment().format('YYYY-MM-DD HH:mm:ss dddd')} end //`);
}, function(){
    // error
    console.log(`Error ${moment().format('YYYY-MM-DD HH:mm:ss')} : FGS286 yahoo fastload`);
}, true, 'Asia/Tokyo');


console.log('cron101 setting');







