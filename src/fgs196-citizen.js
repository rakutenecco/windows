'use strict';

const shell = require('shelljs');
const fs = require('fs-extra');
const commandArgs = require('command-line-args');
const moment = require('moment');
const iconv = require('iconv-lite');
const util = new (require('./util.js'));
const co = require('co');
const csql = require('./fgs196-citizen-sql.js');
let today = moment().format('YYYYMMDD');

const tname = 'FGS196_citizen';
const tools_path = 'D:/work/tool/tools/'; 
const share_path = 'D:/share/'; 


// parse args
const cli = commandArgs([
    { name: 'time', alias: 't', type: String }
 ]);
  
 // reset time
 if (cli['time']) {
     today = cli['time'];
 }
const bteq_path = `D:\/work\/tool\/tools\/bteq\/citizen_${today}\/`;
const box_path = `D:\/Box Sync\/EC Consulting Department\/ecc_fashion\/${tname}\/${today}`;
const out_file1 = `citizen_1_${today}.csv`;
const out_file2 = `citizen_2_${today}.csv`;
const out_file3 = `citizen_list_${today}.sql`;


// init
function init() {

    return new Promise((resolve) => {
        co(function*() {

            // rm ...fastloadで出力されるファイル
            shell.rm(`${tools_path}output/*${tname}*`);
            
            // share -> input_fastload            
            shell.cp(`${share_path}${tname}_${today}.txt`, `${tools_path}input_fastload/${tname}.txt`);

            resolve();
        });
    });
};

// do Fastload
function doFastload() {

    return new Promise((resolve) => {
        co(function*() {

            // Fastload
            shell.exec(`node src/makeFastload.js -p ${tname}_pre -d`);

            resolve();
        });
    });
};

// 本体テーブルにinsert
// modelsテーブルにmerge
function doInsert() {

    return new Promise((resolve) => {
        co(function*() {

            let datapath = `${tools_path}output/${tname}_fastload_data.txt`;
            if (fs.existsSync(datapath)) {
                // insert
                let btqfile = `${tools_path}bteq/${tname}_ins.txt`;
                //yield 
                csql.setBteq_ctzn_ins(btqfile);
                shell.exec(`bteq < bteq/${tname}_ins.txt > bteq/${tname}_ins_log.txt`);
            
            }else{
                let deletepath = `${tools_path}input_fastload/${tname}.txt`;
                fs.unlink(deletepath);
            }
            resolve();
        });
    });
};

// output1（売上情報）を生成
// output2（登録商品情報）を生成
function doOutput() {

    return new Promise((resolve) => {
        co(function*() {

            util.mkDir(bteq_path, function() {
                util.mkDir(box_path, function() {
                    // detail insert output1
                    let btq1 = `${tools_path}bteq/${tname}_dtl.txt`;
                    let out1 = `${bteq_path}${out_file1}`;
                    //  bteq 
                    csql.setBteq_ctzn_dtl(btq1, out1);
                    shell.exec(`bteq < bteq/${tname}_dtl.txt > bteq/${tname}_dtl_log.txt`);
                    //  sjis
                    let data1 = fs.readFileSync(out1, 'utf8');
                    let buf1 = iconv.encode(data1, "Shift_JIS" );
                    fs.writeFileSync(`${out1}`,buf1);

                    // item insert output2
                    let btq2 = `${tools_path}bteq/${tname}_itm.txt`;
                    let out2 = `${bteq_path}${out_file2}`;
                    //  bteq 
                    csql.setBteq_ctzn_itm(btq2, out2);
                    shell.exec(`bteq < bteq/${tname}_itm.txt > bteq/${tname}_itm_log.txt`);
                    //  sjis
                    let data2 = fs.readFileSync(out2, 'utf8');
                    let buf2 = iconv.encode(data2, "Shift_JIS" );
                    fs.writeFileSync(`${out2}`,buf2);

                    // sqlfile output3
                    let out3 = `${bteq_path}${out_file3}`;
                    //  out 
                    csql.setBteq_ctzn_sql(out3);

                    // bteq -> box
                    shell.cp(`${bteq_path}/*`, box_path);

                });
            });

            resolve();
        });
    });
};


// run
co(function*() {
    yield init();
    yield doFastload();
    yield doInsert();
    yield doOutput();

});

