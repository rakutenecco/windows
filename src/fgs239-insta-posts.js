'use strict';

const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const shell = require('shelljs');
const co = require('co');

// parse args
const cli = commandArgs([
    { name: 'type', alias: 'p', type: String }, // 0:all 1:food 2:fashion 3:WEAR 4:watch 5:life 6:flower 7:interior 8:kitchen
    { name: 'targetdate', alias: 't', type: String }, // yyyymmdd default before sunday
    { name: 'term', alias: 'r', type: String }, // all
]);

const targetdate = cli['targetdate'] ? cli['targetdate'] : moment().day(0).format('YYYYMMDD');
const getdate_hyphen = targetdate.substr(0, 4) + '-' + targetdate.substr(4, 2) + '-' + targetdate.substr(6, 2);
const getdate_slash = getdate_hyphen.replace(/-/g,'/');

const tdate = moment(getdate_hyphen).day(0).format('YYYY-MM-DD');//before sunday
const tdate_slash = tdate.replace(/-/g,'/')
const tdate7 = moment(tdate).add(-7, "days").format('YYYY/MM/DD');
const tdate14 = moment(tdate).add(-14, "days").format('YYYY/MM/DD');
const tdate21 = moment(tdate).add(-21, "days").format('YYYY/MM/DD');
const tdate28 = moment(tdate).add(-28, "days").format('YYYY/MM/DD');
const tdate35 = moment(tdate).add(-35, "days").format('YYYY/MM/DD');

console.log('targetdate:',targetdate,'  tdate:', tdate);

const term = cli['term'] ? cli['term'] : 'all'; //all, daily, weekly, monthly

const prgrm = 'FGS239_insta';
const share_path = 'D:/share/'; 

const bteq_path = `D:/work/tool/tools/bteq/${prgrm}/`;
const box_path = `D:/Box Sync/EC Consulting Department/ecc_fashion/${prgrm}/`;
const fl_fd = 'fastload/';
const rp_fd = 'report/';
const mx_fd = 'max/';

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

const tname_insta_food = 'food_FGS239_insta_posts';
const tname_insta_fashion = 'fashion_FGS239_insta_posts_fashion';
const tname_wear_fashion = 'fashion_FGS239_wear_posts_fashion';
const tname_insta_watch = 'fashion_FGS239_insta_posts_watch';
const tname_insta_life = 'life_FGS239_insta_posts';
const tname_insta_flower = 'flower_FGS239_insta_posts';
const tname_insta_interior = 'interior_FGS239_insta_posts';
const tname_insta_kitchen = 'kitchen_FGS239_insta_posts';
const tname_insta_cosme = 'cosme_FGS239_insta_posts';
const tname_insta_wine = 'wine_FGS239_insta_posts';

let category;
let tname;
let minposts;
let load_fnm;
let rprt_fnm;


const tbls = [
    { ctgry: 'food', datatbl: 'food_FGS239_insta_posts', kwdtbl: 'food_FGS239_targets' },
    { ctgry: 'fashion', datatbl: 'fashion_FGS239_insta_posts_fashion', kwdtbl: 'fashion_FGS239_targets_fashion' },
    { ctgry: 'watch', datatbl: 'fashion_FGS239_insta_posts_watch', kwdtbl: 'fashion_FGS239_targets_watch' },
    { ctgry: 'flower', datatbl: 'flower_FGS239_insta_posts', kwdtbl: 'flower_FGS239_targets' },
    { ctgry: 'interior', datatbl: 'interior_FGS239_insta_posts', kwdtbl: 'interior_FGS239_targets' },
    { ctgry: 'kitchen', datatbl: 'kitchen_FGS239_insta_posts', kwdtbl: 'kitchen_FGS239_targets' },
    { ctgry: 'life', datatbl: 'life_FGS239_insta_posts', kwdtbl: 'life_FGS239_targets' },
    { ctgry: 'cosme', datatbl: 'cosme_FGS239_insta_posts', kwdtbl: 'cosme_FGS239_targets' },
    { ctgry: 'wine', datatbl: 'wine_FGS239_insta_posts', kwdtbl: 'wine_FGS239_targets' },
];

console.log(`/// FGS239-insta-posts.js start /// ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
console.log(`  runtype   : ${cli['type']}`);
console.log(`  targetdate: ${getdate_hyphen}`);

let fcnt;
const today = moment().format('YYYYMMDD');
const mdate = moment().add(-34, "days").format('YYYY/MM/DD');

// fastload
function fastload() {
    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;
            let rt = yield load_check();
            let ct = 0;
            if(!rt) {
                let rt2 = yield load();
                if (rt2) ct = yield load_count();
            }
            if(rt || ct > 0) result = true;
            resolve(result);
        });
    });
};


// report
function report() {
    return new Promise((resolve) => {
        co(function*() {

            // out_csv check
            let rt = yield report_check();
            if(!rt) {
                yield report_out();
                if(category == 'WEAR' || category == 'all') {
                    yield max_output();
                }
            }
            resolve();
        });
    });
};


// load_check
function load_check() {

    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;

            if ( category == 'WEAR' ) {
                load_fnm = `FGS239_wear_posts_fashion`;
            } else {
                load_fnm = `FGS239_insta_posts_${category}`;
            }
            
            let fname1 = `${box_path}${fl_fd}${load_fnm}_${targetdate}.txt`;
            let fname2 = `${bteq_path}${fl_fd}${load_fnm}_${targetdate}.txt`;

            // check files
            if (fs.existsSync(fname1)) {
                result = true;
                console.log(fname1 + ' done');
            }else{
                if (fs.existsSync(fname2)) {
                    result = true;
                    console.log(fname2 + ' done');
                }
            }

            resolve(result);
        });
    });
};

// load
function load() {

    return new Promise((resolve) => {
        co(function*() {
            let result = false;
            // check share files
            if (fs.existsSync(`${share_path}${load_fnm}_${targetdate}.txt`)) {
                console.log(`fastload : ${load_fnm}_${targetdate}.txt   ${moment().format('YYYY/MM/DD HH:mm:ss')}  start`);
                // share -> bteq
                shell.cp(`${share_path}${load_fnm}_${targetdate}.txt`, `${bteq_path}${fl_fd}${load_fnm}_${targetdate}.txt`);
                
                let flstr = `
SET SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
    DATABASE SBX_ICB_ICBB;

    DROP TABLE ${tname}_pre;
    DROP TABLE ${tname}_pre_et;
    DROP TABLE ${tname}_pre_uv;

    CREATE TABLE ${tname}_pre
    (
        keyword varchar(500),
        category varchar(500),
        getdate VARCHAR(10),
        postdate VARCHAR(10),
        posts bigint
    )PRIMARY INDEX ( keyword );

    BEGIN LOADING ${tname}_pre ERRORFILES ${tname}_pre_et, ${tname}_pre_uv;
    set record vartext "	";
    DEFINE 
        keyword (varchar(500)),
        category (varchar(500)),
        getdate (VARCHAR(10)),
        postdate (VARCHAR(10)),
        posts (VARCHAR(20))
    FILE = ${bteq_path}${fl_fd}${load_fnm}_${targetdate}.txt;

    INSERT INTO ${tname}_pre VALUES(
        :keyword,
        :category,
        :getdate,
        :postdate,
        :posts
    );
    END LOADING;

.EXIT
`;
                
                //console.log(`fastload filename:${bteq_path}${fl_fd}${load_fnm}_load.txt`);
                fs.writeFileSync(`${bteq_path}${fl_fd}${load_fnm}_load.txt`, flstr);
                // Fastload
                console.log('  fastload 実行中');
                shell.exec(`fastload < ${bteq_path}${fl_fd}${load_fnm}_load.txt > ${bteq_path}${fl_fd}${load_fnm}_load_log.txt`);
                                
                // insert
                let iststr = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;
    DATABASE SBX_ICB_ICBB;

    INSERT INTO ${tname}

    SELECT *
    FROM ${tname}_pre a
    WHERE NOT EXISTS 
       (SELECT * FROM ${tname} b 
         WHERE a.keyword = b.keyword AND a.getdate = b.getdate );

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;

                fs.writeFileSync(`${bteq_path}${fl_fd}${load_fnm}_insert.txt`, iststr);
                console.log('  insert 実行中');
                shell.exec(`bteq < ${bteq_path}${fl_fd}${load_fnm}_insert.txt > ${bteq_path}${fl_fd}${load_fnm}_insert_log.txt`);
                result = true;
            }else{
                console.log(`${share_path}${load_fnm}_${targetdate}.txt not exists`);
            }

            resolve(result);
        });
    });
};

// load_count
function load_count() {

    return new Promise((resolve) => {
        co(function*() {
           
            let cnt;
            let btqfile = `${bteq_path}${fl_fd}${load_fnm}_count.txt`;
            let btqlog = `${bteq_path}${fl_fd}${load_fnm}_count_log.txt`;
            let cntfile = `${bteq_path}${fl_fd}${load_fnm}_count_result.txt`;
            shell.rm(cntfile);

            // select count
            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${cntfile};
.RECORDMODE OFF;
DATABASE SBX_ICB_ICBB;
SELECT
    Trim(x.cnt)
FROM 
  ( select count(*) cnt 
      from ${tname}
     where getdate = '${getdate_slash}'
  ) x;
  
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(btqfile, bteq);
            console.log('  count 実行中');
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);

            let data = fs.readFileSync(cntfile, 'utf8');
            cnt = data.match(/\d{1,}/);

            if(cnt > 0){                
                // share -> box,bteq 
                shell.cp(`${share_path}${load_fnm}_${targetdate}.txt`, `${box_path}${fl_fd}${load_fnm}_${targetdate}.txt`);
                shell.cp(`${share_path}${load_fnm}_${targetdate}.txt`, `${bteq_path}${fl_fd}${load_fnm}_${targetdate}.txt`);
            }

            console.log(`fastload : ${load_fnm}_${targetdate}.txt    ${moment().format('YYYY/MM/DD HH:mm:ss')}  end // `);
            resolve(cnt);
        });
    });
};

// report_check
function report_check() {

    return new Promise((resolve) => {
        co(function*() {
                         
            let result = false;
            
            if ( category == 'WEAR' ) {
                rprt_fnm = `report_wear_fashion`;
            } else {
                rprt_fnm = `report_insta_${category}`;
            }
            
            let f1 = `${box_path}${rp_fd}${rprt_fnm}_${targetdate}.csv`;
            let f2 = `${bteq_path}${rp_fd}${rprt_fnm}_${targetdate}.csv`;
            //console.log(f1);
            //console.log(f2);
            // check files
            if (fs.existsSync(f1)) {
                result = true;
                console.log(`report : ${f1} done`);
            }else{
                if (fs.existsSync(f2)) {
                    result = true;
                    console.log(`report : ${f2} done`);
                }
            }

            resolve(result);
        });
    });
};

// report 急上昇keywordのレポートを生成
function report_out() {

    return new Promise((resolve) => {
        co(function*() {

            console.log(`report output : ${rprt_fnm}_${targetdate}.csv    ${moment().format('YYYY/MM/DD HH:mm:ss')}  start`);
            let reportfile = `${bteq_path}${rp_fd}${rprt_fnm}_${targetdate}.csv`;
            let btqfile = `${bteq_path}${rp_fd}${rprt_fnm}.txt`;
            let btqlog = `${bteq_path}${rp_fd}${rprt_fnm}_log.txt`;

            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${reportfile}
.RECORDMODE OFF;
DATABASE SBX_ICB_ICBB;`;

            if(category == 'food'){

                bteq = `${bteq}
SELECT 'keyword, 属性１, 属性２, ${tdate_slash}　　　　　総投稿数,割合%　　　　　(先週/総投稿数),投稿数　　　　　(先週),伸び率%　　　　　(先週),投稿数　　　　　(2週間前),伸び率%　　　　　(2週間前),投稿数　　　　　(3週間前),伸び率%　　　　　(3週間前),投稿数　　　　　(4週間前),伸び率%　　　　　(4週間前)';
SELECT
    '"' || Trim(x.kwd) || '",' ||
    Trim(x.attr1) || ',' ||
    Trim(x.attr2) || ',' ||`;

            }else {
                bteq = `${bteq}

SELECT 'keyword, ${tdate_slash}　　　　　総投稿数,割合%　　　　　(先週/総投稿数),投稿数　　　　　(先週),伸び率%　　　　　(先週),投稿数　　　　　(2週間前),伸び率%　　　　　(2週間前),投稿数　　　　　(3週間前),伸び率%　　　　　(3週間前),投稿数　　　　　(4週間前),伸び率%　　　　　(4週間前)';
SELECT
    '"' || Trim(x.kwd) || '",' ||`;
            
            }

            bteq = `${bteq}

    Trim(x.a0) || ',' ||
    Trim(x.ttl_rate) || ',' ||
    Trim(x.w1) || ',' ||
    Trim(x.w1_rate) || ',' ||
    Trim(x.w2) || ',' ||
    Trim(x.w2_rate) || ',' ||
    Trim(x.w3) || ',' ||
    Trim(x.w3_rate) || ',' ||
    Trim(x.w4) || ',' ||
    Trim(x.w4_rate)
FROM (

    SELECT
        a0.keyword "kwd"
      , Coalesce( k.attribute1, '' ) "attr1"
      , Coalesce( k.attribute2, '' ) "attr2"
      , a0.posts "a0"
      , (a0.posts - a1.posts) * 10000 / a0.posts  "ttl_rate"

      , a0.posts - a1.posts "w1"
      , Coalesce( ((a0.posts - a1.posts) - (a1.posts - a2.posts)) * 10000 / CASE a1.posts WHEN a2.posts THEN 1 ELSE (a1.posts - a2.posts) end , '-') "w1_rate"

      , Coalesce( a1.posts - a2.posts  , '-') "w2"
      , Coalesce( ((a1.posts - a2.posts) - (a2.posts - a3.posts)) * 10000 / CASE a2.posts WHEN a3.posts THEN 1 ELSE  (a2.posts - a3.posts) END   , '-') "w2_rate"

      , Coalesce( a2.posts - a3.posts   , '-') "w3"
      , Coalesce( ((a2.posts - a3.posts) - (a3.posts - a4.posts)) * 10000 / CASE a3.posts WHEN a4.posts THEN 1 ELSE (a3.posts - a4.posts) END   , '-') "w3_rate"

      , Coalesce( a3.posts - a4.posts   , '-') "w4"
      , Coalesce( ((a3.posts - a4.posts) - (a4.posts - a5.posts)) * 10000 / CASE a4.posts WHEN a5.posts THEN 1 ELSE (a4.posts - a5.posts) END   , '-') "w4_rate"
      
    FROM
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate_slash}'
           AND posts > 0
      ) a0
   INNER JOIN
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate7}'
           AND posts > 0
      ) a1
      ON a0.keyword = a1.keyword
   LEFT JOIN
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate14}'
           AND posts > 0
      ) a2
      ON a0.keyword = a2.keyword
   LEFT JOIN
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate21}'
           AND posts > 0
      ) a3
      ON a0.keyword = a3.keyword
   LEFT JOIN
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate28}'
           AND posts > 0
      ) a4
      ON a0.keyword = a4.keyword
   LEFT JOIN
      ( SELECT keyword, posts
          FROM ${tname}
         WHERE getdate = '${tdate35}'
           AND posts > 0
      ) a5
    ON a0.keyword = a5.keyword

   LEFT JOIN
      ( SELECT Lower(Translate( keyword USING unicode_to_unicode_nfkc)) keyword
             , attribute1, attribute2
          FROM SBX_ICB_ICBB.food_FGS239_targets_test2
      ) k
    ON k.keyword = a0.keyword

   WHERE "w1" >= ${minposts}
     AND "w1"-"w2" > 0
     AND "w2" <> '-'
  
  ) x
  ORDER BY "w1" DESC,  "w1_rate" DESC, kwd;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;

//console.log(btqfile);
//console.log(bteq);
            fs.writeFileSync(btqfile, bteq);

            //  bteq 
            console.log('  report 出力中');
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);
        
            // bteq -> box
            shell.cp(`${reportfile}`, `${box_path}${rp_fd}${rprt_fnm}_${targetdate}.csv`);                

            console.log(`report output : ${rprt_fnm}_${targetdate}.csv    ${moment().format('YYYY/MM/DD HH:mm:ss')}  end //`);
            
            resolve();
        });
    });
};


// loadall count
function load_all_count() {

    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;

            let cnt;
            let btqfile = `${bteq_path}${fl_fd}${load_fnm}_count.txt`;
            let btqlog = `${bteq_path}${fl_fd}${load_fnm}_count_log.txt`;
            let cntfile = `${bteq_path}${fl_fd}${load_fnm}_count_result.txt`;
            shell.rm(cntfile);

            // select count
            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${cntfile}
.RECORDMODE OFF;
DATABASE SBX_ICB_ICBB;
SELECT
    Trim(x.cnt)
FROM 
  ( SELECT count(*) cnt 
      FROM ${tname}_pre
  ) x;
  
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(btqfile, bteq);
            console.log(`  count 実行中   :  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);

            let data = fs.readFileSync(cntfile, 'utf8');
            cnt = data.match(/\d{1,}/);

            if( cnt == fcnt){
                console.log(`   fastload : ${cnt}件`);
                result = true;
            }
            
            resolve(result);
        });
    });
};

// all insert
function all_insert() {

    return new Promise((resolve) => {
        co(function*() {
            
            let result = false;
            // insert
            for (let i = 0; i < tbls.length; i++) {
                let iststr = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;

    DATABASE SBX_ICB_ICBB;
    INSERT INTO ${tbls[i].datatbl}

    SELECT *
    FROM ${tname}_pre a
    WHERE NOT EXISTS 
        (SELECT * FROM ${tbls[i].datatbl} b 
          WHERE a.keyword = b.keyword AND a.getdate = b.getdate )
    AND a.keyword in (select keyword from ${tbls[i].kwdtbl});

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;    
                fs.writeFileSync(`${bteq_path}${fl_fd}${load_fnm}_${tbls[i].ctgry}.txt`, iststr);
                console.log(`  insert 実行中 : ${tbls[i].ctgry}  :  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                shell.exec(`bteq < ${bteq_path}${fl_fd}${load_fnm}_insert_${tbls[i].ctgry}.txt > ${bteq_path}${fl_fd}${load_fnm}_insert_${tbls[i].ctgry}.log`);
            }

            shell.cp(`${bteq_path}${fl_fd}${load_fnm}`, `${box_path}${fl_fd}`);            
            result = true;
            resolve(result);
        });
    });
};


// load all
function load_all() {

    return new Promise((resolve) => {
        co(function*() {
            let result = false;
            // check share files
            if (fs.existsSync(`${share_path}${load_fnm}_${targetdate}.txt`)) {
                console.log(`fastload : ${load_fnm}_${targetdate}.txt   ${moment().format('YYYY/MM/DD HH:mm:ss')}  start`);
                // share -> bteq
                shell.cp(`${share_path}${load_fnm}_${targetdate}.txt`, `${bteq_path}${fl_fd}${load_fnm}_${targetdate}.txt`);
                
                tname = 'SBX_ICB_ICBB.fashion_FGS239_insta_posts_all';
                let flstr = `
SET SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
    DATABASE SBX_ICB_ICBB;

    DROP TABLE ${tname}_pre;
    DROP TABLE ${tname}_pre_et;
    DROP TABLE ${tname}_pre_uv;

    CREATE TABLE ${tname}_pre
    (
        keyword varchar(500),
        category varchar(500),
        getdate VARCHAR(10),
        postdate VARCHAR(10),
        posts bigint
    )PRIMARY INDEX ( keyword );

    BEGIN LOADING ${tname}_pre ERRORFILES ${tname}_pre_et, ${tname}_pre_uv;
    set record vartext "	";
    DEFINE 
        keyword (varchar(500)),
        category (varchar(500)),
        getdate (VARCHAR(10)),
        postdate (VARCHAR(10)),
        posts (VARCHAR(20))
    FILE = ${bteq_path}${fl_fd}${load_fnm}_${targetdate}.txt;

    INSERT INTO ${tname}_pre VALUES(
        :keyword,
        :category,
        :getdate,
        :postdate,
        :posts
    );
    END LOADING;

.EXIT
`;
                
                //console.log(`fastload filename:${bteq_path}${fl_fd}${load_fnm}_load.txt`);
                fs.writeFileSync(`${bteq_path}${fl_fd}${load_fnm}_load.txt`, flstr);
                // Fastload
                console.log('  fastload 実行中');
                shell.exec(`fastload < ${bteq_path}${fl_fd}${load_fnm}_load.txt > ${bteq_path}${fl_fd}${load_fnm}_load_log.txt`);

                //fcnt
                let lines = fs.readFileSync(`${share_path}${load_fnm}_${targetdate}.txt`).toString().split("\n");
                if(lines[lines.length -1].length == 0) lines.pop();
                console.log(`  lines.length : ${lines.length}`);
                fcnt = lines.length;

                let rt = yield load_all_count();
                if( rt ) {
                    rt = yield all_insert();
                    if( rt ) {
                        result = true;
                    } else {
                        console.log('  insert all failed');
                    }
                } else {
                    console.log('  fastload all failed');
                }
            }

            resolve(result);
        });
    });
};


// fastload all
function fastload_all() {
    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;            
            category = 'all'
            let rt = yield load_check();
            if(!rt) {
                result = yield load_all();
            }
            resolve(result);
        });
    });
};


// max output
function max_output() {

    return new Promise((resolve) => {
        co(function*() {

            let result = false;

            // FGS239_insta_targets_all_max.txt
            // FGS239_WEAR_targets_fashion_max.txt
            let maxfnm;
            if(category == 'WEAR') {
                maxfnm = 'FGS239_wear_targets_fashion_max.txt';

            }else if(category == 'all'){
                maxfnm = 'FGS239_insta_targets_all_max.txt';

            }else{
                return;
            }

            console.log(`max output : ${maxfnm}    ${moment().format('YYYY/MM/DD HH:mm:ss')}  start`);
            
            let f1 = `${share_path}targets/${maxfnm}`;
            if (fs.existsSync(f1)) fs.renameSync(f1, `${f1}_${today}bak`);

            let maxfile = `${bteq_path}${mx_fd}${maxfnm}`;
            let btqfile = `${bteq_path}${mx_fd}btq_${maxfnm}`;
            let btqlog = `${bteq_path}${mx_fd}btq_${maxfnm}.log`;

            if (fs.existsSync(maxfile)) fs.renameSync(maxfile, `${maxfile}_${today}bak`);

            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${maxfile}
.RECORDMODE OFF;
DATABASE SBX_ICB_ICBB;`;

            if(category == 'WEAR'){

                bteq = `${bteq}
SELECT Trim(a.keyword) || '///' || Trim(Nvl(b.maxposts,0))
  FROM fashion_FGS239_targets_fashion a
  LEFT JOIN (
      SELECT keyword, Max(posts) maxposts
        FROM fashion_FGS239_wear_posts_fashion 
       WHERE getdate >= '${mdate}'
       GROUP BY 1) b
    ON a.keyword = b.keyword
 ORDER BY 1;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            }

            if(category == 'all'){

                bteq = `${bteq}
SELECT Trim(k.keyword) || '///' || Trim(Nvl(d.maxposts,0))
  FROM (
      SELECT DISTINCT a.keyword
        FROM (
            SELECT keyword FROM ${tbls[0].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[1].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[2].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[3].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[4].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[5].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[6].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[7].kwdtbl} UNION ALL
            SELECT keyword FROM ${tbls[8].kwdtbl}
             ) a
        ) k
  LEFT JOIN (
      SELECT b.keyword, Max(b.posts) maxposts
        FROM (
            SELECT keyword,posts FROM ${tbls[0].datatbl} WHERE getdate >= '${mdate}' UNION ALL
            SELECT keyword,posts FROM ${tbls[1].datatbl} WHERE getdate >= '${mdate}' UNION ALL
            SELECT keyword,posts FROM ${tbls[2].datatbl} WHERE getdate >= '${mdate}' UNION ALL
            SELECT keyword,posts FROM ${tbls[3].datatbl} WHERE getdate >= '${mdate}' UNION ALL
            SELECT keyword,posts FROM ${tbls[4].datatbl} WHERE getdate >= '${mdate}' UNION ALL
            SELECT keyword,posts FROM ${tbls[5].datatbl} WHERE getdate >= '${mdate}' UNION ALL
            SELECT keyword,posts FROM ${tbls[6].datatbl} WHERE getdate >= '${mdate}' UNION ALL
            SELECT keyword,posts FROM ${tbls[7].datatbl} WHERE getdate >= '${mdate}' UNION ALL
            SELECT keyword,posts FROM ${tbls[8].datatbl} WHERE getdate >= '${mdate}' 
             ) b
       GROUP BY 1
        ) d
    ON k.keyword = d.keyword
 ORDER BY 1;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            }

            fs.writeFileSync(btqfile, bteq);

            //  bteq 
            console.log('  maxfile 出力中');
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);
        
            // bteq -> share
            shell.cp(`${maxfile}`, `${share_path}targets/${maxfnm}`);                

            console.log(`max output : ${maxfnm}    ${moment().format('YYYY/MM/DD HH:mm:ss')}  end //`);
            
            resolve();
        });
    });
};



// run
co(function*() {
    
    if (getdate_hyphen != tdate){
        console.log('targetdate is not Sunday.');
    } else {

        if (!cli['type'] || cli['type'] == 1) {
            // instagram posts (food)
            category = 'food';
            tname = tname_insta_food;
            minposts = 1000;
            let rt = yield fastload();
            if (rt) yield report();
        }

        if (!cli['type'] || cli['type'] == 2) {
            // instagram posts (fashion)
            category = 'fashion';
            tname = tname_insta_fashion;
            minposts = 1000;
            let rt = yield fastload();
            if (rt) yield report();
        }


        // WEAR !! //
        if (!cli['type'] || cli['type'] == 3) {
            // Wear.jp posts (fashion)
            category = 'WEAR';
            tname = tname_wear_fashion;
            minposts = 10;
            let rt = yield fastload();
            if (rt) yield report();
            
        }


        if (!cli['type'] || cli['type'] == 4) {
            // watch posts (watch)
            category = 'watch';
            tname = tname_insta_watch;
            minposts = 50;
            let rt = yield fastload();
            if (rt) yield report();
        }
        
        if (!cli['type'] || cli['type'] == 5) {
            // life posts (life)
            category = 'life';
            tname = tname_insta_life;
            minposts = 1000;
            let rt = yield fastload();
            if (rt) yield report();
        }

        if (!cli['type'] || cli['type'] == 6) {
            // flower posts (flower)
            category = 'flower';
            tname = tname_insta_flower;
            minposts = 50;
            let rt = yield fastload();
            if (rt) yield report();
        }

        if (!cli['type'] || cli['type'] == 7) {
            // interior posts (interior)
            category = 'interior';
            tname = tname_insta_interior;
            minposts = 100;
            let rt = yield fastload();
            if (rt) yield report();
        }

        if (!cli['type'] || cli['type'] == 8) {
            // kitchen posts (kitchen)
            category = 'kitchen';
            tname = tname_insta_kitchen;
            minposts = 100;
            let rt = yield fastload();
            if (rt) yield report();
        }

        if (!cli['type'] || cli['type'] == 9) {
            // cosme posts
            category = 'cosme';
            tname = tname_insta_cosme;
            minposts = 1000;
            let rt = yield fastload();
            if (rt) yield report();
        }

        if (!cli['type'] || cli['type'] == 10) {
            // wine posts
            category = 'wine';
            tname = tname_insta_wine;
            minposts = 50;
            let rt = yield fastload();
            if (rt) yield report();
        }

        if (cli['type'] == 0) {
        // if (!cli['type'] || cli['type'] == 0) {
        //     // all
        //     let rt = yield fastload_all();
        //     if (rt) {
        //         category = 'food';
        //         tname = tname_insta_food;
        //         minposts = 1000;
        //         yield report();

        //         category = 'fashion';
        //         tname = tname_insta_fashion;
        //         minposts = 1000;
        //         yield report();

        //         category = 'watch';
        //         tname = tname_insta_watch;
        //         minposts = 50;
        //         yield report();

        //         category = 'life';
        //         tname = tname_insta_life;
        //         minposts = 1000;
        //         yield report();

        //         category = 'flower';
        //         tname = tname_insta_flower;
        //         minposts = 50;
        //         yield report();

        //         category = 'interior';
        //         tname = tname_insta_interior;
        //         minposts = 100;
        //         yield report();

        //         category = 'kitchen';
        //         tname = tname_insta_kitchen;
        //         minposts = 100;
        //         yield report();
        
                    category = 'all';
                    yield max_output();

        //     }
        //     
        }

        console.log(`FGS239-insta-posts  ${moment().format('YYYY/MM/DD HH:mm:ss')}  end //`);
    }

});

