'use strict';

const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const shell = require('shelljs');
const co = require('co');
const util = new (require('./util_co.js'));
let today = moment().format('YYYYMMDD');

// parse args
const cli = commandArgs([
    { name: 'time', alias: 't', type: String }
]);

// reset time
if (cli['time']) {
    today = cli['time'];
}

const tools_path = 'D:/work/tool/tools/'; 
const share_path = 'D:/share/'; 

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

let target = 'FGS247_insta_official';



// load
function load(filename, tablename) {

    return new Promise((resolve) => {
        co(function*() {
           
            // check share files
            if (fs.existsSync(`${share_path}${target}_${today}.txt`)) {

                // rm 
                shell.rm(`${tools_path}output/*${target}*`);
                
                // share -> input_fastload 
                shell.cp(`${share_path}${target}_${today}.txt`, `${tools_path}input_fastload/${target}.txt`);

                // Fastload
                shell.exec(`node src/makeFastload.js -p ${target}_pre -d`);

                // insert
                let datapath = `${tools_path}output/${target}_fastload_data.txt`;
                if (fs.existsSync(datapath)) {

                    let filepath1 = `${tools_path}bteq/${target}.txt`;
                    let bteq1 = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;

    INSERT INTO SBX_ICB_ICBB.fashion_${target}

    SELECT *
    FROM SBX_ICB_ICBB.fashion_${target}_pre a
    WHERE NOT EXISTS 
       (SELECT * FROM SBX_ICB_ICBB.fashion_${target} b 
         WHERE a.keyword = b.keyword AND a.getdate = b.getdate );

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;

                    fs.writeFileSync(filepath1, bteq1);
                    shell.exec(`bteq < bteq/${target}.txt`);
                
                }else{
                    let deletepath = `${tools_path}input_fastload/${target}.txt`;
                    fs.unlink(deletepath);
                }
            }

            resolve();
        });
    });
};

// run
co(function*() {
    
    yield load();
    
    console.log('FGS247_insta_official end //');

});











