'use strict';

const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const shell = require('shelljs');
const co = require('co');

// parse args
const cli = commandArgs([
    { name: 'type', alias: 'p', type: Number },
    { name: 'time', alias: 't', type: String }  // startdate YYYY-MM-DD
]);

const lastSunday = moment().subtract(6,'d').day(0).format('YYYY-MM-DD');
const startdate = cli['time'] ? cli['time'] : lastSunday;  //YYYY-MM-DD

const tlpath = 'D:/work/tool/tools/bteq/FGS239_insta_GMS/'; 
if(!fs.existsSync(tlpath)){
    fs.mkdirSync(tlpath);
} 
const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

// do Insert
function doInsert(tgttbl, totbl, clm, gr1, gr2, gr3) {
    return new Promise((resolve) => {
        co(function*() {

            //let dt = startdate;
            let dt = moment(startdate).day(0).format('YYYY-MM-DD');
            console.log(dt,totbl);
            let st = moment().format('YYYY/MM/DD HH:mm:ss');
            let sql = yield getSql(dt, lastSunday, tgttbl, totbl, clm, gr1, gr2, gr3);

            let filepath = `${tlpath}${totbl}_insert.txt`;
            fs.writeFileSync(filepath, sql);
            shell.exec(`bteq < ${filepath} > ${tlpath}${totbl}_insert_log_${moment().format('YYYYMMDD')}.txt`);

            console.log(`${totbl} insert end ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}//`);
            resolve();
        });
    });
};

// get sql
const getSql = function (dt, lastSunday, tgttbl, totbl, clm, gr1, gr2, gr3) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

--create keywords
CREATE VOLATILE MULTISET TABLE FGS239_keywords AS 
(
SELECT DISTINCT
       RegExp_Replace(Lower(keyword) ,'[ー‐―−]','-',1,0,'c') (VARCHAR (50)) AS keyword
  FROM SBX_ICB_ICBB.${tgttbl}
) WITH DATA
NO PRIMARY INDEX
ON COMMIT PRESERVE ROWS
;`;

            while(dt <= lastSunday){
                let dt8 = dt.replace(/[^0-9]/g, "");
                let nxtdt = moment(dt).add(7,'d').format('YYYY-MM-DD');
                sql = sql + `
                
--create orders
CREATE VOLATILE MULTISET TABLE FGS239_orders AS
(
SELECT `;
                if(tgttbl == 'food_FGS239_targets'){
                    sql = `${sql}
        CASE b.g1 WHEN ${gr1} THEN 's' 
                  ELSE 'f' End (CHAR(1)) AS gnr
        , `;
                
                }else if(tgttbl == 'fashion_FGS239_targets_fashion'){
                    sql = `${sql}
        CASE b.g1 WHEN ${gr1} THEN 's'
                  WHEN ${gr2} THEN 'f'  
                  ELSE 'g' End (CHAR(1)) AS gnr
        , `;    
                }
                sql = `${sql}RegExp_Replace(Lower(Translate( item_name USING unicode_to_unicode_nfkc)) ,'[ー‐―−]','-',1,0,'c') (VARCHAR (250)) AS item_name
        , Sum(sub_total_amt (BIGINT)) AS gms
        , Sum(units (BIGINT)) AS units
  FROM ua_view_mk_ichiba.red_basket_detail_tbl a
 INNER JOIN ua_view_mk_ichiba.item_genre_dimension b
    ON a.genre_id = b.genre_id `;
                if(tgttbl == 'food_FGS239_targets'){
                    sql = `${sql}
   AND g1 IN (${gr1}, ${gr2}) --food,sweets`;
                
                }else if(tgttbl == 'fashion_FGS239_targets_fashion'){
                    sql = `${sql}
   AND g1 IN (${gr1}, ${gr2}, ${gr3}) --ladies fashion,mens fashion,bag`;

                }else if(tgttbl == 'wine_FGS239_targets'){
                    sql = `${sql}
   AND g1 = ${gr1} AND g2 = ${gr2} --wine`;

                }else{
                    sql = `${sql}
   AND g1 = ${gr1}`;

                }
                sql = `${sql}
   AND reg_datetime >= '${dt} 00:00:00'
   AND reg_datetime <  '${nxtdt} 00:00:00'
   AND price > 0
   AND units > 0
   AND cancel_datetime IS NULL

 GROUP BY 1`;
                if(tgttbl == 'food_FGS239_targets' || tgttbl == 'fashion_FGS239_targets_fashion'){
                    sql = `${sql},2`;
                }
                sql = `${sql}
) WITH DATA
NO PRIMARY INDEX
ON COMMIT PRESERVE ROWS
;

INSERT INTO SBX_ICB_ICBB.${totbl}
    ( keyword, startdate, ${clm} )

-- test
--.set width 20000;
--.EXPORT DATA FILE=${tlpath}${totbl}_insert_test01.txt
--.RECORDMODE OFF

SELECT oreplace(k.keyword,'-','ー') AS keyword
     , '${dt8}' AS startdate`;
                if(tgttbl == 'food_FGS239_targets'){
                    sql = `${sql}
     , Nvl(s.GMS,0) GMS_sweets
     , Nvl(s.units,0) units_sweets
     , Nvl(f.GMS,0) GMS_food
     , Nvl(f.units,0) units_food`;
                } else if(tgttbl == 'fashion_FGS239_targets_fashion'){
                    sql = `${sql}
     , Nvl(s.GMS,0) GMS_ladies
     , Nvl(s.units,0) units_ladies
     , Nvl(f.GMS,0) GMS_mens
     , Nvl(f.units,0) units_mens
     , Nvl(g.GMS,0) GMS_bag
     , Nvl(g.units,0) units_bag`;
                } else {
                    sql = `${sql}
     , Nvl(s.GMS,0) GMS
     , Nvl(s.units,0) units`;
                }
                sql = `${sql}
  FROM FGS239_keywords k

  LEFT JOIN
    ( SELECT a.keyword AS keyword, Sum(gms) AS gms, Sum(units) AS units
        FROM FGS239_keywords a
       INNER JOIN FGS239_orders b
          ON Position(a.keyword IN b.item_name) > 0`;
                if(tgttbl == 'food_FGS239_targets' || tgttbl == 'fashion_FGS239_targets_fashion'){
                    sql = `${sql}
         AND b.gnr = 's'`;
                }
                sql = `${sql}
       GROUP BY 1) AS s ON k.keyword = s.keyword`;

                if(tgttbl == 'food_FGS239_targets' || tgttbl == 'fashion_FGS239_targets_fashion'){
                    sql = `${sql}
  LEFT JOIN
    ( SELECT a.keyword AS keyword, Sum(gms) AS gms, Sum(units) AS units
        FROM FGS239_keywords a
       INNER JOIN FGS239_orders b
          ON Position(a.keyword IN b.item_name) > 0
         AND b.gnr = 'f'
       GROUP BY 1) AS f ON k.keyword = f.keyword`;
                }

                if(tgttbl == 'fashion_FGS239_targets_fashion'){
                    sql = `${sql}
  LEFT JOIN
    ( SELECT a.keyword AS keyword, Sum(gms) AS gms, Sum(units) AS units
        FROM FGS239_keywords a
       INNER JOIN FGS239_orders b
          ON Position(a.keyword IN b.item_name) > 0
         AND b.gnr = 'g'
       GROUP BY 1) AS g ON k.keyword = g.keyword`;
                }

                sql = `${sql}
 WHERE NOT EXISTS 
    (  SELECT * FROM SBX_ICB_ICBB.${totbl} t
        WHERE t.keyword = oreplace(k.keyword,'-','ー') AND t.startdate = '${dt8}');

-- test
--ORDER BY 1;

`;   
        
                dt = moment(dt).add(7,'d').format('YYYY-MM-DD');  //7日後
            }

            sql = `${sql}
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            resolve(sql);
        });
    });
};

// run
co(function*() {

    if (!cli['type'] || cli['type'] == 1) {
        // instagram posts (food)
        let tgttbl = 'food_FGS239_targets';
        let totbl = 'food_FGS239_insta_GMS';
        let clm = 'GMS_sweets, units_sweets, GMS_food, units_food';
        let gr1 = 551167; //'AND i.g1 = 551167 --sweets';
        let gr2 = 100227; //'AND i.g1 = 100227 --food';
        yield doInsert(tgttbl, totbl, clm, gr1, gr2);
    }
    if (!cli['type'] || cli['type'] == 2) {
        // instagram posts (fashion)
        let tgttbl = 'fashion_FGS239_targets_fashion';
        let totbl = 'fashion_FGS239_insta_GMS_fashion';
        let clm = 'GMS_ladies, units_ladies, GMS_mens, units_mens, GMS_bag, units_bag';
        let gr1 = 100371; //'AND i.g1 = 100371 --ladies fashion';
        let gr2 = 551177; //'AND i.g1 = 551177 --mens fashion';
        let gr3 = 216131; //'AND i.g1 = 216131 --bag';
        yield doInsert(tgttbl, totbl, clm, gr1, gr2, gr3);
    }
    if (!cli['type'] || cli['type'] == 3) {
        // instagram posts (watch)
        let tgttbl = 'fashion_FGS239_targets_watch';
        let totbl = 'fashion_FGS239_insta_GMS_watch';
        let clm = 'GMS, units';
        let gr1 = 558929; //'AND i.g1 = 558929 --watch';
        yield doInsert(tgttbl, totbl, clm, gr1);
    }
    if (!cli['type'] || cli['type'] == 4) {
        // instagram posts (flower)
        let tgttbl = 'flower_FGS239_targets';
        let totbl = 'flower_FGS239_insta_GMS';
        let clm = 'GMS, units';
        let gr1 = 100005; //'AND i.g1 = 100005 --flower';
        yield doInsert(tgttbl, totbl, clm, gr1);
    }
    if (!cli['type'] || cli['type'] == 5) {
        // instagram posts (kitchen)
        let tgttbl = 'kitchen_FGS239_targets';
        let totbl = 'kitchen_FGS239_insta_GMS';
        let clm = 'GMS, units';
        let gr1 = 558944; //'AND i.g1 = 558944 --kitchen';
        yield doInsert(tgttbl, totbl, clm, gr1);
    }
    if (!cli['type'] || cli['type'] == 6) {
        // instagram posts (interior)
        let tgttbl = 'interior_FGS239_targets';
        let totbl = 'interior_FGS239_insta_GMS';
        let clm = 'GMS, units';
        let gr1 = 100804; //'AND i.g1 = 100804 --interior';
        yield doInsert(tgttbl, totbl, clm, gr1);
    }
    if (!cli['type'] || cli['type'] == 7) {
        // instagram posts (cosme)
        let tgttbl = 'cosme_FGS239_targets';
        let totbl = 'cosme_FGS239_insta_GMS';
        let clm = 'GMS, units';
        let gr1 = 100939; //'AND i.g1 = 100804 --interior';
        yield doInsert(tgttbl, totbl, clm, gr1);
    }
    if (!cli['type'] || cli['type'] == 8) {
        // instagram posts (wine)
        let tgttbl = 'wine_FGS239_targets';
        let totbl = 'wine_FGS239_insta_GMS';
        let clm = 'GMS, units';
        let gr1 = 510915;
        let gr2 = 100317;
        yield doInsert(tgttbl, totbl, clm, gr1, gr2);
    }


    console.log('FGS239-insta-GMS end //');

});