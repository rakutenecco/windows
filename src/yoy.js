'use strict';


const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const isFile = require('is-file');
const shell = require('shelljs');
const co = require('co');
const iconv = require('iconv-lite');
const excel = require('exceljs-rtl');
const util = new (require('./util_co.js'));

let dirPath_bteq = `D:\/work\/tool\/tools\/bteq\/yoy`;
//let dirPath_box = `C:\/Users\/ts-rie.naruo\/Box Sync\/fashion_kuriyama3`;
let filepath_bteq = `D:\/work\/tool\/tools\/bteq\/yoy.txt`;
let filepath_export_out = '';
let fileName = moment().format('YYYYMMDD') + '提出【ツール】Item別YoY.xlsx';
// let filepath2 = `C:\/Users\/ts-rie.naruo\/Box Sync\/fashion_kuriyama3\/${fileName}`;
let filepath2 = `D:\/Box Sync\/EC Consulting Department\/ecc_fashion\/genre_moving_average_7_yoy\/${fileName}`;
// filepath2 = `D:\/work\/tool\/tools\/bteq\/yoy\/${fileName}`;

let filepath_yoy_3th = 'D:\/work\/tool\/tools\/bteq\/yoy\/yoy_3th.tsv';
let filepath_yoy_6th = 'D:\/work\/tool\/tools\/bteq\/yoy\/yoy_6th.tsv';
let filepath_yoy_lastyear1 = 'D:\/work\/tool\/tools\/bteq\/yoy\/yoy_lastyear.tsv';
let filepath_yoy_lastyear2 = 'D:\/work\/tool\/tools\/bteq\/yoy\/yoy_lastyear.txt';
let filepath_yoy_today1 = 'D:\/work\/tool\/tools\/bteq\/yoy\/yoy_today.tsv';
let filepath_yoy_today2 = 'D:\/work\/tool\/tools\/bteq\/yoy\/yoy_today.txt';
let filepath_yoy_yesterday1 = 'D:\/work\/tool\/tools\/bteq\/yoy\/yoy_yesterday.tsv';
let filepath_yoy_yesterday2 = 'D:\/work\/tool\/tools\/bteq\/yoy\/yoy_yesterday.txt';

// parse args
const cli = commandArgs([
    { name: 'test', alias: 't', type: Boolean }
]);



let sql_tmp = `
SELECT

t1."date",
t1."year",
t1."month",
t1."day",
t1.genre_id,
t1.genre_name1,
t1.genre_name2,
t1.genre_name3,
t1.genre_name4,
t1.genre_name5,
t1.genre_name6,
t1.sum_today,
t1.num_today,
t2.sum_7day,
t2.num_7day,
t2.total_day

FROM
(
select
  (select (SELECT DATE '{0}' (FORMAT 'YYYYMMDD') (CHAR(8)))) as "date",
  (select (SELECT DATE '{0}' (FORMAT 'YYYY') (CHAR(4)))) as "year",
  (select (SELECT DATE '{0}' (FORMAT 'MM') (CHAR(2)))) as "month",
  (select (SELECT DATE '{0}' (FORMAT 'DD') (CHAR(2)))) as "day",
  t.genre_id as genre_id,
  i.gn1 as genre_name1,
  i.gn2 as genre_name2,
  i.gn3 as genre_name3,
  i.gn4 as genre_name4,
  i.gn5 as genre_name5,
  i.gn6 as genre_name6,
  sum(t.sub_total_amt) as sum_today, --      "当日売上合計", -- 販売売上
  sum(t.units) as num_today --  "当日売上合計個数" -- 販売個数

  from UA_VIEW_MK_ICHIBA.red_basket_detail_tbl t
  inner join UA_VIEW_MK_ICHIBA.item_genre_dimension201610 i

  on t.reg_datetime >= (select (SELECT DATE '{0}' (FORMAT 'YYYY-MM-DD') (CHAR(10))) || ' 00:00:00') and t.reg_datetime < (select (SELECT DATE '{0}' + interval '1' day (FORMAT 'YYYY-MM-DD') (CHAR(10))) || ' 00:00:00')
  and t.sub_total_amt > 0
  and t.cancel_datetime is null
  and  t.genre_id = i.genre_id

  group by 5,6,7,8,9,10,11
) t1

inner join
(
  select
  t.genre_id as genre_id,
  sum(t.sub_total_amt) as sum_7day  , -- "累計売上合計", -- 販売売上
  sum(t.units) as num_7day, -- "累計売上合計個数", -- 販売個数
  7 as total_day -- "累計日数"

  from UA_VIEW_MK_ICHIBA.red_basket_detail_tbl t
  inner join UA_VIEW_MK_ICHIBA.item_genre_dimension201610 i

  on t.reg_datetime >= (select (SELECT DATE '{0}' - interval '7' day (FORMAT 'YYYY-MM-DD') (CHAR(10))) || ' 00:00:00') and t.reg_datetime < (select (SELECT DATE '{0}' (FORMAT 'YYYY-MM-DD') (CHAR(10))) || ' 00:00:00')
  and t.sub_total_amt > 0
  and t.cancel_datetime is null
  and  t.genre_id = i.genre_id

  group by 1
) t2

on t1.genre_id = t2.genre_id
`;


// fastload用にheadを作成する。
function getYOY1() {

    let filepath_export = `D:/work/tool/tools/bteq/yoy/yoy_today.txt`;
    let filepath_export2 = `D:\/work\/tool\/tools\/bteq\/yoy\/yoy_today.txt`;
    let day = moment().add(-1, 'days').format('YYYY-MM-DD');
    let sql = sql_tmp.replace(/\{0\}/g, day);
    let sql_order = `t1.genre_id`;
    let run_bteq = `bteq < bteq/yoy.txt`;
    util.makeBteqDataTab(filepath_bteq, filepath_export, filepath_export2, sql, sql_order, run_bteq, filepath_export_out);
};


// fastload用にheadを作成する。
function getYOY2() {

    let filepath_export = `D:/work/tool/tools/bteq/yoy/yoy_yesterday.txt`;
    let filepath_export2 = `D:\/work\/tool\/tools\/bteq\/yoy\/yoy_yesterday.txt`;
    let day = moment().add(-2, 'days').format('YYYY-MM-DD');
    let sql = sql_tmp.replace(/\{0\}/g, day);
    let sql_order = `t1.genre_id`;
    let run_bteq = `bteq < bteq/yoy.txt`;
    util.makeBteqDataTab(filepath_bteq, filepath_export, filepath_export2, sql, sql_order, run_bteq, filepath_export_out);
};


// fastload用にheadを作成する。
function getYOY3() {

    let filepath_export = `D:/work/tool/tools/bteq/yoy/yoy_lastyear.txt`;
    let filepath_export2 = `D:\/work\/tool\/tools\/bteq\/yoy\/yoy_lastyear.txt`;
    let today = moment().add(-1, 'days').format('YYYY-MM-DD');
    let day = moment(today).add(-1, 'years').format('YYYY-MM-DD');
    let sql = sql_tmp.replace(/\{0\}/g, day);
    let sql_order = `t1.genre_id`;
    let run_bteq = `bteq < bteq/yoy.txt`;
    util.makeBteqDataTab(filepath_bteq, filepath_export, filepath_export2, sql, sql_order, run_bteq, filepath_export_out);
};


//
function list(filepath) {

    let li = [];
    let file = fs.readFileSync(filepath, 'utf-8');
    let rows = file.replace(/\r/g, '').split('\n');

    for (let i = 1; i < rows.length; i++) {
        let cells = rows[i].split('	');
        li.push(cells);
    }

    return li;
};

//
function getRows(genre_id, list) {

    for (let i = 0; i < list.length; i++) {
        if (list[i][4] === genre_id) {
            return list[i];
        }
    }
    return [];
};

//
function getRowTarget(genre_names, list) {

    for (let i = 0; i < list.length; i++) {
        if (list[i][0] === genre_names) {
            return list[i];
        }
    }
    return [];
};

// 3thに複数の行を集合して扱う
function getRows3th(list) {

    let rows = {}
    let total_amount_all = 0;
    for (let i = 0; i < list.length; i++) {

        if (!list[i][4]) continue;

        let date = list[i][0];
        let year = list[i][1];
        let month = list[i][2];
        let day = list[i][3];
        let genre_id = list[i][4];
        let genre_name1 = list[i][5];
        let genre_name2 = list[i][6];
        let genre_name3 = list[i][7];
        let genre_name4 = list[i][8];
        let genre_name5 = list[i][9];
        let genre_name6 = list[i][10];
        let sum_today = list[i][11];
        let num_today = list[i][12];
        let sum_7day = list[i][13];
        let num_7day = list[i][14];
        let total_day = list[i][15];
        let genre_name_3th = genre_name1 + '<>' + genre_name2 + '<>' + genre_name3;
        total_amount_all += parseInt(sum_7day, 10);

        // rows[genre_name_3th]が未登録なら初期化する
        if (!rows[genre_name_3th]) {
            rows[genre_name_3th] = [];
        }

        // genre_name_3thごとに、データを個別に格納する
        rows[genre_name_3th].push([genre_name_3th, date, year, month, day, genre_name1, genre_name2, genre_name3, sum_today, num_today, sum_7day, num_7day, total_day]);
    }

    // genre_name_3thごとに重複したデータを合計し、配列として整形する。
    let new_rows = [];
    for (let genre_name_3th in rows) {
        let _rows = rows[genre_name_3th];
        let new_row = ['', '', '', '', '', '', '', '', 0, 0, 0, 0, 0];
        for (let i = 0; i < _rows.length; i++) {
            new_row[0] = _rows[i][0]; // genre_name_3th
            new_row[1] = _rows[i][1]; // date
            new_row[2] = _rows[i][2]; // year
            new_row[3] = _rows[i][3]; // month
            new_row[4] = _rows[i][4]; // day
            new_row[5] = _rows[i][5]; // genre_name1
            new_row[6] = _rows[i][6]; // genre_name2
            new_row[7] = _rows[i][7]; // genre_name3
            new_row[8] = parseInt(new_row[8], 10) + parseInt(_rows[i][8], 10); // sum_today
            new_row[9] = parseInt(new_row[9], 10) + parseInt(_rows[i][9], 10); // num_today
            new_row[10] = parseInt(new_row[10], 10) + parseInt(_rows[i][10], 10); // sum_7day
            new_row[11] = parseInt(new_row[11], 10) + parseInt(_rows[i][11], 10); // num_7day
            new_row[12] = _rows[i][12]; // total_day
        }
        new_rows.push(new_row);
    }

    return [new_rows, total_amount_all];
};




// 6th 作成　当日日付データを用いて作成
function make6th_new(li_today, li_yesterday, li_lastyear, filepath) {

    // left side of 'Item 6th genre' sheet.
    let to_genre_id;
    let to_genre_name1;
    let to_genre_name2;
    let to_genre_name3;
    let to_genre_name4;
    let to_genre_name5;
    let to_genre_name6;
    let gms; // 前日GMS
    let ratio_7day; // 7日移動平均乖離率
    let ave_7day; // 7日移動平均
    let total_amount; // 累計
    let total_amount_yoy; // 累計YoY
    let dod; // DoD
    let la_total_amount; // 去年累計

    // right side of 'Item 6th genre' sheet.
    let ye_genre_id;
    let ye_gms; // 前々日GMS

    //
    let data = ['to_genre_id', 'to_genre_name1', 'to_genre_name2', 'to_genre_name3', 'to_genre_name4', 'to_genre_name5', 'to_genre_name6', 'ye_genre_id', 'ye_gms', 'la_total_amount', 'gms', 'ave_7day', 'total_amount', 'ratio_7day', 'total_amount_yoy', 'dod'].join('  ') + '\n';
    fs.appendFileSync(filepath, data, 'utf-8');

    // write each files
    for (let i = 0; i < li_today.length; i++) {

        if (!li_today[i][4]) return;

        to_genre_id = li_today[i][4];
        to_genre_name1 = li_today[i][5];
        to_genre_name2 = li_today[i][6];
        to_genre_name3 = li_today[i][7];
        to_genre_name4 = li_today[i][8];
        to_genre_name5 = li_today[i][9];
        to_genre_name6 = li_today[i][10];

        let row_yesterday = getRows(to_genre_id, li_yesterday);
        let row_lastyear = getRows(to_genre_id, li_lastyear);
        ye_genre_id = row_yesterday[4] ? row_yesterday[4] : '-';
        ye_gms = row_yesterday[11] ? row_yesterday[11] : '-';
        la_total_amount = row_lastyear[13] ? row_lastyear[13] : '-';

        //
        gms = li_today[i][11] ? li_today[i][11] : '-';
        ave_7day = (li_today[i][13] && li_today[i][15] && li_today[i][15] != 0) ? li_today[i][13] / li_today[i][15] : '-';
        total_amount = (li_today[i][13]) ? li_today[i][13] : '-';
        ratio_7day = (gms && ave_7day && ave_7day != 0 && gms !== '-' && ave_7day !== '-') ? (gms - ave_7day) / ave_7day : '-';
        total_amount_yoy = (total_amount && row_lastyear[13] && row_lastyear[13] != 0 && total_amount != '-') ? (total_amount / row_lastyear[13]) - 1 : '-';
        dod = (gms && ye_gms && ye_gms !== '-' && gms !== '-') ? (gms - ye_gms) : '-';

        let data = [to_genre_id, to_genre_name1, to_genre_name2, to_genre_name3, to_genre_name4, to_genre_name5, to_genre_name6, ye_genre_id, ye_gms, la_total_amount, gms, ave_7day, total_amount, ratio_7day, total_amount_yoy, dod].join('  ') + '\n';
        fs.appendFileSync(filepath, data, 'utf-8');
    }
};



// 6th　作成　今まで用いてきたロジック
function make6th(li_today, li_yesterday, li_lastyear, filepath) {

    // left side of 'Item 6th genre' sheet.
    let to_genre_id;
    let to_genre_name1;
    let to_genre_name2;
    let to_genre_name3;
    let to_genre_name4;
    let to_genre_name5;
    let to_genre_name6;
    let gms; // 前日GMS
    let ratio_7day; // 7日移動平均乖離率
    let ave_7day; // 7日移動平均
    let total_amount; // 累計
    let total_amount_yoy; // 累計YoY
    let dod; // DoD
    let la_total_amount; // 去年累計

    // right side of 'Item 6th genre' sheet.
    let ye_genre_id;
    let ye_gms; // 前々日GMS

    //
    let data = ['to_genre_id', 'to_genre_name1', 'to_genre_name2', 'to_genre_name3', 'to_genre_name4', 'to_genre_name5', 'to_genre_name6', 'ye_genre_id', 'ye_gms', 'la_total_amount', 'gms', 'ave_7day', 'total_amount', 'ratio_7day', 'total_amount_yoy', 'dod'].join('	') + '\n';
    fs.appendFileSync(filepath, data, 'utf-8');

    // write each files
    for (let i = 0; i < li_yesterday.length; i++) {

        if (!li_yesterday[i][4]) return;

        to_genre_id = li_yesterday[i][4];
        to_genre_name1 = li_yesterday[i][5];
        to_genre_name2 = li_yesterday[i][6];
        to_genre_name3 = li_yesterday[i][7];
        to_genre_name4 = li_yesterday[i][8];
        to_genre_name5 = li_yesterday[i][9];
        to_genre_name6 = li_yesterday[i][10];

        let row_today = getRows(to_genre_id, li_today);
        let row_lastyear = getRows(to_genre_id, li_lastyear);
        ye_genre_id = li_yesterday[i][4] ? li_yesterday[i][4] : '-';
        ye_gms = li_yesterday[i][11] ? li_yesterday[i][11] : '-';
        la_total_amount = row_lastyear[13] ? row_lastyear[13] : '-';

        //
        gms = row_today[11] ? row_today[11] : '-';
        ave_7day = (row_today[13] && row_today[15] && row_today[15] != 0) ? row_today[13] / row_today[15] : '-';
        total_amount = (row_today[13]) ? row_today[13] : '-';
        ratio_7day = (gms && ave_7day && ave_7day != 0 && gms !== '-' && ave_7day !== '-') ? (gms - ave_7day) / ave_7day : '-';
        total_amount_yoy = (total_amount && row_lastyear[13] && row_lastyear[13] != 0 && total_amount !== '-') ? (total_amount / row_lastyear[13]) - 1 : '-';
        dod = (gms && ye_gms && gms !== '-' && ye_gms !== '-') ? (gms - ye_gms) : '-';

        let data = [to_genre_id, to_genre_name1, to_genre_name2, to_genre_name3, to_genre_name4, to_genre_name5, to_genre_name6, ye_genre_id, ye_gms, la_total_amount, gms, ave_7day, total_amount, ratio_7day, total_amount_yoy, dod].join('	') + '\n';
        fs.appendFileSync(filepath, data, 'utf-8');
    }
};





// 3th　作成　今まで用いてきたロジック
function make3th_2(li_today, li_yesterday, li_lastyear, filepath_3th) {

    // left side of 'Item 6th genre' sheet.
    let to_genre_names;
    let to_genre_name1;
    let to_genre_name2;
    let to_genre_name3;
    let to_genre_name4;
    let to_genre_name5;
    let to_genre_name6;
    let gms; // 前日GMS
    let ratio_7day; // 7日移動平均乖離率
    let ave_7day; // 7日移動平均
    let total_amount; // 累計
    let total_amount_yoy; // 累計YoY
    let dod; // DoD
    let la_total_amount; // 去年累計
    let share; // share

    // right side of 'Item 6th genre' sheet.
    let ye_genre_names;
    let ye_gms; // 前々日GMS

    //
    // let data = ['to_genre_names','to_genre_name1','to_genre_name2','to_genre_name3','gms','ratio_7day','ave_7day','total_amount','total_amount_yoy','share','dod','la_total_amount'].join('	') + '\n';
    // fs.appendFileSync(filepath, data, 'utf-8');

    // データを3thへ整形　2次元配列として取得したli_today、li_yesterday、li_lastyearに対して、ジャンル3階層分のジャンル名（CD・DVD・楽器<>レコード<>ジャズ）の購入額、購入数の総和を取得する
    let [rows_today, total_amount_all_today] = getRows3th(li_today);    //total_amount_all_todayはExcelのSub total欄（一番上）を計算するのに使う
    let [rows_yesterday, total_amount_all_yesterday] = getRows3th(li_yesterday);
    let [rows_lastyear, total_amount_all_lastyear] = getRows3th(li_lastyear);

    // new_row[0] = _rows[i][0]; // genre_name_3th
    // new_row[1] = _rows[i][1]; // date
    // new_row[2] = _rows[i][2]; // year
    // new_row[3] = _rows[i][3]; // month
    // new_row[4] = _rows[i][4]; // day
    // new_row[5] = _rows[i][5]; // genre_name1
    // new_row[6] = _rows[i][6]; // genre_name2
    // new_row[7] = _rows[i][7]; // genre_name3
    // new_row[8] = parseInt(new_row[8], 10) + parseInt(_rows[i][8], 10); // sum_today
    // new_row[9] = parseInt(new_row[9], 10) + parseInt(_rows[i][9], 10); // num_today
    // new_row[10] = parseInt(new_row[10], 10) + parseInt(_rows[i][10], 10); // sum_7day
    // new_row[11] = parseInt(new_row[11], 10) + parseInt(_rows[i][11], 10); // num_7day
    // new_row[12] = _rows[i][12]; // total_day

    // [ '日用品雑貨・文房具・手芸<>日用品・生活雑貨<>虫除け・殺虫剤',
    // '20181106',
    // '2018',
    // '11',
    // '06',
    // '日用品雑貨・文房具・手芸',
    // '日用品・生活雑貨',
    // '虫除け・殺虫剤',
    // 2811422,
    // 1820,
    // 17274749,
    // 12916,
    // '7' ],



    let arr_3th = [];
    // write each files
    for (let i = 0; i < rows_today.length; i++) {

        if (i > 3) {
            // continue;
        }
        if (!rows_today[i][0]) return;
        // console.log(rows_today[i])

        to_genre_names = rows_today[i][0];
        to_genre_name1 = rows_today[i][5];
        to_genre_name2 = rows_today[i][6];
        to_genre_name3 = rows_today[i][7];
        // to_genre_name4 = rows_today[i][8];
        // to_genre_name5 = rows_today[i][9];
        // to_genre_name6 = rows_today[i][10];

        let row_yesterday = getRowTarget(to_genre_names, rows_yesterday);   //昨日のデータから今日のと同じジャンル名を持ってくる
        let row_lastyear = getRowTarget(to_genre_names, rows_lastyear);   //去年のデータから今日のと同じジャンル名を持ってくる

        // console.log(row_yesterday);
        // console.log(row_lastyear);
        // console.log('---');


        ye_genre_names = row_yesterday[0] ? row_yesterday[0] : '-';
        ye_gms = row_yesterday[8] ? row_yesterday[8] : 0;
        la_total_amount = row_lastyear[10] ? row_lastyear[10] : 0;

        //
        gms = rows_today[i][8] ? rows_today[i][8] : 0;
        ave_7day = (rows_today[i][10] && rows_today[i][12] && rows_today[i][12] != 0) ? rows_today[i][10] / rows_today[i][12] : 0;
        total_amount = (rows_today[i][10]) ? rows_today[i][10] : 0;
        ratio_7day = (gms && ave_7day && ave_7day != 0 && gms !== 0 && ave_7day !== 0) ? (gms - ave_7day) / ave_7day : 0;
        total_amount_yoy = (total_amount && row_lastyear[10] && row_lastyear[10] != 0 && total_amount !== 0) ? (total_amount / row_lastyear[10]) - 1 : 0;
        dod = (gms && ye_gms && gms !== 0 && ye_gms !== 0) ? (gms - ye_gms) : 0;
        share = total_amount / total_amount_all_today;

        let data = [to_genre_names, to_genre_name1, to_genre_name2, to_genre_name3, gms, ratio_7day, ave_7day, total_amount, total_amount_yoy, share, dod, la_total_amount];
        arr_3th.push(data);
    }

    // sort at ratio_7day
    arr_3th.sort(
        function (a, b) {
            var aRatio_7day = a[5];
            var bRatio_7day = b[5];
            if (aRatio_7day < bRatio_7day) return 1;
            if (aRatio_7day > bRatio_7day) return -1;
            return 0;
        }
    );

    // write file
    let gms_sum = 0;
    let ave_7day_sum = 0;
    let total_amount_sum = 0;
    let share_sum = 0;
    let dod_sum = 0;
    let total_amount_all_sum = 0;
    let data = ['genre_3th', 'genre_name1', 'genre_name2', 'genre_name3', 'gms', 'ratio_7day', 'ave_7day', 'total_amount', 'total_amount_yoy', 'share', 'dod', 'la_total_amount'].join('	') + '\n';
    fs.appendFileSync(filepath_3th, data, 'utf-8');
    for (let i = 0; i < arr_3th.length; i++) {

        if (!arr_3th[i]) {
            continue;
        }
        // arr_3th[i][5] = Math.round(arr_3th[i][5] * 100) + '%';
        // arr_3th[i][8] = Math.round(arr_3th[i][8] * 100) + '%';
        // arr_3th[i][9] = Math.round(arr_3th[i][9] * 10000) / 100;
        // arr_3th[i][9] = arr_3th[i][9].toFixed(2) + '%';
        //
        //全ジャンルの計算を行う
        gms_sum += (typeof (arr_3th[i][4]) === 'number' && !isNaN(arr_3th[i][4])) ? arr_3th[i][4] : 0;
        ave_7day_sum += (typeof (arr_3th[i][6]) === 'number' && !isNaN(arr_3th[i][6])) ? arr_3th[i][6] : 0;
        total_amount_sum += (typeof (arr_3th[i][7]) === 'number' && !isNaN(arr_3th[i][7])) ? arr_3th[i][7] : 0;
        share_sum += (typeof (arr_3th[i][9]) === 'number' && !isNaN(arr_3th[i][9])) ? arr_3th[i][9] : 0;
        dod_sum += (typeof (arr_3th[i][10]) === 'number' && !isNaN(arr_3th[i][10])) ? arr_3th[i][10] : 0;
        total_amount_all_sum += (typeof (arr_3th[i][11]) === 'number' && !isNaN(arr_3th[i][11])) ? arr_3th[i][11] : 0;

        data = arr_3th[i].join('	') + '\n';
        fs.appendFileSync(filepath_3th, data, 'utf-8');
    }

    // 3th data
    setExcel(arr_3th, gms_sum, ave_7day_sum, total_amount_sum, share_sum, dod_sum, total_amount_all_sum);

};








//
function make3th(filepath_6th, filepath_3th) {

    let li_6th = list(filepath_6th);
    let obj_3th = {};
    let arr_3th = [];
    let total_amount_all = 0;

    // 6th を3thへ変換
    for (var i = 1; i < li_6th.length; i++) {

        if (!li_6th[i][4]) continue;

        if (!obj_3th[li_6th[i][1] + '<>' + li_6th[i][2] + '<>' + li_6th[i][3]]) {
            obj_3th[li_6th[i][1] + '<>' + li_6th[i][2] + '<>' + li_6th[i][3]] = [];
        }

        obj_3th[li_6th[i][1] + '<>' + li_6th[i][2] + '<>' + li_6th[i][3]].push({
            genre_3th: li_6th[i][1] + '<>' + li_6th[i][2] + '<>' + li_6th[i][3],
            genre_name1: li_6th[i][1],
            genre_name2: li_6th[i][2],
            genre_name3: li_6th[i][3],
            ye_gms: li_6th[i][8],
            la_total_amount: li_6th[i][9],
            gms: li_6th[i][10],
            ave_7day: li_6th[i][11],
            total_amount: li_6th[i][12],
            ratio_7day: li_6th[i][13],
            total_amount_yoy: li_6th[i][14],
            dod: li_6th[i][15]
        });

        // 累計合計
        total_amount_all += (li_6th[i][12] !== '-') ? parseInt(li_6th[i][12], 10) : 0;
    }

    li_6th = []; // init

    // 3thの作成
    for (var key in obj_3th) {
        if (obj_3th.hasOwnProperty(key)) {

            let genre_3th = '';
            let genre_name1 = '';
            let genre_name2 = '';
            let genre_name3 = '';
            // let ye_gms = 0;
            let la_total_amount = 0;
            let gms = 0;
            let ave_7day = 0;
            let total_amount = 0;
            let ratio_7day = 0;
            let total_amount_yoy = 0;
            let dod = 0;
            let share = 0;

            // obj_3th[key]
            for (let i = 0; i < obj_3th[key].length; i++) {

                genre_3th = obj_3th[key][i].genre_3th;
                genre_name1 = obj_3th[key][i].genre_name1;
                genre_name2 = obj_3th[key][i].genre_name2;
                genre_name3 = obj_3th[key][i].genre_name3;
                gms += ((obj_3th[key][i].gms !== '-') ? parseInt(obj_3th[key][i].gms, 10) : 0);
                ave_7day += ((obj_3th[key][i].ave_7day !== '-') ? parseInt(obj_3th[key][i].ave_7day, 10) : 0);
                total_amount += ((obj_3th[key][i].total_amount !== '-') ? parseInt(obj_3th[key][i].total_amount, 10) : 0);
                dod += ((obj_3th[key][i].dod !== '-') ? parseInt(obj_3th[key][i].dod, 10) : 0);
                la_total_amount += ((obj_3th[key][i].la_total_amount !== '-') ? parseInt(obj_3th[key][i].la_total_amount, 10) : 0);
            }

            ratio_7day = (ave_7day !== 0) ? ((gms - ave_7day) / ave_7day) : -99999999999;
            share = (total_amount / total_amount_all);
            total_amount_yoy = (la_total_amount !== 0) ? (total_amount / la_total_amount - 1) : 0;

            arr_3th.push([genre_3th, genre_name1, genre_name2, genre_name3, gms, ratio_7day, ave_7day, total_amount, total_amount_yoy, share, dod, la_total_amount]);
        }
    }

    obj_3th = {}; // init

    // sort at ratio_7day
    arr_3th.sort(
        function (a, b) {
            var aRatio_7day = a[5];
            var bRatio_7day = b[5];
            if (aRatio_7day < bRatio_7day) return 1;
            if (aRatio_7day > bRatio_7day) return -1;
            return 0;
        }
    );

    // write file
    let gms_sum = 0;
    let ave_7day_sum = 0;
    let total_amount_sum = 0;
    let share_sum = 0;
    let dod_sum = 0;
    let total_amount_all_sum = 0;
    let data = ['genre_3th', 'genre_name1', 'genre_name2', 'genre_name3', 'gms', 'ratio_7day', 'ave_7day', 'total_amount', 'total_amount_yoy', 'share', 'dod', 'la_total_amount'].join('	') + '\n';
    fs.appendFileSync(filepath_3th, data, 'utf-8');
    for (let i = 0; i < arr_3th.length; i++) {
        // arr_3th[i][5] = Math.round(arr_3th[i][5] * 100) + '%';
        // arr_3th[i][8] = Math.round(arr_3th[i][8] * 100) + '%';
        // arr_3th[i][9] = Math.round(arr_3th[i][9] * 10000) / 100;
        // arr_3th[i][9] = arr_3th[i][9].toFixed(2) + '%';
        //
        gms_sum += (typeof (arr_3th[i][4]) === 'number' && !isNaN(arr_3th[i][4])) ? arr_3th[i][4] : 0;
        ave_7day_sum += (typeof (arr_3th[i][6]) === 'number' && !isNaN(arr_3th[i][6])) ? arr_3th[i][6] : 0;
        total_amount_sum += (typeof (arr_3th[i][7]) === 'number' && !isNaN(arr_3th[i][7])) ? arr_3th[i][7] : 0;
        share_sum += (typeof (arr_3th[i][9]) === 'number' && !isNaN(arr_3th[i][9])) ? arr_3th[i][9] : 0;
        dod_sum += (typeof (arr_3th[i][10]) === 'number' && !isNaN(arr_3th[i][10])) ? arr_3th[i][10] : 0;
        total_amount_all_sum += (typeof (arr_3th[i][11]) === 'number' && !isNaN(arr_3th[i][11])) ? arr_3th[i][11] : 0;

        data = arr_3th[i].join('	') + '\n';
        fs.appendFileSync(filepath_3th, data, 'utf-8');
    }

    // 3th data
    setExcel(arr_3th, gms_sum, ave_7day_sum, total_amount_sum, share_sum, dod_sum, total_amount_all_sum);
};





//
function setExcel(arr_3th, gms_sum, ave_7day_sum, total_amount_sum, share_sum, dod_sum, total_amount_all_sum) {

    var workbook = new excel.Workbook();

    let filename = `D:/work/tool/tools/bteq/yoy/yyyymmdd提出【ツール】Item別YoY.xlsx`;
    let ymd = moment().format('YYYYMMDD');

    workbook.xlsx.readFile(filename)
        .then(function () {

            var worksheet = workbook.getWorksheet('ジャンル3rd');

            worksheet.getCell(`E2`).value = gms_sum;
            worksheet.getCell(`F2`).value = (gms_sum / ave_7day_sum) - 1;
            worksheet.getCell(`G2`).value = ave_7day_sum;
            worksheet.getCell(`H2`).value = total_amount_sum;
            worksheet.getCell(`I2`).value = (total_amount_sum / total_amount_all_sum - 1);
            worksheet.getCell(`J2`).value = share_sum;
            worksheet.getCell(`K2`).value = dod_sum;
            worksheet.getCell(`L2`).value = total_amount_all_sum;

            worksheet.getCell(`G3`).value = worksheet.getCell(`G3`).value.richText[0].text + worksheet.getCell(`G3`).value.richText[1].text;
            worksheet.getCell(`H3`).value = worksheet.getCell(`H3`).value.richText[0].text + worksheet.getCell(`H3`).value.richText[1].text;
            worksheet.getCell(`I3`).value = worksheet.getCell(`I3`).value.richText[0].text + worksheet.getCell(`I3`).value.richText[1].text;
            worksheet.getCell(`J3`).value = worksheet.getCell(`J3`).value.richText[0].text + worksheet.getCell(`J3`).value.richText[1].text;
            worksheet.getCell(`K3`).value = worksheet.getCell(`K3`).value.richText[0].text + worksheet.getCell(`K3`).value.richText[1].text;
            worksheet.getCell(`L3`).value = worksheet.getCell(`L3`).value.richText[0].text + worksheet.getCell(`L3`).value.richText[1].text;

            for (let i = 0; i < arr_3th.length; i++) {
                worksheet.getCell(`A${i + 4}`).value = arr_3th[i][0];
                worksheet.getCell(`B${i + 4}`).value = arr_3th[i][1];
                worksheet.getCell(`C${i + 4}`).value = arr_3th[i][2];
                worksheet.getCell(`D${i + 4}`).value = arr_3th[i][3];
                worksheet.getCell(`E${i + 4}`).value = arr_3th[i][4];
                worksheet.getCell(`F${i + 4}`).value = arr_3th[i][5];
                worksheet.getCell(`G${i + 4}`).value = arr_3th[i][6];
                worksheet.getCell(`H${i + 4}`).value = arr_3th[i][7];
                worksheet.getCell(`I${i + 4}`).value = arr_3th[i][8];
                worksheet.getCell(`J${i + 4}`).value = arr_3th[i][9];
                worksheet.getCell(`K${i + 4}`).value = arr_3th[i][10];
                worksheet.getCell(`L${i + 4}`).value = arr_3th[i][11];

                // if (arr_3th[i][5] >= 0.5) {
                //     console.log(1212, arr_3th[i][5]);
                //     worksheet.getCell(`F${i + 4}`).fill = {
                //         type: 'pattern',
                //         pattern:'lightUp',
                //         bgColor:{argb:'ff99ff00'}
                //     };
                // } else if (arr_3th[i][5] >= 0.3) {
                //     worksheet.getCell(`F${i + 4}`).fill = {
                //         type: 'pattern',
                //         pattern:'lightUp',
                //         bgColor:{argb:'ffffff99'}
                //     };
                // } else if (arr_3th[i][5] >= 0) {
                //     worksheet.getCell(`F${i + 4}`).fill = {
                //         type: 'pattern',
                //         pattern:'lightUp',
                //         bgColor:{argb:'ffFFB2FF'}
                //     };
                // }
            }

            // filename = `D:/work/tool/tools/bteq/yoy/${ymd}提出【ツール】Item別YoY.xlsx`;
            workbook.xlsx.writeFile(filepath2)
                .then(function () {
                    console.log('finish writing');
                });
        });
};




// メール送信
function mailto() {

    return new Promise((resolve) => {

        co(function* () {

            let title = '本日の7日移動平均報告です。';
            let from = 'ts-rie.naruo@rakuten.com';
            let to = 'jun.shimizu@rakuten.com';
            if (cli['test']) {
                to = 'ts-rie.naruo@rakuten.com';
            }
            let cc = ['ts-rie.naruo@rakuten.com']
            let text = `
清水さん

本日の7日移動平均報告です。
ご確認よろしくお願いします。
https://rak.app.box.com/files/0/f/15207840335/fashion_kuriyama
https://rak.app.box.com/files/0/f/16817847354/fashion_kuriyama2
https://rak.app.box.com/files/0/f/24494815462/fashion_kuriyama3
${fileName}

栗山
`;

            // mail to each person
            yield util.mailto(title, from, to, cc, text, '');
            resolve();
        });
    });
};










co(function* () {
    
    // yoyフォルダの生成
    yield util.mkDir(dirPath_bteq);

    // 不要なファイルの削除
    util.remove(filepath_yoy_3th);
    util.remove(filepath_yoy_6th);
    util.remove(filepath_yoy_lastyear1);
    util.remove(filepath_yoy_lastyear2);
    util.remove(filepath_yoy_today1);
    util.remove(filepath_yoy_today2);
    util.remove(filepath_yoy_yesterday1);
    util.remove(filepath_yoy_yesterday2);

    // YOYの取得
    getYOY1();                       //タブ区切りデータのtextをダウンロードしている
    getYOY2();
    getYOY3();

    let filepath_export = `D:/work/tool/tools/bteq/yoy/yoy_today.tsv`;
    let li_today = list(filepath_export);   //タブ区切りデータを2次元配列にする（前日）

    filepath_export = `D:/work/tool/tools/bteq/yoy/yoy_yesterday.tsv`;
    let li_yesterday = list(filepath_export);//タブ区切りデータを2次元配列にする（2日前）

    filepath_export = `D:/work/tool/tools/bteq/yoy/yoy_lastyear.tsv`;
    let li_lastyear = list(filepath_export);//タブ区切りデータを2次元配列にする（前年

    let filepath_6th = `D:/work/tool/tools/bteq/yoy/yoy_6th.tsv`;
    let filepath_3th = `D:/work/tool/tools/bteq/yoy/yoy_3th.tsv`;
    // make6th(li_today, li_yesterday, li_lastyear, filepath_6th);
    // make3th(filepath_6th, filepath_3th);

    make3th_2(li_today, li_yesterday, li_lastyear, filepath_3th);   //Excelファイルに書き出す


    // yield mailto();
});

