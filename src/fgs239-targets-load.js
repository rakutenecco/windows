'use strict';

const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const shell = require('shelljs');
const co = require('co');
let today_slash = moment().format('YYYY/MM/DD');
let today = moment().format('YYYYMMDD');

// parse args
const cli = commandArgs([
    { name: 'runtype', alias: 'r', type: Number },
    { name: 'targetfname', alias: 'f', type: String },
]);

const share_path = 'D:/share/targets/'; 
const box_path = `D:\/Box Sync\/EC Consulting Department\/ecc_fashion\/FGS239_insta\/targets\/`;
const fl_path = `D:\/work\/tool\/tools\/bteq\/FGS239_insta\/targets\/`;
const fls_path = 'D:/work/tool/tools/bteq/FGS239_insta/targets/';

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

const tbls = [
    { ctgry: 'food',     tgts: 'food_FGS239_targets', ista: 'food_FGS239_insta_posts', twtr: 'food_FGS258_twitter_posts', yaho: 'food_FGS286_yahoo' },
    { ctgry: 'fashion',  tgts: 'fashion_FGS239_targets_fashion', ista: 'fashion_FGS239_insta_posts_fashion', twtr: 'fashion_FGS258_twitter_posts_fashion', yaho: 'fashion_FGS286_yahoo_fashion' },
    { ctgry: 'watch',    tgts: 'fashion_FGS239_targets_watch', ista: 'fashion_FGS239_insta_posts_watch', twtr: 'fashion_FGS258_twitter_posts_watch', yaho: 'fashion_FGS286_yahoo_watch' },
    { ctgry: 'flower',   tgts: 'flower_FGS239_targets', ista: 'flower_FGS239_insta_posts', twtr: 'flower_FGS258_twitter_posts', yaho: 'flower_FGS286_yahoo' },
    { ctgry: 'interior', tgts: 'interior_FGS239_targets', ista: 'interior_FGS239_insta_posts', twtr: 'interior_FGS258_twitter_posts', yaho: 'interior_FGS286_yahoo' },
    { ctgry: 'kitchen',  tgts: 'kitchen_FGS239_targets', ista: 'kitchen_FGS239_insta_posts', twtr: 'kitchen_FGS258_twitter_posts', yaho: 'kitchen_FGS286_yahoo' },
    { ctgry: 'life',     tgts: 'life_FGS239_targets', ista: 'life_FGS239_insta_posts', twtr: 'life_FGS258_twitter_posts', yaho: 'life_FGS286_yahoo' },
    { ctgry: 'cosme',    tgts: 'cosme_FGS239_targets', ista: 'cosme_FGS239_insta_posts', twtr: 'cosme_FGS258_twitter_posts', yaho: 'cosme_FGS286_yahoo' },
    { ctgry: 'wine',     tgts: 'wine_FGS239_targets', ista: 'wine_FGS239_insta_posts', twtr: 'wine_FGS258_twitter_posts', yaho: 'wine_FGS286_yahoo' },
    { ctgry: 'official', tgts: 'fashion_FGS239_targets_official', ista: '', twtr: '', yaho: '' },
    { ctgry: 'area',     tgts: 'area_FGS239_targets', ista: '', twtr: '', yaho: '' },
];

let runtype = cli['runtype'] ? cli['runtype'] : 0;
let adddate = today;
let adddate_slash = today_slash;

let ctgry;
let tgts;
let ista;
let twtr;
let yaho;
let addfile;
let copyfile;

let file_ctgry = '';
let file_add;
let file_copy;

if ( cli['targetfname'] ){
    let fnm = cli['targetfname'];
    //targets_food_addspdb_20190319.txt
    //targets_food_copy_20190319.txt
    if( fnm.search(/^targets_[a-z]{4,8}_addspdb_[0-9]{8}\.txt$/) == 0 ){
        runtype = 1;
        file_add = fnm;

    }else if( fnm.search(/^targets_[a-z]{4,8}_copy_[0-9]{8}\.txt$/) == 0 ){
        runtype = 2;
        file_copy = fnm;

    }else{
        console.log(` file format false : ${fnm}`);
        runtype = 9;
    }

    if(runtype != 9){
        let tmpfnm = fnm.split(/[_\.]/g);
        file_ctgry = tmpfnm[1]; // food,interior,...
        adddate = tmpfnm[3]; // yyyymmdd
        adddate_slash = tmpfnm[3].substr(0, 4) + '-' + tmpfnm[3].substr(4, 2) + '-' + tmpfnm[3].substr(6, 2); // yyyy/mm/dd
    }
    
}

// load_check
function load_filecheck(fname) {

    return new Promise((resolve) => {
        co(function*() {
           
            let result = false;
            let fname0 = `${share_path}${fname}`;
            let fname1 = `${box_path}${fname}`;
            let fname2 = `${fl_path}${fname}`;
            //console.log(`fname1 : ${fname1}`);
            
            // share files            
            if (!fs.existsSync(fname0)) {
                console.log(` share : not exists ${fname0}`);

            }else{
                // box files  
                if (fs.existsSync(fname1)) {
                    console.log(`fastload : ${fname1} done`);
                }else{
                    if (fs.existsSync(fname2)) {
                        console.log(`fastload : ${fname2} done`);
                    } else {
                        result = true;
                    }
                }
            }

            resolve(result);
        });
    });
};

// load_count
function load_count(fnm, tnm) {

    return new Promise((resolve) => {
        co(function*() {
           
            let cnt;
            let btqfile = `${fl_path}loadcount_${fnm}`;
            let btqlog = `${fl_path}loadcount_${fnm}.log`;
            let cntfile = `${fl_path}loadcount_${fnm}_result.txt`;
            shell.rm(cntfile);

            // select count
            let bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${cntfile}
.RECORDMODE OFF
DATABASE SBX_ICB_ICBB;
SELECT
    Trim(x.cnt)
FROM 
  ( SELECT count(*) cnt 
      FROM ${tnm}_pre
  ) x;
  
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(btqfile, bteq);
            console.log('   load count 実行中');
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);

            let data = fs.readFileSync(cntfile, 'utf8');
            cnt = data.match(/\d{1,}/);

            console.log(`    fastload : ${fnm} cnt:${cnt}件`);
            resolve(cnt);
        });
    });
};

// load
function load_add() {

    return new Promise((resolve) => {
        co(function*() {

            let result = false;
            
            shell.cp(`${share_path}${addfile}`, fl_path);
                    
            let flstr = `
SET SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
    DATABASE SBX_ICB_ICBB;

    DROP TABLE ${tgts}_pre;
    DROP TABLE ${tgts}_pre_et;
    DROP TABLE ${tgts}_pre_uv;

    CREATE TABLE ${tgts}_pre
    (
        keyword varchar(500)
    )PRIMARY INDEX ( keyword );

    BEGIN LOADING ${tgts}_pre ERRORFILES ${tgts}_pre_et, ${tgts}_pre_uv;
    set record vartext "	";
    DEFINE 
        keyword (VARCHAR(500))
    FILE = ${fls_path}${addfile};

    INSERT INTO ${tgts}_pre VALUES(
        :keyword
    );
    END LOADING;

.EXIT
`;
            fs.writeFileSync(`${fl_path}load_${addfile}`, flstr);
            // Fastload
            console.log(`  addfile fastload 実行中  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
            shell.exec(`fastload < ${fl_path}load_${addfile} > ${fl_path}load_${addfile}.log`);

            let lct = yield load_count(addfile, tgts);
            if(lct > 0) {
                result = true;
            } else {
                console.log('  fastload failed');
            }
            resolve(result);
        });
    });
};

// insert_add
function insert_add() {

    return new Promise((resolve) => {
        co(function*() {
            
            let result = false;
                
            // insert
            let iststr = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;
    DATABASE SBX_ICB_ICBB;

    INSERT INTO SBX_ICB_ICBB.${tgts}

    SELECT distinct(a.keyword), null, '${adddate_slash}'
    FROM ${tgts}_pre a
    WHERE NOT EXISTS 
      (SELECT * FROM ${tgts} b 
        WHERE a.keyword = b.keyword );

    
    INSERT INTO SBX_ICB_ICBB.${tgts}_test2 (keyword, adddate)

    SELECT distinct(a.keyword), '${adddate_slash}'
    FROM ${tgts}_pre a
    WHERE NOT EXISTS 
        (SELECT * FROM ${tgts}_test2 b 
        WHERE a.keyword = b.keyword );


.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(`${fl_path}insert_${addfile}`, iststr);
            console.log('   insert 実行中');
            shell.exec(`bteq < ${fl_path}insert_${addfile} > ${fl_path}insert_${addfile}.log`);

            shell.cp(`${fls_path}${addfile}`, box_path);

            resolve();
        });
    });
};

// targets_load
const targets_load = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < tbls.length; i++) {

                ctgry = tbls[i].ctgry;
                tgts = tbls[i].tgts;
                addfile = '';

                if(file_ctgry.length == 0 || file_ctgry == ctgry){

                    addfile = file_add ? file_add : `targets_${ctgry}_addspdb_${adddate}.txt`;
                    //console.log('  addfile:',addfile);

                    let rt = yield load_filecheck(addfile);
                    if(rt) {
                        let rt2 = yield load_add();
                        if (rt2) yield insert_add();
                    }
                }
            }
    
            resolve();
        });
    });
}


// load
function load_copy() {

    return new Promise((resolve) => {
        co(function*() {

            let result = false;
            
            shell.cp(`${share_path}${copyfile}`, fl_path);
                    
            let flstr = `
SET SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
    DATABASE SBX_ICB_ICBB;

    DROP TABLE ${tgts}_pre;
    DROP TABLE ${tgts}_pre_et;
    DROP TABLE ${tgts}_pre_uv;

    CREATE TABLE ${tgts}_pre
    (
        keyword varchar(500)
    )PRIMARY INDEX ( keyword );

    BEGIN LOADING ${tgts}_pre ERRORFILES ${tgts}_pre_et, ${tgts}_pre_uv;
    set record vartext "	";
    DEFINE 
        keyword (VARCHAR(500))
    FILE = ${fls_path}${copyfile};

    INSERT INTO ${tgts}_pre VALUES(
        :keyword
    );
    END LOADING;

.EXIT
`;
            fs.writeFileSync(`${fl_path}load_${copyfile}`, flstr);
            // Fastload
            console.log(`  copyfile fastload 実行中  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
            shell.exec(`fastload < ${fl_path}load_${copyfile} > ${fl_path}load_${copyfile}.log`);

            let lct = yield load_count(copyfile, tgts);
            if(lct > 0) {
                result = true;
            } else {
                console.log('  fastload failed');
            }
            resolve(result);
        });
    });
};

// insta_copy
function insta_copy() {

    return new Promise((resolve) => {
        co(function*() {
            
            let result = false;
            let sincedate = moment().add(-41, 'd').format('YYYY/MM/DD');
                
            // insert
            let iststr = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;
    DATABASE SBX_ICB_ICBB;

    INSERT INTO SBX_ICB_ICBB.${ista}

    SELECT keyword, Max(category), getdate, Min(postdate), Max(posts)
      FROM
        (
        SELECT keyword, category, getdate, postdate, posts
          FROM food_FGS239_insta_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND getdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM fashion_FGS239_insta_posts_fashion
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND getdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM fashion_FGS239_insta_posts_watch
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND getdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM life_FGS239_insta_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND getdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM flower_FGS239_insta_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND getdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM interior_FGS239_insta_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND getdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM kitchen_FGS239_insta_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND getdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM cosme_FGS239_insta_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND getdate >= '${sincedate}'
        ) a
        
    GROUP BY keyword, getdate;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(`${fl_path}${copyfile}_insta.txt`, iststr);
            console.log('  insta   copy 実行中');
            shell.exec(`bteq < ${fl_path}${copyfile}_insta.txt > ${fl_path}${copyfile}_insta.log`);

            result = true;
            resolve();
        });
    });
};

// twitter_copy
function twitter_copy() {

    return new Promise((resolve) => {
        co(function*() {
            
            let result = false;
            let sincedate = moment().add(-36, 'd').format('YYYY/MM/DD');
                
            // insert
            let iststr = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;
    DATABASE SBX_ICB_ICBB;

    INSERT INTO SBX_ICB_ICBB.${twtr}

    SELECT keyword, Max(category), Min(getdate), postdate, Max(posts)
      FROM
        (
        SELECT keyword, category, getdate, postdate, posts
          FROM food_FGS258_twitter_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM fashion_FGS258_twitter_posts_fashion
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM fashion_FGS258_twitter_posts_watch
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM life_FGS258_twitter_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM flower_FGS258_twitter_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM interior_FGS258_twitter_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM kitchen_FGS258_twitter_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, posts
          FROM cosme_FGS258_twitter_posts
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        ) a
        
    GROUP BY keyword, postdate;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(`${fl_path}${copyfile}_twtr.txt`, iststr);
            console.log('  twitter copy 実行中');
            shell.exec(`bteq < ${fl_path}${copyfile}_twtr.txt > ${fl_path}${copyfile}_twtr.log`);

            result = true;
            resolve();
        });
    });
};

// yahoo_copy
function yahoo_copy() {

    return new Promise((resolve) => {
        co(function*() {
            
            let result = false;
            let sincedate = moment().add(-36, 'd').format('YYYY/MM/DD');
                
            // insert
            let iststr = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};
.set width 20000;
    DATABASE SBX_ICB_ICBB;

    INSERT INTO SBX_ICB_ICBB.${yaho}

    SELECT keyword, Max(category), Min(getdate), postdate,  Max(tweets), Max(badfeel), Max(goodfeel)
      FROM
        (
        SELECT keyword, category, getdate, postdate, tweets, badfeel, goodfeel
          FROM food_FGS286_yahoo
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, tweets, badfeel, goodfeel
          FROM fashion_FGS286_yahoo_fashion
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, tweets, badfeel, goodfeel
          FROM fashion_FGS286_yahoo_watch
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, tweets, badfeel, goodfeel
          FROM life_FGS286_yahoo
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, tweets, badfeel, goodfeel
          FROM flower_FGS286_yahoo
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, tweets, badfeel, goodfeel
          FROM interior_FGS286_yahoo
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, tweets, badfeel, goodfeel
          FROM kitchen_FGS286_yahoo
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        UNION ALL
        SELECT keyword, category, getdate, postdate, tweets, badfeel, goodfeel
          FROM cosme_FGS286_yahoo
         WHERE keyword IN ( SELECT keyword FROM ${tgts}_pre )
           AND postdate >= '${sincedate}'
        ) a
        
    GROUP BY keyword, postdate;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            fs.writeFileSync(`${fl_path}${copyfile}_yahoo.txt`, iststr);
            console.log('  yahoo   copy 実行中');
            shell.exec(`bteq < ${fl_path}${copyfile}_yahoo.txt > ${fl_path}${copyfile}_yahoo.log`);

            result = true;
            resolve();
        });
    });
};

// data_copy
const data_copy = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < tbls.length; i++) {

                ctgry = tbls[i].ctgry;
                tgts = tbls[i].tgts;
                ista = tbls[i].ista;
                twtr = tbls[i].twtr;
                yaho = tbls[i].yaho;
                copyfile = '';

                if(file_ctgry.length == 0 || file_ctgry == ctgry){

                    copyfile = file_copy ? file_copy : `targets_${ctgry}_copy_${adddate}.txt`;
                    //console.log('  copyfile:',copyfile);

                    let rt = yield load_filecheck(copyfile);
                    if(rt) {
                        let rt2 = yield load_copy();
                        
                        if (rt2){
                            let rti = false;
                            if(ista.length > 0){
                                rti = yield insta_copy();
                            }else{
                                rti = true;
                            }
                            let rtt = false;
                            if(twtr.length > 0){
                                rtt = yield twitter_copy();
                            }else{
                                rtt = true;
                            }
                            let rty = false;
                            if(yaho.length > 0){
                                rty = yield yahoo_copy();
                            }else{
                                rty = true;
                            }
                        
                            if (rti && rtt && rty) {
                                shell.cp(`${fls_path}${copyfile}`, box_path);
                            }
                        }
                    }
                }
            }
    
            resolve();
        });
    });
}


// run
co(function*() {
    
    let st = moment().format('YYYY/MM/DD HH:mm:ss');
        
    if ( runtype == 0 || runtype == 1 ) {
        //targets_load
        yield targets_load();
    }
    if ( runtype == 0 || runtype == 2 ) {
        //data_copy
        yield data_copy();
    }
    console.log(`/// FGS239-targets-load end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

});
