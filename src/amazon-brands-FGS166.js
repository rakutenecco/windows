'use strict';

const moment = require('moment');
const commandArgs = require('command-line-args');
const fs = require('fs-extra');
const shell = require('shelljs');
const co = require('co');
const util = new (require('./util_co.js'));
let today = moment().format('YYYYMMDD');

// parse args
const cli = commandArgs([
   { name: 'time', alias: 't', type: String }
]);


// reset time
if (cli['time']) {
    today = cli['time'];
}


// init
function init() {

    return new Promise((resolve) => {
        co(function*() {

            shell.cp(`D:\/share\/amazon-brands-FGS166-${today}.txt`, `D:\/work\/tool\/tools\/input_fastload\/amazon-brands-FGS166.txt`);
            //shell.exec(`cp /d/share/amazon-brands-FGS166-${today}.txt /d/work/tool/tools/input_fastload/amazon-brands-FGS166.txt`);

            resolve();
        });
    });
};


// do Fastload
function doFastload() {

    return new Promise((resolve) => {
        co(function*() {

            // インスタグラムへ
            shell.exec('node src/makeFastload.js -p amazon_brands_FGS166_pre -d');

            resolve();
        });
    });
};


// do Insert
function doInsert() {

    return new Promise((resolve) => {
        co(function*() {

            // DROP table SBX_ICB_SP.fashion_tairyu_kikan
            let filepath1 = 'D:\/work\/tool\/tools\/bteq\/amazon-brands-FGS166.txt';
            let bteq1 = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        INSERT INTO SBX_ICB_ICBB.fashion_amazon_brands_FGS166

        SELECT
            *
        FROM SBX_ICB_ICBB.fashion_amazon_brands_FGS166_pre A
        where NOT EXISTS

        (SELECT * FROM SBX_ICB_ICBB.fashion_amazon_brands_FGS166 B WHERE A.reg_date = B.reg_date);

        drop table SBX_ICB_ICBB.fashion_amazon_brands_FGS166_pre;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

            fs.writeFileSync(filepath1, bteq1);
            shell.exec(`bteq < bteq/amazon-brands-FGS166.txt`);

            resolve();
        });
    });
};


// run
co(function*() {

    // 初期化
    yield init();

    // fastload pre
    yield doFastload();

    // insert to main table
    yield doInsert();

});











