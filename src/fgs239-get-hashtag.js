'use strict';

const shell = require('shelljs');
const fs = require('fs-extra');
const moment = require('moment');
const co = require('co');
const commandArgs = require('command-line-args');
let yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: String }, // 0 /1:insta/2:twitter
    { name: 'targetdate', alias: 't', type: String }, // yyyy-mm-dd regdate,yesterday 
]);

//let runtype = cli['run']? cli['run'] : 0;
let targetdate;
//let lastdate;
//let lastyear;
let ttmp;
if(cli['targetdate']){
    targetdate = cli['targetdate'];
    ttmp = targetdate.replace(/-/g,'');
    //ttmp = ttmp.substr(0, 4) + '-' + ttmp.substr(4, 2) + '-' + ttmp.substr(6, 2);
    //lastdate = moment(ttmp).subtract(1, 'days').format('YYYY/MM/DD');
    //lastyear = moment(ttmp).subtract(1, 'years').format('YYYY/MM/DD');
}else{
    targetdate = yesterday;
    ttmp = targetdate.replace(/-/g,'');
    //ttmp = ttmp.substr(0, 4) + '-' + ttmp.substr(4, 2) + '-' + ttmp.substr(6, 2);
    //lastdate = moment().subtract(2, 'days').format('YYYY/MM/DD');
    //lastyear = moment(ttmp).subtract(1, 'years').format('YYYY/MM/DD');
}

const hashtagfrd = 'hashtag';
let bteq_path = `D:\/work\/tool\/tools\/bteq\/FGS239_insta\/${hashtagfrd}\/`;
let box_path = `D:\/Box Sync\/EC Consulting Department\/ecc_fashion\/FGS239_insta\/${hashtagfrd}\/`;

const con = 'dm-red101';
const usr = 'ts-masako.ikeda';
const p = '1234qwerT';

let type;

let fname;
let btqfile;
let btqlog;

// output 新規keywordのレポートを生成
function output(filename) {

    return new Promise((resolve) => {
        co(function*() {

            fname = `${filename}_${ttmp}.csv`;
            btqfile = `${filename}.txt`;
            btqlog = `${filename}_log.txt`;
                
            yield setBteq();
            // daily output
            //  bteq 
            shell.exec(`bteq < ${bteq_path}${btqfile} > ${bteq_path}${btqlog}`);
        
            // bteq -> box
            shell.cp(`${bteq_path}${fname}`, box_path);                    

            console.log(`${fname} output end //`);
            resolve();
        });
    });
};


// load
function out_check(fname) {

    return new Promise((resolve) => {
        co(function*() {
           
              
            let result = false;
            let f1 = `${box_path}${fname}_${ttmp}.csv`;
            let f2 = `${bteq_path}${fname}_${ttmp}.csv`;

            // check files
            if (fs.existsSync(f1)) {
                result = true;
                console.log(f1 + ' done');
            }else{
                if (fs.existsSync(f2)) {
                    result = true;
                    console.log(f2 + ' done');
                }
            }

            resolve(result);
        });
    });
};


// output bteqファイルを生成
function setBteq() {
    return new Promise((resolve) => {
        co(function*() {
            let bteq;
            if(type == 1){
                bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${bteq_path}${fname}
.RECORDMODE OFF
SELECT '${targetdate}, count';

WITH new_kwd AS (
    SELECT OReplace(Trim(tag1),'　','') tag1
          ,Count(*) Cnt
      FROM SBX_ICB_ICBB.food_fgs242_instagram_explore
     WHERE reg_date = '${targetdate}' 
       AND explore_id IN ('アイス','グルメ','ケーキ','スイーツ','チョコレート')
       AND tag1 IS NOT NULL
       AND ( tag1 LIKE '%ケーキ%' OR tag1 LIKE '%チョコ%' )
     GROUP BY 1
    HAVING Cnt >= 4  
)
, old_kwd AS (
    SELECT DISTINCT(tag1) tag1
      FROM SBX_ICB_ICBB.food_fgs242_instagram_explore
     WHERE reg_date < '${targetdate}' 
       AND explore_id IN ('アイス','グルメ','ケーキ','スイーツ','チョコレート')
       AND tag1 IS NOT NULL
       AND ( tag1 LIKE '%ケーキ%' OR tag1 LIKE '%チョコ%' )
)
, f_kwd AS (
    SELECT DISTINCT('#'||keyword) keyword
      FROM SBX_ICB_ICBB.food_FGS239_insta_posts
)
SELECT
    Trim(x.tag1) || ',' ||
    Trim(x.Cnt)
  FROM
    ( SELECT n.tag1 tag1
            ,n.Cnt  Cnt
        FROM new_kwd n
       WHERE NOT EXISTS (SELECT * FROM old_kwd o WHERE o.tag1 = n.tag1)
         AND NOT EXISTS (SELECT * FROM f_kwd f WHERE f.keyword = n.tag1)
    ) x
 ORDER BY x.Cnt DESC, x.tag1;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            }else if(type == 2){

                bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${bteq_path}${fname}
.RECORDMODE OFF
SELECT '${targetdate}, count';

WITH new_kwd AS (
    SELECT keyword
          ,Count(*) Cnt
      FROM SBX_ICB_ICBB.food_fgs258_twitter_explore
     WHERE up_date = '${targetdate}'
       AND page_id IN ('アイス','グルメ','ケーキ','スイーツ','チョコレート')
       AND ( keyword LIKE '%ケーキ%' OR keyword LIKE '%チョコ%' )
     GROUP BY 1
    HAVING Cnt >= 2  
)
, old_kwd AS (
    SELECT DISTINCT(keyword) keyword
      FROM SBX_ICB_ICBB.food_fgs258_twitter_explore
     WHERE up_date < '${targetdate}'
       AND page_id IN ('アイス','グルメ','ケーキ','スイーツ','チョコレート')
       AND ( keyword LIKE '%ケーキ%' OR keyword LIKE '%チョコ%' )
)
, f_kwd AS (
    SELECT DISTINCT(keyword) keyword
      FROM SBX_ICB_ICBB.food_FGS239_insta_posts
)
SELECT
    Trim(x.keyword) || ',' ||
    Trim(x.Cnt)
  FROM
    ( SELECT n.keyword keyword
            ,n.Cnt Cnt 
        FROM new_kwd n
       WHERE NOT EXISTS (SELECT * FROM old_kwd o WHERE o.keyword = n.keyword)
         AND NOT EXISTS (SELECT * FROM f_kwd f WHERE f.keyword = n.keyword)
    ) x
ORDER BY x.Cnt DESC, x.keyword;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            }

            fs.writeFileSync(bteq_path + btqfile, bteq);
            resolve();
        });
    });
}


// load_count
function load_count(filename,tablename) {

    return new Promise((resolve) => {
        co(function*() {
           
            let cnt;
            let btqfile = `${bteq_path}${filename}_count.txt`;
            let btqlog = `${bteq_path}${filename}_count_log.txt`;
            let cntfile = `${bteq_path}${filename}_count_result.txt`;
            shell.rm(cntfile);

            // select count
            let bteq;
            if(type == 1){
                bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${cntfile}
.RECORDMODE OFF
SELECT
    Trim(x.cnt)
FROM 
  ( select count(*) cnt 
      from SBX_ICB_ICBB.${tablename}
     where reg_date = '${targetdate}'
  ) x;
  
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            }else if(type == 2){

                bteq = `
.SESSION CHARSET 'utf8';
.logmech LDAP;
.LOGON ${con}/${usr},${p};

.set width 20000;
.EXPORT DATA FILE=${cntfile}
.RECORDMODE OFF
SELECT
    Trim(x.cnt)
FROM 
  ( select count(*) cnt 
      from SBX_ICB_ICBB.${tablename}
     where up_date = '${targetdate}'
  ) x;
    
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.EXIT
`;
            }

            fs.writeFileSync(btqfile, bteq);
            shell.exec(`bteq < ${btqfile} > ${btqlog}`);

            let data = fs.readFileSync(cntfile, 'utf8');
            cnt = data.match(/\d{1,}/);

            resolve(cnt);
        });
    });
};


// run
co(function*() {
    //yield output();

    if (!cli['type'] || cli['type'] == 1) {
        // instagram
        type = 1;
        const filename = 'targets_food_add_insta';
        const tablename = 'food_fgs242_instagram_explore';
        // out_csv check
        let rt = yield out_check(filename);
        if(!rt) {
            let ct = yield load_count(filename,tablename);
            if(ct > 0){
                yield output(filename);
            }
        }
    }
    if (!cli['type'] || cli['type'] == 2) {
        // twitter        
        type = 2;
        const filename = 'targets_food_add_twitter';
        const tablename = 'food_fgs258_twitter_explore';
        // out_csv check
        let rt = yield out_check(filename);
        if(!rt) {
            let ct = yield load_count(filename,tablename);
            if(ct > 0){
                yield output(filename);
            }
        }
    }
    
    console.log('fgs239-get-hashtag end //');

});