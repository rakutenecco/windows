'use strict';

const moment = require('moment');
const fs = require('fs');
const shell = require('shelljs');
const isFile = require('is-file');
const isdir = require('isdir');
const _ = require('underscore');
const nodemailer = require('nodemailer');

// Create the transporter with the required configuration for Gmail
// change the user and pass !
let transporter = nodemailer.createTransport({
    host: 'smtp.office365.com',
    port: 587,
    // secure: true, // use SSL
    auth: {
        user: 'ts-masako.ikeda@rakuten.com',
        pass: '1234qwerT'
    }
});

// Util 関数生成
let Util = function() {
    this.title = 'util';
    this.DB_USER = 'ts-masako.ikeda';
    this.DB_PW = '1234qwerT';
    // this.DB_DIR = 'D:\/work\/tool\/tools\/output\/';
    this.DB_DIR = 'D:\/work\/tool\/tools\/output\/';
    this.TEMP_FASLOAD = `
    SET SESSION CHARSET 'utf8';

    LOGMECH LDAP;
    .LOGON dm-red101/{0},{7};
    DATABASE SBX_ICB_ICBB;

    DROP TABLE {1};
    DROP TABLE {1}_et;
    DROP TABLE {1}_uv;

    CREATE TABLE {1}
    (
        {2}
    )PRIMARY INDEX ( {3} );

    BEGIN LOADING {1} ERRORFILES {1}_et, {1}_uv;

    set record vartext "	";


    DEFINE 
        {4}
    FILE = {5};


    INSERT INTO {1} VALUES(
        {6}
    );
    END LOADING;
    LOGOFF;
    `;
};


// mail to each person
Util.prototype.remove = function(filepath) {

    try {
        fs.unlinkSync(filepath);
    } catch (e) {}
}


Util.prototype.mailto = function(title, from, to, cc, text, filepath) {

	// setup e-mail data
	let mailOptions = {
	    from: from, // sender address (who sends)
	    to: to, // list of receivers (who receives)
        cc: cc,
	    subject: title, // Subject line
	    text: text
	};

    // 
    if (filepath) {
        mailOptions.attachments = [
            {
                path: filepath // stream this file
            }
        ];
    }

	// send mail with defined transport object
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        return console.log(error);
	    }

	    console.log('Message sent: ' + info.response);
	});
}


// get response data
Util.prototype.fastload = function(mes, project, filepath, isTime, cb) {

    let column = [];
    let data = mes.data;
    let splits = filepath.split('/');
    let filename = splits[splits.length - 1].replace('.txt','');
    let filepath1 = filepath.replace('.txt','') + '_fastload_script.txt';
    let filepath2 = filepath.replace('.txt','') + '_fastload_data.txt';
    let filename2 = filename + '_fastload_data.txt';
    if (isFile(filepath1)) {
        fs.unlinkSync(filepath1);
    }
    if (isFile(filepath2)) {
        fs.unlinkSync(filepath2);
    }

    for (let i = 0; i < data.length; i++) {

        let vals = [];
        for (var key in data[i]) {
            if (data[i].hasOwnProperty(key)) {

                if (i === 0) {
                    column.push({
                        name: key,
                        type: data[i][key]
                    });
                }

                if (i > 0) {
                    vals.push(data[i][key]);
                }
            }
        }

        if (i === 0) { // set fastload script

            let script = this.TEMP_FASLOAD;
            let table = `fashion_${project}`;
            if (isTime) {
                table = table + `_` + moment().format('YYYYMMDD');
            }

            let define = '';
            for (let j = 0; j < column.length; j++) {
                define += column[j].name + ' ' + column[j].type;
                if (j !== column.length - 1) {
                    define += ',';
                }
            }

            let vol = '';
            for (let j = 0; j < column.length; j++) {
                let type_chk = column[j].type.match(/VARCHAR/g);
                let type_vol = type_chk ? ` (${column[j].type}),` : ' (VARCHAR(1000)),';
                vol += column[j].name + type_vol;
            }

            let index = column[0].name;

            let path = this.DB_DIR + filename2; //config.DB_DIR + filepath2;

            let val = '';
            for (let j = 0; j < column.length; j++) {
                val += ':' + column[j].name;
                if (j !== column.length - 1) {
                    val += ',';
                }
            }

            script = script.replace('{0}', this.DB_USER)
                .replace(/\{1\}/g, table)
                .replace('{2}', define)
                .replace('{3}', index)
                .replace('{4}', vol)
                .replace('{5}', path)
                .replace('{6}', val)
                .replace('{7}', this.DB_PW)

            Util.prototype.insertTxt(filepath1, script);

        } else { // set fastload txt file

            let row = vals.join('	') + '\n';
            Util.prototype.insertTxt(filepath2, row);

            // 最終行でfastload実行
            if (i === data.length - 1) {
                console.log('fastload 設定');
                let dbdir = this.DB_DIR;
                setTimeout(function() {
                    shell.exec(`fastload < ${dbdir}${filename}_fastload_script.txt > ${dbdir}${project}_log.txt`);
                    console.log('fastload 実行');
                    cb();
                }, 3000);
            }
        }
    }
}


// get response data
Util.prototype.fastload_ikeda = function(mes, project, filepath, isTime, cb) {

    let column = [];
    let data = mes.data;
    let splits = filepath.split('/');
    let filename = splits[splits.length - 1].replace('.txt','');
    let filepath1 = filepath.replace('.txt','') + '_fastload_script.txt';
    let filepath2 = filepath.replace('.txt','') + '_fastload_data.txt';
    let filename2 = filename + '_fastload_data.txt';
    if (isFile(filepath1)) {
        fs.unlinkSync(filepath1);
    }
    if (isFile(filepath2)) {
        fs.unlinkSync(filepath2);
    }

    for (let i = 0; i < data.length; i++) {

        let vals = [];
        for (var key in data[i]) {
            if (data[i].hasOwnProperty(key)) {

                if (i === 0) {
                    column.push({
                        name: key,
                        type: data[i][key]
                    });
                }

                if (i > 0) {
                    vals.push(data[i][key]);
                }
            }
        }

        if (i === 0) { // set fastload script

            let script = this.TEMP_FASLOAD;
            let table = `fashion_i_${project}`;
            if (isTime) {
                table = table + `_` + moment().format('YYYYMMDD');
            }

            let define = '';
            for (let j = 0; j < column.length; j++) {
                define += column[j].name + ' ' + column[j].type;
                if (j !== column.length - 1) {
                    define += ',';
                }
            }

            let vol = '';
            for (let j = 0; j < column.length; j++) {
                let type_chk = column[j].type.match(/VARCHAR/g);
                let type_vol = type_chk ? ` (${column[j].type}),` : ' (VARCHAR(1000)),';
                vol += column[j].name + type_vol;
            }

            let index = column[0].name;

            let path = this.DB_DIR + filename2; //config.DB_DIR + filepath2;

            let val = '';
            for (let j = 0; j < column.length; j++) {
                val += ':' + column[j].name;
                if (j !== column.length - 1) {
                    val += ',';
                }
            }

            script = script.replace('{0}', this.DB_USER)
                .replace(/\{1\}/g, table)
                .replace('{2}', define)
                .replace('{3}', index)
                .replace('{4}', vol)
                .replace('{5}', path)
                .replace('{6}', val)
                .replace('{7}', this.DB_PW)

            Util.prototype.insertTxt(filepath1, script);

        } else { // set fastload txt file

            let row = vals.join('	') + '\n';
            Util.prototype.insertTxt(filepath2, row);

            // 最終行でfastload実行
            if (i === data.length - 1) {
                console.log('fastload 設定');
                let dbdir = this.DB_DIR;
                setTimeout(function() {
                    shell.exec(`fastload < ${dbdir}${filename}_fastload_script.txt > ${dbdir}${project}_log.txt`);
                    console.log('fastload 実行');
                    cb();
                }, 3000);
            }
        }
    }
}


// insert row data
Util.prototype.insertTxt = function(filepath, row, cb) {

    try {
        // ファイルの読み込み
        fs.appendFileSync(filepath, row, 'utf8');
    } catch (e) {
        console.log('ERROR: insertTxt');
    }
}


// get response data
Util.prototype.getResponse = function(filepath, cb) {

    try {
        // ファイルの読み込み
        let data = fs.readFileSync(filepath, 'utf8');
        return JSON.parse(data);
    } catch (e) {
        console.log('ERROR: PASS');
    }
}

// set response data
Util.prototype.setResponse = function(filepath, mes, cb) {

    try {
        // to json
        let data = JSON.stringify(mes);
        // ファイルの読み込み
        fs.writeFileSync(filepath, data, 'utf8');
    } catch (e) {
        console.log('ERROR: PASS');
    }
}


// insert row data
Util.prototype.getList = function(filepath, {isData, data, delimiter}) {

    let res = {};
    delimiter = delimiter ? delimiter : '	';

    try {

        // ファイルの読み込み
        let file = fs.readFileSync(filepath, 'utf8');
        let rows = file.replace(/\r/g,'').split('\n');
        let heads = [];
        let brands = [];

        res.isData = (isData && rows.length > 1) ? true: false;

        if (data) {
            for (let i = 1; i < rows.length; i++) {
                let brand = {};
                let cells = rows[i].split(delimiter);
                // i=1の場合はキーにする。
                if (i === 1) {
                    heads = cells;
                } else {
                    for (let j = 0; j < cells.length; j++) {
                        brand[heads[j]] = (i>2&&(heads[j]==='num' || heads[j]==='delflag' || heads[j]==='price' || heads[j]==='price_tax')) ? (cells[j]==='' ? '': parseInt(cells[j], 10)) : cells[j];
                        // brand[heads[j]] = (i>2 && (heads[j]==='num' || heads[j]==='delflag' || heads[j]==='price' || heads[j]==='price_tax')) ? parseInt(cells[j], 10) : cells[j];
                    }
                    if (cells.length > 1) {
                        brands.push(brand);
                    }
                }
            }
            res.data = brands;
        }

        return res;

    } catch (e) {
        console.log('ERROR: getList');
        res.isData = false;
        return res;
    }
}



Util.prototype.getBrandList = function(filepath, {
    ids,
    nums,
    categories,
    names,
    from,
    to
}, cb) {

    try {

        fs.readFile(filepath, 'utf8', function(err, data) {

            if (err) throw err;
            let cnt = 0;
            let heads = [];
            let brands = [];
            let rows = data.replace(/\r/g, '').split('\n');

            rows.forEach(function(row, i) {

                // 日本語除外
                if (i < 1) return;

                let brand = {};
                let cells = row.split('	');

                // i=1の場合はキーにする。
                if (i === 1) {
                    heads = cells;

                } else {
                    for (let j = 0; j < cells.length; j++) {
                        brand[heads[j]] = cells[j];
                    }

                    // 除外処理
                    if (brand.delflag === 1) return;

                    // ids, nums, categories, names
                    if (
                        ( // delflagの除外
                            (brand.delflag === 1)
                        ) ||
                        (!( // ids, nums, categories, namesの指定内容を合わせる
                                (ids.indexOf(brand.id) >= 0) ||
                                (nums.indexOf(brand.num) >= 0) ||
                                (categories.indexOf(brand.category) >= 0) ||
                                (names.indexOf(brand.name) >= 0)
                            ) &&
                            !( // すべてに指定がなかった場合は全対象の例外
                                (ids.length === 0) &&
                                (nums.length === 0) &&
                                (categories.length === 0) &&
                                (names.length === 0)
                            )
                        )
                    ) {
                        return;
                    }

                    cnt++;

                    if ( // cntが範囲内であること(to==-1は全範囲)
                        !(cnt >= from && cnt < to) &&
                        to > 0
                    ) {
                        return;
                    }


                    brands.push(brand);
                }
            });
            // call back
            cb(brands);
        });

    } catch (e) {
        console.log('ERROR: PASS');
    }
}







Util.prototype.setBteq_casio1 = function(filepath) {

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        drop table SBX_ICB_ICBB.TMP_casio;
        drop view SBX_ICB_ICBB.fashion_casio_all;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


Util.prototype.setBteq_casio2 = function(filepath, casio_days) {

    const day1 = casio_days[0];

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        create multiset table  SBX_ICB_ICBB.TMP_casio
        as
        (SELECT t0.MPN_1, '${day1}' as times FROM SBX_ICB_ICBB.fashion_casio_${day1} t0 )
        with data primary index ( MPN_1 , times );

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


Util.prototype.setBteq_casio3 = function(filepath, casio_days) {

    let inner = `ins into  SBX_ICB_ICBB.TMP_casio SELECT t1.MPN_1, '{0}' as times FROM SBX_ICB_ICBB.fashion_casio_{0} t1;`;

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

{0}

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;


    let inner2 = '';
    for (let i = 0; i < casio_days.length; i++) {
        inner2 += inner.replace(/\{0\}/g, casio_days[i]) + '\n';
    }

    let bteq2 = bteq.replace('{0}', inner2);

    fs.writeFileSync(filepath, bteq2);
}


Util.prototype.setBteq_casio4 = function(filepath) {

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

    collect statistics index ( MPN_1,times) on  SBX_ICB_ICBB.TMP_casio;
    collect statistics  column ( MPN_1 ),column(times) on  SBX_ICB_ICBB.TMP_casio;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


Util.prototype.setBteq_casio5 = function(filepath) {

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

    CREATE VIEW SBX_ICB_ICBB.fashion_casio_all
    AS

    select
        ROW_NUMBER() OVER( ORDER BY t.MPN_1 ) AS Cnt,
        t.MPN_1,
        max(t.times) as times
    from
    SBX_ICB_ICBB.TMP_casio t
    group by t.MPN_1;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


// Util.prototype.setBteq_casio5_2 = function(filepath) {

//     let month0Start = moment().format('YYYY-MM-01');
//     let month1Start = moment().add(-1, "months").format('YYYY-MM-01');

//     let bteq = `
// .SESSION CHARSET 'utf8';

// .logmech LDAP;
// .LOGON dm-red101/ts-satohiro.kuriyama,Ktry02027;
// .set width 20000;
// .SET separator "	";

//     DATABASE SBX_ICB_ICBB;

//         ct SBX_ICB_ICBB.TMP_red_basket_detail_tbl_selected as
//         (
//         select order_no,units,item_name,price from UA_VIEW_MK_ICHIBA.red_basket_detail_tbl s
//         inner join UA_VIEW_MK_ICHIBA.item_genre_dimension201610 i
//         on
//         (
//         s.genre_id = i.genre_id
//         and i.g1 in (558929)
//         )
//         and
//         s.reg_datetime >= '${month1Start} 00:00:00' and s.reg_datetime < '${month0Start} 00:00:00' group by 1,2,3,4
//         )
//         with data primary index ( order_no,units,item_name,price);

//     .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

// .LOGOFF
// .EXIT
//     `;

//     fs.writeFileSync(filepath, bteq);
// }


// Util.prototype.setBteq_casio5_3 = function(filepath) {

//     let bteq = `
// .SESSION CHARSET 'utf8';

// .logmech LDAP;
// .LOGON dm-red101/ts-satohiro.kuriyama,Ktry02027;
// .set width 20000;
// .SET separator "	";

//     DATABASE SBX_ICB_ICBB;

//     COLLECT STATISTICS COLUMN (ORDER_NO) ON SBX_ICB_ICBB.TMP_red_basket_detail_tbl_selected;

//     .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

// .LOGOFF
// .EXIT
//     `;

//     fs.writeFileSync(filepath, bteq);
// }




Util.prototype.setBteq_casio_usa1 = function(filepath) {

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        drop table SBX_ICB_ICBB.TMP_casio_usa;
        drop view SBX_ICB_ICBB.fashion_casio_usd_all;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


Util.prototype.setBteq_casio_usa2 = function(filepath, casio_days) {

    const day1 = casio_days[0];

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        create multiset table  SBX_ICB_ICBB.TMP_casio_usa
        as
        (SELECT t0.MPN_1, '${day1}' as times FROM SBX_ICB_ICBB.fashion_casio_usa_${day1} t0 )
        with data primary index ( MPN_1 , times );

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


Util.prototype.setBteq_casio_usa3 = function(filepath, casio_days) {

    let inner = `ins into  SBX_ICB_ICBB.TMP_casio_usa SELECT t1.MPN_1, '{0}' as times FROM SBX_ICB_ICBB.fashion_casio_usa_{0} t1;`;

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

{0}

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;


    let inner2 = '';
    for (let i = 0; i < casio_days.length; i++) {
        inner2 += inner.replace(/\{0\}/g, casio_days[i]) + '\n';
    }

    let bteq2 = bteq.replace('{0}', inner2);

    fs.writeFileSync(filepath, bteq2);
}


Util.prototype.setBteq_casio_usa4 = function(filepath) {

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

    collect statistics index ( MPN_1,times) on  SBX_ICB_ICBB.TMP_casio_usa;
    collect statistics  column ( MPN_1 ),column(times) on  SBX_ICB_ICBB.TMP_casio_usa;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


Util.prototype.setBteq_casio_usa5 = function(filepath) {

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

    CREATE VIEW SBX_ICB_ICBB.fashion_casio_usd_all
    AS

    SELECT
    ROW_NUMBER() OVER( ORDER BY t.mpn_1_1 ) AS Cnt,
    t.times,
    t.mpn_1_1,
    t.mpn_1_2,
    t.mpn_1_3
    FROM
    (
        select
        t.times
        ,t.MPN_1 as mpn_1_1
        ,case when REGEXP_SUBSTR(t.MPN_1, '^[A-Z]{2}', 1, 1, 'i') is NULL then null else
        REGEXP_REPLACE(t.MPN_1, '^(' || REGEXP_SUBSTR(t.MPN_1, '^[A-Z]{2}', 1, 1, 'i') || ')', REGEXP_SUBSTR(t.MPN_1, '^[A-Z]{2}', 1, 1, 'i') || '-', 1, 0, 'i')
        end as mpn_1_2
        ,case when REGEXP_SUBSTR(t.MPN_1, '^[A-Z]{3}', 1, 1, 'i') is NULL then null else
        REGEXP_REPLACE(t.MPN_1, '^(' || REGEXP_SUBSTR(t.MPN_1, '^[A-Z]{3}', 1, 1, 'i') || ')', REGEXP_SUBSTR(t.MPN_1, '^[A-Z]{3}', 1, 1, 'i') || '-', 1, 0, 'i')
        end as mpn_1_3
        from
        (
            SELECT
            ROW_NUMBER() OVER( ORDER BY t.MPN_1 ) AS Cnt,
            t.MPN_1,
            max(t.times) as times
            FROM

            SBX_ICB_ICBB.TMP_casio_usa t

            group by t.MPN_1
        ) t
    ) t;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


// Util.prototype.setBteq_casio_usa5_2 = function(filepath) {

//     let month0Start = moment().format('YYYY-MM-01');
//     let month1Start = moment().add(-1, "months").format('YYYY-MM-01');

//     let bteq = `
// .SESSION CHARSET 'utf8';

// .logmech LDAP;
// .LOGON dm-red101/ts-satohiro.kuriyama,Ktry02027;
// .set width 20000;
// .SET separator "	";

//     DATABASE SBX_ICB_ICBB;

//         ct SBX_ICB_ICBB.TMP_red_basket_detail_tbl_selected as
//         (
//         select order_no,units,item_name,price from UA_VIEW_MK_ICHIBA.red_basket_detail_tbl s
//         inner join UA_VIEW_MK_ICHIBA.item_genre_dimension201610 i
//         on
//         (
//         s.genre_id = i.genre_id
//         and i.g1 in (558929)
//         )
//         and
//         s.reg_datetime >= '${month1Start} 00:00:00' and s.reg_datetime < '${month0Start} 00:00:00' group by 1,2,3,4
//         )
//         with data primary index ( order_no,units,item_name,price);

//     .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

// .LOGOFF
// .EXIT
//     `;

//     fs.writeFileSync(filepath, bteq);
// }


// Util.prototype.setBteq_casio_usa5_3 = function(filepath) {

//     let bteq = `
// .SESSION CHARSET 'utf8';

// .logmech LDAP;
// .LOGON dm-red101/ts-satohiro.kuriyama,Ktry02027;
// .set width 20000;
// .SET separator "	";

//     DATABASE SBX_ICB_ICBB;

//     COLLECT STATISTICS COLUMN (ORDER_NO) ON SBX_ICB_ICBB.TMP_red_basket_detail_tbl_selected;

//     .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

// .LOGOFF
// .EXIT
//     `;

//     fs.writeFileSync(filepath, bteq);
// }




Util.prototype.setBteq_seiko1 = function(filepath) {

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        drop table SBX_ICB_ICBB.TMP_seiko;
        drop view SBX_ICB_ICBB.fashion_seiko_all;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


Util.prototype.setBteq_seiko2 = function(filepath, seiko_days) {

    const day1 = seiko_days[0];

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        create multiset table  SBX_ICB_ICBB.TMP_seiko
        as
        (SELECT t0.MPN_1, '${day1}' as times FROM SBX_ICB_ICBB.fashion_seiko_${day1} t0 )
        with data primary index ( MPN_1 , times );

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


Util.prototype.setBteq_seiko3 = function(filepath, seiko_days) {

    let inner = `ins into  SBX_ICB_ICBB.TMP_seiko SELECT t1.MPN_1, '{0}' as times FROM SBX_ICB_ICBB.fashion_seiko_{0} t1;`;

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

{0}

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;


    let inner2 = '';
    for (let i = 0; i < seiko_days.length; i++) {
        inner2 += inner.replace(/\{0\}/g, seiko_days[i]) + '\n';
    }

    let bteq2 = bteq.replace('{0}', inner2);

    fs.writeFileSync(filepath, bteq2);
}


Util.prototype.setBteq_seiko4 = function(filepath) {

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

    collect statistics index ( MPN_1,times) on  SBX_ICB_ICBB.TMP_seiko;
    collect statistics  column ( MPN_1 ),column(times) on  SBX_ICB_ICBB.TMP_seiko;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


Util.prototype.setBteq_seiko5 = function(filepath) {

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

    CREATE VIEW SBX_ICB_ICBB.fashion_seiko_all
    AS

    select
        ROW_NUMBER() OVER( ORDER BY t.MPN_1 ) AS Cnt,
        t.MPN_1,
        max(t.times) as times
    from
    SBX_ICB_ICBB.TMP_seiko t
    group by t.MPN_1;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}


Util.prototype.setBteq_seiko5_2 = function(filepath) {

    let month0Start = moment().format('YYYY-MM-01');
    let month1Start = moment().add(-1, "months").format('YYYY-MM-01');

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        CREATE VOLATILE TABLE TMP_red_basket_detail_tbl_selected
        AS (
        select order_no,units,item_name,price from UA_VIEW_MK_ICHIBA.red_basket_detail_tbl s
        inner join UA_VIEW_MK_ICHIBA.item_genre_dimension201610 i
        on
        (
        s.genre_id = i.genre_id
        and i.g1 in (558929)
        )
        and
        s.reg_datetime >= '${month1Start} 00:00:00' and s.reg_datetime < '${month0Start} 00:00:00' group by 1,2,3,4
        ) WITH DATA
        PRIMARY INDEX(order_no,units,item_name,price)
        ON COMMIT PRESERVE ROWS

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

    `;

    fs.writeFileSync(filepath, bteq);
}


// Util.prototype.setBteq_seiko5_3 = function(filepath) {

//     let bteq = `
// .SESSION CHARSET 'utf8';

// .logmech LDAP;
// .LOGON dm-red101/ts-satohiro.kuriyama,Ktry02027;
// .set width 20000;
// .SET separator "	";

//     DATABASE SBX_ICB_ICBB;

//     COLLECT STATISTICS COLUMN (ORDER_NO) ON TMP_red_basket_detail_tbl_selected;

//     .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

// .LOGOFF
// .EXIT
//     `;

//     fs.writeFileSync(filepath, bteq);
// }




Util.prototype.setBteq_genre_instance = function(filepath, genre) {

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

    INSERT INTO SBX_ICB_ICBB.fashion_genre_${genre}_instance (
        ge_id,ranking,search_date,search_word,gms,page_view,visits,instances
    )

    SELECT
        ge_id,ranking,search_date,search_word,gms,page_view,visits,instances
    FROM SBX_ICB_ICBB.fashion_genre_${genre}_instance_pre A
    where NOT EXISTS

    (SELECT * FROM SBX_ICB_ICBB.fashion_genre_${genre}_instance B WHERE A.search_date = B.search_date);

    drop table SBX_ICB_ICBB.fashion_genre_${genre}_instance_pre;
    drop table SBX_ICB_ICBB.fashion_genre_${genre}_instance_pre_et;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}



Util.prototype.setBteq_genre_week_instance = function(filepath, genre) {

    let bteq = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

    INSERT INTO SBX_ICB_ICBB.fashion_genre_week_${genre}_instance (
        ge_id,ranking,search_date,search_word,gms,page_view,visits,instances
    )

    SELECT
        ge_id,ranking,search_date,search_word,gms,page_view,visits,instances
    FROM SBX_ICB_ICBB.fashion_genre_week_${genre}_instance_pre A
    where NOT EXISTS

    (SELECT * FROM SBX_ICB_ICBB.fashion_genre_week_${genre}_instance B WHERE A.search_date = B.search_date);

    drop table SBX_ICB_ICBB.fashion_genre_week_${genre}_instance_pre;
    drop table SBX_ICB_ICBB.fashion_genre_week_${genre}_instance_pre_et;

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
    `;

    fs.writeFileSync(filepath, bteq);
}











Util.prototype.toCsv = function(filepath, filename) {

    let today = moment().format('YYYYMMDD');

    let data = fs.readFileSync(filepath, 'utf8');
    let data2 = data.replace(/( *)/g,'').replace(/	/g,',').replace(/^(-*)$/g,'').replace(/(,,*\r\n)/g,'');

    filepath = filepath.replace(/out/i, filename + '_out').replace(/out./i, today).replace(/\.txt/i, '.csv');
    fs.writeFileSync(filepath, data2);
}


Util.prototype.newJson = function(filepath) {

    let today = moment().format('YYYYMMDD');
    let data_json = fs.readFileSync(filepath, 'utf8');
    let data_obj = JSON.parse(data_json);
    data_obj.push(today);
    data_obj = data_obj.filter(function (x, i, self) {
        return self.indexOf(x) === i;
    });
    let new_json = JSON.stringify(data_obj);
    fs.writeFileSync(filepath, new_json);
}


Util.prototype.mkDir = function(dirPath, cb) {

    isdir(dirPath, function callback(err, dir) { // named callback function 
        if(err) {
            fs.mkdirSync(dirPath);
            cb();
        } else if(dir) {
            cb();
        } else {
            fs.mkdirSync(dirPath);
            cb();
        }
    });
}






Util.prototype.makeBteqData_mecab = function(filepath_bteq, filepath_export, filepath_export2, sql, sql_order, run_bteq, filepath_export_out) {

    let export_tmp = `.EXPORT REPORT FILE={0}`;

    let bteq_tmp = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        {0}

        {1}

        qualify row_number() over (order by {2}) BETWEEN {3} and {4};

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
`;

    let isEnd = false; // 最終行までデータ取得が完了しているか
    let cnt = 0;


    // unlink
    try {
        fs.unlinkSync(filepath_export2);
    } catch (e) {}


    while (!isEnd) {

        // ファイルのチェック
        try {
            const stats = fs.statSync(filepath_export2);
            const fileSizeInBytes = stats.size;
            if (fileSizeInBytes < 2000000) {
                console.log('out !!!!', fileSizeInBytes, filepath_export2);
                isEnd = true;
                return;
            }

            // unlink
            fs.unlinkSync(filepath_export2);
        } catch (e) {}

        // set bteq
        let bteq = bteq_tmp;
        let start = cnt * 20000 + 1;
        let end = (cnt + 1) * 20000;

        // outファイルに出力する場合
        if (filepath_export) {
            let export_str = export_tmp.replace('{0}', filepath_export);
            bteq = bteq.replace('{0}', export_str);
        } else {
            bteq = bteq.replace('{0}', '');
        }

        // sqlを作成
        bteq = bteq.replace('{1}', sql).replace('{2}', sql_order).replace('{3}', start).replace('{4}', end);
        fs.writeFileSync(filepath_bteq, bteq, 'utf8');

        // fileの実行
        shell.exec(run_bteq);

        // csvの書き込み
        let data = fs.readFileSync(filepath_export2, 'utf8');
        if (cnt > 0) {
            // 1.2行目削除
            data = data.replace(/( ( +))/g,'').replace(/(.*)-----/i, '').replace(/	/g,',').replace(/^(.*)/i, '').replace(/\r\n/i, '').replace(/\r\n/i, '');
        } else {
            // 2行目のみ削除　------
            data = data.replace(/( ( +))/g,'').replace(/(.*)-----/i, '').replace(/	/g,',').replace(/\r\n/i, '');
        }

        // ? の削除
        if (data.length < 3) {
            return;
        }

        let filepath_export3 = filepath_export2.replace('.txt', '.csv');
        fs.appendFileSync(filepath_export3, data, 'utf8');

        if (filepath_export_out) {
            fs.appendFileSync(filepath_export_out, data, 'utf8');
        }

        cnt++;
    }
}



Util.prototype.makeBteqData = function(filepath_bteq, filepath_export, filepath_export2, sql, sql_order, run_bteq, filepath_export_out, sql_head) {

    let export_tmp = `.EXPORT REPORT FILE={0}`;

    let bteq_tmp = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        {0}

        {1}

        qualify row_number() over (order by {2}) BETWEEN {3} and {4};

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
`;

    let bteq_tmp2 = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        {0}

        {-1}

        {1}

        qualify row_number() over (order by {2}) BETWEEN {3} and {4};

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
`;


    let isEnd = false; // 最終行までデータ取得が完了しているか
    let cnt = 0;

    if (sql_head) {
        bteq_tmp = bteq_tmp2.replace('{-1}', sql_head);
    }

    // unlink
    try {
        fs.unlinkSync(filepath_export2);
    } catch (e) {}


    while (!isEnd) {

        // ファイルのチェック
        try {
            const stats = fs.statSync(filepath_export2);
            const fileSizeInBytes = stats.size;
            if (fileSizeInBytes < 2000000) {
                console.log('out !!!!', fileSizeInBytes, filepath_export2);
                isEnd = true;
                return;
            }

            // unlink
            fs.unlinkSync(filepath_export2);
        } catch (e) {}

        // set bteq
        let bteq = bteq_tmp;
        let start = cnt * 20000 + 1;
        let end = (cnt + 1) * 20000;

        // outファイルに出力する場合
        if (filepath_export) {
            let export_str = export_tmp.replace('{0}', filepath_export);
            bteq = bteq.replace('{0}', export_str);
        } else {
            bteq = bteq.replace('{0}', '');
        }

        // sqlを作成
        bteq = bteq.replace('{1}', sql).replace('{2}', sql_order).replace('{3}', start).replace('{4}', end);
        fs.writeFileSync(filepath_bteq, bteq, 'utf8');

        // fileの実行
        shell.exec(run_bteq);

        // csvの書き込み
        let data = fs.readFileSync(filepath_export2, 'utf8');
        if (cnt > 0) {
            // 1.2行目削除
            data = data.replace(/( *)/g,'').replace(/(.*)-----/i, '').replace(/	/g,',').replace(/^(.*)/i, '').replace(/\r\n/i, '').replace(/\r\n/i, '');
        } else {
            // 2行目のみ削除　------
            data = data.replace(/( *)/g,'').replace(/(.*)-----/i, '').replace(/	/g,',').replace(/\r\n/i, '');
        }

        let filepath_export3 = filepath_export2.replace('.txt', '.csv');
        fs.appendFileSync(filepath_export3, data, 'utf8');

        if (filepath_export_out) {
            fs.appendFileSync(filepath_export_out, data, 'utf8');
        }

        cnt++;
    }
}




Util.prototype.makeBteqDataTab_namechange = function(filepath_bteq, filepath_export, filepath_export2, sql, sql_order, run_bteq, CB_filename) {

    let export_tmp = `.EXPORT REPORT FILE={0}`;

    let bteq_tmp = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        {0}

        {1}

        qualify row_number() over (order by {2}) BETWEEN {3} and {4};

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
`;

    let isEnd = false; // 最終行までデータ取得が完了しているか
    let cnt = 0;


    // unlink
    try {
        fs.unlinkSync(filepath_export2);
    } catch (e) {}


    while (!isEnd) {

        // ファイルのチェック
        try {
            const stats = fs.statSync(filepath_export2);
            const fileSizeInBytes = stats.size;
            if (fileSizeInBytes < 10000000) {
                console.log('out !!!!', fileSizeInBytes, filepath_export2);
                isEnd = true;
                return;
            }

            // unlink
            fs.unlinkSync(filepath_export2);
        } catch (e) {}

        // set bteq
        let bteq = bteq_tmp;
        let start = cnt * 20000 + 1;
        let end = (cnt + 1) * 20000;

        // outファイルに出力する場合
        if (filepath_export) {
            let export_str = export_tmp.replace('{0}', filepath_export);
            bteq = bteq.replace('{0}', export_str);
        } else {
            bteq = bteq.replace('{0}', '');
        }

        // sqlを作成
        bteq = bteq.replace('{1}', sql).replace('{2}', sql_order).replace('{3}', start).replace('{4}', end);
        fs.writeFileSync(filepath_bteq, bteq, 'utf8');

        // fileの実行
        shell.exec(run_bteq);

        // csvの書き込み
        let data = fs.readFileSync(filepath_export2, 'utf8');
        if (cnt > 0) {
            // 1.2行目削除
            data = data.replace(/( *)/g,'').replace(/(.*)-----/i, '').replace(/^(.*)/i, '').replace(/\r\n/i, '').replace(/\r\n/i, '');
        } else {
            // 2行目のみ削除　------
            data = data.replace(/( *)/g,'').replace(/(.*)-----/i, '').replace(/\r\n/i, '');
        }


        let rows = data.replace(/\r\n/, '\n').split('\n');
        for (let i = 1; i < rows.length; i++) {

            let cells = rows[i].split('	');
            let filepath_out = CB_filename(cells);
            // console.log(filepath_out);
            fs.appendFileSync(filepath_out, rows[i], 'utf8');
        }

        // if (filepath_export_out) {
        //     fs.appendFileSync(filepath_export_out, data, 'utf8');
        // }

        cnt++;
    }
}




Util.prototype.makeBteqDataTab = function(filepath_bteq, filepath_export, filepath_export2, sql, sql_order, run_bteq, filepath_export_out) {

    let export_tmp = `.EXPORT REPORT FILE={0}`;

    let bteq_tmp = `
.SESSION CHARSET 'utf8';

.logmech LDAP;
.LOGON dm-red101/ts-masako.ikeda,1234qwerT;
.set width 20000;
.SET separator "	";

    DATABASE SBX_ICB_ICBB;

        {0}

        {1}

        qualify row_number() over (order by {2}) BETWEEN {3} and {4};

    .IF ERRORCODE <> 0 THEN .EXIT ERRORCODE;

.LOGOFF
.EXIT
`;

    let isEnd = false; // 最終行までデータ取得が完了しているか
    let cnt = 0;


    // unlink
    try {
        fs.unlinkSync(filepath_export2);
    } catch (e) {}


    while (!isEnd) {

        // ファイルのチェック
        try {
            const stats = fs.statSync(filepath_export2);
            const fileSizeInBytes = stats.size;
            if (fileSizeInBytes < 10000000) {
                console.log('out !!!!', fileSizeInBytes, filepath_export2);
                isEnd = true;
                return;
            }

            // unlink
            fs.unlinkSync(filepath_export2);
        } catch (e) {}

        // set bteq
        let bteq = bteq_tmp;
        let start = cnt * 20000 + 1;
        let end = (cnt + 1) * 20000;

        // outファイルに出力する場合
        if (filepath_export) {
            let export_str = export_tmp.replace('{0}', filepath_export);
            bteq = bteq.replace('{0}', export_str);
        } else {
            bteq = bteq.replace('{0}', '');
        }

        // sqlを作成
        bteq = bteq.replace('{1}', sql).replace('{2}', sql_order).replace('{3}', start).replace('{4}', end);
        fs.writeFileSync(filepath_bteq, bteq, 'utf8');

        // fileの実行
        shell.exec(run_bteq);

        // csvの書き込み
        let data = fs.readFileSync(filepath_export2, 'utf8');
        if (cnt > 0) {
            // 1.2行目削除
            data = data.replace(/( *)/g,'').replace(/(.*)-----/i, '').replace(/^(.*)/i, '').replace(/\r\n/i, '').replace(/\r\n/i, '');
        } else {
            // 2行目のみ削除　------
            data = data.replace(/( *)/g,'').replace(/(.*)-----/i, '').replace(/\r\n/i, '');
        }

        let filepath_export3 = filepath_export2.replace('.txt', '.tsv');
        fs.appendFileSync(filepath_export3, data, 'utf8');

        if (filepath_export_out) {
            fs.appendFileSync(filepath_export_out, data, 'utf8');
        }

        cnt++;
    }
}


Util.prototype.getBteqData = function(filepath) {

    let data = fs.readFileSync(filepath, 'utf8');
    let row = data.replace(/\r/g,'').split('\n');
    let out = [];

    // bteqで取得したデータをセルに整形し、テキストで保存する。
    for (let i = 0; i < row.length; i++) {

        // ハイフン区切り（-）は除外する
        if (i === 1) {
            continue;
        }

        // 各行から、タブで区切って、スペースを削除し、
        row[i] = row[i].split('\t');
        for (let j = 0; j < row[i].length; j++) {
            row[i][j] = row[i][j].replace(/^ */, '').replace(/ *$/, '');
            if (i===0) {
                row[i][j] = row[i][j].replace(/ /g,'');
            }
        }

        // 不完全な行は削除
        if (row[i].length < 2) {
            continue;
        }

        // もう一度タブで区切る。
        out.push(row[i]);
    }

    return out;
}


Util.prototype.isInTime = function() {

    let flag = false;

    let now = moment().format('HH:mm');
    if (now > '05:30' && now < '21:00') {
        flag = true;
    }

    return flag;
}

Util.prototype.isInTime_test = function() {

    let flag = false;

    let now = moment().add(-16, 'hours').format('HH:mm');
    if (now > '05:30' && now < '21:00') {
        flag = true;
    }

    return flag;
}

// remove file
Util.prototype.unlink = function(filepath) {

    try {
        fs.unlinkSync(filepath);
    } catch (e) {}
};





module.exports = Util;